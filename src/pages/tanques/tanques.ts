import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { SqliteDbProvider } from '../../providers/sqlite-db/sqlite-db';
import { InformacionApiProvider } from '../../providers/informacion-api/informacion-api';

/**
 * Generated class for the TanquesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-tanques',
  templateUrl: 'tanques.html',
})
export class TanquesPage {

  tanquesSQLite = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,public db: SqliteDbProvider,public info: InformacionApiProvider, public aCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TanquesPage');
    this.obtenerTanques();
  }

  //obtengo los mantenimientos para desplegarlos en lista
  obtenerTanques(){
    this.db.obtenerTanques().then((data) => {

      this.tanquesSQLite= data;
      console.log(this.tanquesSQLite);

    },(error) =>{
      console.log(error);
    })
  }

  //metodo que actualiza el campo enviado para cambiar el color en la interfaz
  actualizarEnviado(numserie:any){
    this.db.actualizarEnviadoTanque({
      numserie: numserie,
      enviado: 1
    }).then((data) => {
      console.log(data);
    }).catch(error => {console.error(error)});

   this.obtenerTanques();
  }

  enviarTanque(granelera){


    var tanque = {
      numSerie: granelera.numserie,
      capacidadLitros: granelera.capacidadlitros,
      capacidadGalones: granelera.capacidadgalones,
      fechaFabricacion: granelera.fechafabricacion,
      fabricante: granelera.fabricante,
      codigoCliente: granelera.codcliente,
      numContrato: granelera.numcontrato,
      ruta: granelera.ruta,
      propietario: granelera.propietario,
      nombreNegocio: granelera.nombreNegocio,
      contratoActivo: granelera.contratoActivo,
      estado: granelera.estado,
      Latitud: granelera.latitud,
      Longitud: granelera.longitud,
      provincia: granelera.provincia,
      canton: granelera.canton,
      distrito: granelera.distrito,
      marcaTanque: granelera.marcaTanque,
      posicionTanque: granelera.posicionTanque,
      fechaClave: granelera.fechaClave
    }

    //console.log(fac);

    if(granelera.numserie !== ''){
      this.info.enviarTanques(tanque).then(data => {
        console.log('Enviado con Exito');
        console.log(data);
        this.actualizarEnviado(tanque.numSerie);
        this.mensajeEnvio();
      }, err => {
        console.log(err);
       console.log("Error de envio sin internet");
      })
    }
  }

  mensajeEnvio() {
    let alert = this.aCtrl.create({
      title: 'Tanque enviado a la base de datos',
      buttons: ['Aceptar']
    });
    alert.present();
  }

}
