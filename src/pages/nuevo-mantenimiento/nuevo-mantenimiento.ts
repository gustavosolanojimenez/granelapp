import { ImprimirMantePage } from './../imprimir-mante/imprimir-mante';
import { Component } from '@angular/core';
import { Camera } from '@ionic-native/camera';
import {ImagePicker} from '@ionic-native/image-picker';
import { SqliteDbProvider } from '../../providers/sqlite-db/sqlite-db';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { MenuMantenimientoPage } from '../menu-mantenimiento/menu-mantenimiento';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

/**
 * Generated class for the NuevoMantenimientoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-nuevo-mantenimiento',
  templateUrl: 'nuevo-mantenimiento.html',
})
export class NuevoMantenimientoPage {

  trabajos:any; serie; observaciones; fechavisita; fotouno; fotodos;
  image:any;
  fotos = [];
  mantenimientos = [];
  fechacompleta;
  fechavalvula; porcentajeInstalacion = 0; porcentajeDesinstalacion=0;
  terminado = 'no';
  horaInicio;
  horaFin;
  codigo;
  razon;
  nombre;

  constructor(public navCtrl: NavController,public aCtrl:AlertController, public navParams: NavParams,public camera: Camera,public picker:ImagePicker, public db: SqliteDbProvider) {

    this.codigo = this.navParams.get('codigo');
    this.razon = this.navParams.get('razon');
    this.nombre = this.navParams.get('nombre');

    console.log(this.codigo);
    console.log(this.nombre);
    console.log(this.razon



      );



  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NuevoMantenimientoPage');
  }

  guardar(){
   console.log(this.trabajos);
  }

  abrirGaleria(){
    this.camera.getPicture({
      quality: 20,
      destinationType:this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit:false,
      encodingType:this.camera.EncodingType.JPEG,
      targetHeight:1024,
      targetWidth:1024,
      correctOrientation:true,
      saveToPhotoAlbum: true
    }).then(resultado => {
      this.image = "data:image/jpeg;base64," + resultado;
      console.log(this.image);
      this.fotos.push(this.image);
      console.log(this.fotos);
    }).catch(error => {
      console.log(error)
    })
  }

  borrarFoto(index){
    this.fotos.splice(index, 1);
    console.log(this.fotos);
  }

  guardarNuevoMantenimiento(){
      //recorro los productos que obtuve del api

      var fecha =  new Date();
      var day;

      var dia = fecha.getDate();
      var mes =  fecha.getMonth();
      var year =  fecha.getFullYear();

      var horas = fecha.getHours();
      var minut = fecha.getMinutes();
      var segun = fecha.getSeconds();
      var minutos;
      var segundos;
      var meses = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

      if(minut <= 9){
            minutos = '0'+minut.toString();
      }else{
            minutos = minut.toString();
      }

      if(segun <= 9){
          segundos = '0'+segun.toString();
      }else{
          segundos = segun.toString();
      }

      if(dia <= 9){
        day = '0'+dia.toString();
      }else{
        day = dia.toString();
      }

      this.fechacompleta = day+'-'+meses[mes]+'-'+year+' '+horas+':'+minutos+':'+segundos;
      this.horaInicio = horas+':'+minutos+':'+segundos;
      this.fotouno = this.fotos[0];
      this.fotodos = this.fotos[1];


      //armo cada cliente para despues almacenarlos
      let mantenimiento = {
        horaInicio: this.horaInicio,
        fechavisita: this.fechacompleta,
        observaciones: this.observaciones,
        serieTanque: this.serie,
        foto1: this.fotouno,
        foto2: this.fotodos,
        fechavalvula: this.fechavalvula,
        porcentajeGasDesin: this.porcentajeDesinstalacion,
        porcentajeGasInsta: this.porcentajeInstalacion,
        terminado:this.terminado,
        trabajorealizado: JSON.stringify(this.trabajos) ,
        horaFin:this.horaFin,
      }

      console.log(mantenimiento);

      this.db.guardarMantenimiento(mantenimiento);

      this.mensajeDeSeguridad();
  }


  obtenerMantenimientos(){
    this.db.obtenerMantenimientos().then((data) => {

      this.mantenimientos= data;
      console.log(this.mantenimientos);

    },(error) =>{
      console.log(error);
    })
  }

  //alerta que aparece cuando tienen que verificar si lleno la informacion
  mensajeDeSeguridad() {
    let alert = this.aCtrl.create({
      title: 'Reparación registrada correctamente',
      buttons: [
       {
          text: 'Aceptar',
          handler: () => {
            console.log('Confirm Okay');

          }
        }
      ]
    });
    alert.present();
  }

  iraImprimirMante(){
    this.navCtrl.push(ImprimirMantePage,{trabajos: this.trabajos, observaciones: this.observaciones, serieTanque: this.serie, terminado: this.terminado, nombre: this.nombre, razon: this.razon, codigo: this.codigo, fechavalvula: this.fechavalvula} );
  }


}
