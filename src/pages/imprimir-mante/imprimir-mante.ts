import { MenuMantenimientoPage } from './../menu-mantenimiento/menu-mantenimiento';
import { MenuGranelPage } from './../menu-granel/menu-granel';
import { PrinterProvider } from './../../providers/printer/printer/printer';
import { SqliteDbProvider } from './../../providers/sqlite-db/sqlite-db';
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, Platform, AlertController } from 'ionic-angular';
import { commands } from '../../providers/printer/printer/printer-commands';


/**
 * Generated class for the ImprimirMantePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-imprimir-mante',
  templateUrl: 'imprimir-mante.html',
})
export class ImprimirMantePage {

trabajos; serieTanque; observaciones; fechavisita; fotouno; fotodos; fechavalvula;
image:any;
fotos = [];
mantenimientos = [];
fechacompleta;
fechaV; porcentajeGasInsta; porcentajeGasDesin;
terminado;
titulo = '  Gas Tomza de  Costa Rica S.A.';
cedula = '3-101-349880';
subtitulo;
subtitulo1;
subtitulo2;
contadorOriginal = 0;
inputData: any = {};
receipt: any;
codigo;
nombre;
razon;
ruta;
mantenimiento='MANTENIMIENTO';
mantenimientocopia='MANTENIMIENTO COPIA';
trabajosrealizados;
  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController,
    public loadCtrl: LoadingController,
    private toastCtrl: ToastController,
    private printer: PrinterProvider,
    public db: SqliteDbProvider,
    public platform: Platform)  {

      this.fechavisita = this.navParams.get('fechavisita');
      this.observaciones = this.navParams.get('observaciones');
      this.trabajos = this.navParams.get('trabajos');
      this.serieTanque = this.navParams.get('serieTanque');
      this.terminado = this.navParams.get('terminado');
      this.porcentajeGasDesin = this.navParams.get('porcentajeGasDesin');
      this.porcentajeGasInsta = this.navParams.get('porcentajeGasInsta');
      this.fechavalvula = this.navParams.get('fechavalvula');

      this.codigo = this.navParams.get('codigo');
      this.ruta = this.navParams.get('ruta');
      this.nombre = this.navParams.get('nombre');
      this.razon = this.navParams.get('razon');




      console.log('observaciones', this.observaciones);
      console.log('trabajos', this.trabajos);
      console.log('serie', this.serieTanque)

for (var index in this.trabajos){
  var resultado = '';
var prueba = prueba + 1
      resultado = this.trabajos[index]  + '\n  ';
      this.trabajosrealizados = resultado;
      }


      }

      IrAMenu(){
        this.navCtrl.push(MenuMantenimientoPage,{id:this.ruta});
      }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ImprimirMantePage');
  }

  showToast(data) {
    let toast = this.toastCtrl.create({
      duration: 3000,
      message: data,
      position: 'bottom',
    });
    toast.present();
  }
  noSpecialChars(string) {
    var translate = {
      à: 'a',
      á: 'a',
      â: 'a',
      ã: 'a',
      ä: 'a',
      å: 'a',
      æ: 'a',
      ç: 'c',
      è: 'e',
      é: 'e',
      ê: 'e',
      ë: 'e',
      ì: 'i',
      í: '₡',
      î: 'i',
      ï: 'i',
      ð: 'd',
      ñ: 'n',
      ò: 'o',
      ó: 'o',
      ô: 'o',
      õ: 'o',
      ö: 'o',
      ø: 'o',
      ù: 'u',
      ú: 'u',
      û: 'u',
      ü: 'u',
      ý: 'y',
      þ: 'b',
      ÿ: 'y',
      ŕ: 'r',
      À: 'A',
      Á: 'A',
      Â: 'A',
      Ã: 'A',
      Ä: 'A',
      Å: 'A',
      Æ: 'A',
      Ç: 'C',
      È: 'E',
      É: 'E',
      Ê: 'E',
      Ë: 'E',
      Ì: 'I',
      Í: 'I',
      Î: 'I',
      Ï: 'I',
      Ð: 'D',
      Ñ: 'N',
      Ò: 'O',
      Ó: 'O',
      Ô: 'O',
      Õ: 'O',
      Ö: 'O',
      Ø: 'O',
      Ù: 'U',
      Ú: 'U',
      Û: 'U',
      Ü: 'U',
      Ý: 'Y',
      Þ: 'B',
      Ÿ: 'Y',
      Ŕ: 'R',

    },
      translate_re = /[àáâãäåæçèéêëìíîïðñòóôõöøùúûüýþßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŕŕÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÝÝÞŸŔŔ]/gim;
    return string.replace(translate_re, function (match) {
      return translate[match];
    });
  }

  print(device, data) {
    console.log('Device mac: ', device);
    console.log('Data: ', data);
    let load = this.loadCtrl.create({
      content: 'Imprimiendo...',
    });
    load.present();
    this.printer.connectBluetooth(device).subscribe(
      (status) => {
        console.log(status);
        this.printer
          .printData(this.noSpecialChars(data))
          .then((printStatus) => {
            console.log(printStatus);
            let alert = this.alertCtrl.create({
              title: 'Impreso Correctamente!',
              buttons: [
                {
                  text: 'Ok',
                  handler: () => {
                    load.dismiss();
                    this.printer.disconnectBluetooth();
                  },
                },
              ],
              enableBackdropDismiss : false
            });
            alert.present();
          })
          .catch((error) => {
            console.log(error);
            let alert = this.alertCtrl.create({
              title: 'Fue un error en la impresora, Intente de nuevo!',
              buttons: [
                {
                  text: 'Ok',
                  handler: () => {
                    load.dismiss();
                    //this.printer.disconnectBluetooth();
                  },
                },
              ],
            });
            alert.present();
          });
      },
      (error) => {
        console.log(error);
        let alert = this.alertCtrl.create({
          title:
            'Error al conectar la impresora, Intente de nuevo!',
          buttons: [
            {
              text: 'Ok',
              handler: () => {
                load.dismiss();
                //this.printer.disconnectBluetooth();
              },
            },
          ],
        });
        alert.present();
      },
    );
  }

  imprimirMante(data){
    var datos = data;
    this.prepareToPrint(datos);
  }


  prepareToPrint(data){
    // u can remove this when generate the receipt using another method
    if (!data.title) {
    data.title = this.titulo;
  }
  if (!data.subtitulo) {
    data.subtitulo = 'Cédula Jur.' + this.cedula;
  }
  if(!data.mantenimiento1){
    data.mantenimiento1 = this.mantenimiento
  }
  if (!data.subtitulo1) {
    data.subtitulo1 = 'La Lima de Cartago, Costa Rica';
  }
  if (!data.subtitulo2) {
    data.subtitulo2 = 'Teléfono: (506) 2201-60-00';
  }
  if (!data.subtitulo3) {
    data.subtitulo3 = 'servicioalcliente.cr@tomza.com';
  }

  if (!data.razon_social) {
    data.razon_social =
      'Razon Social: ' + this.razon;
  }
  if (!data.nombre) {
    data.nombre =
      'Nombre:' + this.nombre;
  }
  if (!data.codigo) {
    data.codigo =
      'Codigo: ' + this.codigo;
  }
  if (!data.factura) {
    data.factura =
      'Trabajos:'+ this.trabajos;
  }
  if (!data.ruta) {
    data.ruta =
      'Terminado: ' + this.terminado;
  }
  if (!data.fecha) {
    data.fecha =
      'Fecha Valvula : ' + this.fechavalvula;
  }
  if (!data.pago) {
    data.pago=
      'Serie: ' + this.serieTanque;
  }

  if (!data.Total) {
    data.Total =
      'Observaciones : ' + this.observaciones;
  }
  if (!data.firma1) {
    data.firma1 =
      'Firma Recibido';
  }
  if (!data.firma2) {
    data.firma2 =
      'Nombre';
  }
  if (!data.sello) {
    data.sello =
      'Sellos';
  }

  if (!data.ced) {
    data.ced =
      '# Cedula:';
  }




  let receipt = '';
  receipt += commands.HARDWARE.HW_INIT;
  receipt += commands.TEXT_FORMAT.TXT_ALIGN_CT;
  receipt += data.mantenimiento1;
  receipt += commands.EOL;
  receipt += commands.TEXT_FORMAT.TXT_2WIDTH;
  receipt += commands.TEXT_FORMAT.TXT_ALIGN_CT;
  receipt += data.title.toUpperCase();
  receipt += commands.EOL;//salto de linea
  receipt += commands.TEXT_FORMAT.TXT_NORMAL;
  receipt += commands.HORIZONTAL_LINE.HR_58MM;
  receipt += commands.EOL;
  receipt += commands.HORIZONTAL_LINE.HR2_58MM;
  receipt += commands.EOL;
  receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
  receipt += commands.EOL;
  receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
  receipt += commands.EOL;
  receipt += data.subtitulo;
  receipt += commands.EOL;
  receipt += data.subtitulo1;
  receipt += commands.EOL;
  receipt += data.subtitulo2;
  receipt += commands.EOL;
  receipt += data.subtitulo3;
  receipt += commands.EOL;
  receipt += commands.HORIZONTAL_LINE.HR2_58MM;
  receipt += commands.EOL;
  receipt += commands.EOL;
  receipt += commands.EOL;
  receipt += data.razon_social;
  receipt += commands.EOL;
  receipt += data.nombre;
  receipt += commands.EOL;
  receipt += data.codigo;
  receipt += commands.EOL;
  receipt += data.ruta;
  receipt += commands.EOL;
  receipt += data.pago;
  receipt += commands.EOL;
  receipt += data.fecha;
  receipt += commands.EOL;
  receipt += data.Total;
  receipt += commands.EOL;
  receipt += commands.EOL;
  receipt += commands.HORIZONTAL_LINE.HR2_58MM;
  receipt += commands.TEXT_FORMAT.TXT_NORMAL;
  receipt += commands.EOL;
  receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
  receipt += data.factura;
  receipt += commands.EOL;
  receipt += commands.EOL;
  receipt += commands.EOL;

  receipt += commands.EOL;
  receipt += commands.EOL;
  receipt += commands.TEXT_FORMAT.TXT_NORMAL;
  receipt += commands.EOL;
  receipt += commands.EOL;
  receipt += commands.EOL;
  receipt += commands.EOL;
  receipt += commands.TEXT_FORMAT.TXT_NORMAL;
  receipt += data.firma1;
  receipt += commands.EOL;
  receipt += commands.EOL;
  receipt += commands.EOL;
  receipt += commands.EOL;
  receipt += commands.EOL;
  receipt += commands.EOL;
  receipt += commands.TEXT_FORMAT.TXT_NORMAL;
  receipt += data.ced;
  receipt += commands.EOL;
  receipt += commands.EOL;
  receipt += commands.EOL;
  receipt += commands.EOL;
  receipt += commands.EOL;
  receipt += commands.EOL;
  receipt += data.firma2;
  receipt += commands.EOL;
  receipt += commands.EOL;
  receipt += commands.EOL;
  receipt += commands.EOL;
  receipt += commands.TEXT_FORMAT.TXT_NORMAL;
  receipt += commands.EOL;
  receipt += commands.EOL;
  receipt += commands.EOL;
  receipt += commands.EOL;
  receipt += data.sello;
  receipt += commands.EOL;
  receipt += commands.EOL;
  receipt += commands.EOL;
  //secure space on footer
  receipt += commands.EOL;

  //this.receipt = receipt;
  this.mountAlertBt(receipt);
}

impresionOriginal(data){
  this.contadorOriginal+=1;

  var datos = data;
  this.prepareToPrint(datos);

 }

 prepareToPrintCopia(data){
  // u can remove this when generate the receipt using another method
  if (!data.title) {
  data.title = this.titulo;
}
if (!data.subtitulo) {
  data.subtitulo = 'Cédula Jur.' + this.cedula;
}
if(!data.mantenimiento1){
  data.mantenimiento1 = 'Mantenimiento Copia';
}
if (!data.subtitulo1) {
  data.subtitulo1 = 'La Lima de Cartago, Costa Rica';
}
if (!data.subtitulo2) {
  data.subtitulo2 = 'Teléfono: (506) 2201-60-00';
}
if (!data.subtitulo3) {
  data.subtitulo3 = 'servicioalcliente.cr@tomza.com';
}

if (!data.razon_social) {
  data.razon_social =
    'Razon Social: ' + this.razon;
}
if (!data.nombre) {
  data.nombre =
    'Nombre:' + this.nombre;
}
if (!data.codigo) {
  data.codigo =
    'Codigo: ' + this.codigo;
}
if (!data.factura) {
  data.factura =
    'Trabajos:'+ this.trabajos;
}
if (!data.ruta) {
  data.ruta =
    'Terminado: ' + this.terminado;
}
if (!data.fecha) {
  data.fecha =
    'Fecha: ' + this.fechavalvula;
}
if (!data.pago) {
  data.pago=
    'Serie: ' + this.serieTanque;
}

if (!data.Total) {
  data.Total =
    'Observaciones : ' + this.observaciones;
}
if (!data.firma1) {
  data.firma1 =
    'Firma Recibido';
}
if (!data.firma2) {
  data.firma2 =
    'Nombre';
}
if (!data.sello) {
  data.sello =
    'Sellos';
}

if (!data.ced) {
  data.ced =
    '# Cedula:';
}




let receipt = '';
receipt += commands.HARDWARE.HW_INIT;
receipt += commands.TEXT_FORMAT.TXT_ALIGN_CT;
receipt += data.mantenimiento1;
receipt += commands.EOL;
receipt += commands.TEXT_FORMAT.TXT_2WIDTH;
receipt += commands.TEXT_FORMAT.TXT_ALIGN_CT;
receipt += data.title.toUpperCase();
receipt += commands.EOL;//salto de linea
receipt += commands.TEXT_FORMAT.TXT_NORMAL;
receipt += commands.HORIZONTAL_LINE.HR_58MM;
receipt += commands.EOL;
receipt += commands.HORIZONTAL_LINE.HR2_58MM;
receipt += commands.EOL;
receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
receipt += commands.EOL;
receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
receipt += commands.EOL;
receipt += data.subtitulo;
receipt += commands.EOL;
receipt += data.subtitulo1;
receipt += commands.EOL;
receipt += data.subtitulo2;
receipt += commands.EOL;
receipt += data.subtitulo3;
receipt += commands.EOL;
receipt += commands.HORIZONTAL_LINE.HR2_58MM;
receipt += commands.EOL;
receipt += commands.EOL;
receipt += commands.EOL;
receipt += data.razon_social;
receipt += commands.EOL;
receipt += data.nombre;
receipt += commands.EOL;
receipt += data.codigo;
receipt += commands.EOL;
receipt += data.ruta;
receipt += commands.EOL;
receipt += data.pago;
receipt += commands.EOL;
receipt += data.fecha;
receipt += commands.EOL;
receipt += data.Total;
receipt += commands.EOL;
receipt += commands.EOL;
receipt += commands.HORIZONTAL_LINE.HR2_58MM;
receipt += commands.TEXT_FORMAT.TXT_NORMAL;
receipt += commands.EOL;
receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
receipt += data.factura;
receipt += commands.EOL;
receipt += commands.EOL;
receipt += commands.EOL;

receipt += commands.EOL;
receipt += commands.EOL;
receipt += commands.TEXT_FORMAT.TXT_NORMAL;
receipt += commands.EOL;
receipt += commands.EOL;
receipt += commands.EOL;
receipt += commands.EOL;
receipt += commands.TEXT_FORMAT.TXT_NORMAL;
receipt += data.firma1;
receipt += commands.EOL;
receipt += commands.EOL;
receipt += commands.EOL;
receipt += commands.EOL;
receipt += commands.EOL;
receipt += commands.EOL;
receipt += commands.TEXT_FORMAT.TXT_NORMAL;
receipt += data.ced;
receipt += commands.EOL;
receipt += commands.EOL;
receipt += commands.EOL;
receipt += commands.EOL;
receipt += commands.EOL;
receipt += commands.EOL;
receipt += data.firma2;
receipt += commands.EOL;
receipt += commands.EOL;
receipt += commands.EOL;
receipt += commands.EOL;
receipt += commands.TEXT_FORMAT.TXT_NORMAL;
receipt += commands.EOL;
receipt += commands.EOL;
receipt += commands.EOL;
receipt += commands.EOL;
receipt += data.sello;
receipt += commands.EOL;
receipt += commands.EOL;
receipt += commands.EOL;
//secure space on footer
receipt += commands.EOL;

//this.receipt = receipt;
this.mountAlertBt(receipt);
}

//para imprimir la copia de la factura


  // prepareToPrint(data){
  //      // u can remove this when generate the receipt using another method

  //     if (!data.subtitulo) {
  //       data.subtitulo = 'Cédula Jur.' + this.cedula;
  //     }
  //     if (!data.subtitulo1) {
  //       data.subtitulo1 = 'La Lima de Cartago, Costa Rica';
  //     }
  //     if (!data.subtitulo2) {
  //       data.subtitulo2 = 'Teléfono: (506) 2201-60-00';
  //     }
  //     if (!data.subtitulo3) {
  //       data.subtitulo3 = 'servicioalcliente.cr@tomza.com';
  //     }

  //     if(!data.consecutivoFactura){
  //       data.consecutivoFactura = 'Consecutivo:'+ this.trabajos;
  //     }
  //     if(!data.claveFactura){
  //       data.claveFactura = 'Clave:'+ this.observaciones;
  //     }

  //     if (!data.ruta) {
  //       data.ruta =
  //         'Ruta: ' + this.fechavisita;
  //     }
  //     if (!data.fecha) {
  //       data.fecha =
  //         'Fecha: ' + this.fechacompleta;
  //     }

  //     if (!data.firma1) {
  //       data.firma1 =
  //         'Firma Recibido';
  //     }
  //     if (!data.firma2) {
  //       data.firma2 =
  //         'Nombre';
  //     }
  //     if (!data.sello) {
  //       data.sello =
  //         'Sellos';
  //     }


  //     let receipt = '';
  //     receipt += commands.HARDWARE.HW_INIT;
  //     receipt += commands.EOL;
  //     receipt += commands.TEXT_FORMAT.TXT_ALIGN_CT;
  //     receipt += data.original;
  //     receipt += commands.EOL;
  //     receipt += commands.TEXT_FORMAT.TXT_2WIDTH;
  //     receipt += commands.TEXT_FORMAT.TXT_ALIGN_CT;
  //    // receipt += data.title.toUpperCase();
  //     receipt += commands.EOL;//baba de linea
  //     receipt += commands.TEXT_FORMAT.TXT_NORMAL;
  //     receipt += commands.HORIZONTAL_LINE.HR_58MM;
  //     receipt += commands.EOL;
  //     receipt += commands.HORIZONTAL_LINE.HR2_58MM;
  //     receipt += commands.EOL;
  //     receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
  //     receipt += commands.EOL;
  //     receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
  //     //receipt += data.comprobante;
  //     receipt += data.tipoTiquete;
  //     receipt += commands.EOL;
  //     receipt += data.subtitulo;
  //     receipt += commands.EOL;
  //     receipt += data.subtitulo1;
  //     receipt += commands.EOL;
  //     receipt += data.subtitulo2;
  //     receipt += commands.EOL;
  //     receipt += data.subtitulo3;
  //     receipt += commands.EOL;
  //     receipt += commands.HORIZONTAL_LINE.HR2_58MM;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += data.consecutivoFactura;
  //     receipt += commands.EOL;
  //     receipt += data.claveFactura;
  //     receipt += commands.EOL;
  //     receipt += data.facnumero;
  //     receipt += commands.EOL;
  //     receipt += data.ruta;
  //     receipt += commands.EOL;
  //     receipt += data.fecha;
  //     receipt += commands.EOL;
  //     receipt += data.razon_social;
  //     receipt += commands.EOL;
  //     receipt += data.nombre;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.HORIZONTAL_LINE.HR2_58MM;
  //     receipt += commands.TEXT_FORMAT.TXT_NORMAL;
  //     receipt += commands.EOL;
  //     receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.TEXT_FORMAT.TXT_BOLD_ON;

  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.TEXT_FORMAT.TXT_NORMAL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.TEXT_FORMAT.TXT_BOLD_ON;

  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.TEXT_FORMAT.TXT_NORMAL;
  //     receipt += data.firma1;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.TEXT_FORMAT.TXT_NORMAL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += data.firma2;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.TEXT_FORMAT.TXT_NORMAL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += data.sello;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     //secure space on footer
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += data.footer;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     //receipt += data.leyenda;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     receipt += commands.EOL;
  //     //this.receipt = receipt;
  //     this.mountAlertBt(receipt);

  //   }

    mountAlertBt(data) {
      this.receipt = data;
      let alert = this.alertCtrl.create({
        title: 'Seleccione su Impresora',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
          },
          {
            text: 'Seleccionar Impresora',
            handler: (device) => {
              if (!device) {
                this.showToast('Seleccionar Impresora');
                return false;
              }
              console.log(device);
              this.print(device, this.receipt);
            },
          },
        ],
        enableBackdropDismiss : false
      });
      this.printer
        .enableBluetooth()
        .then(() => {
          this.printer
            .searchBluetooth()
            .then((devices) => {
              devices.forEach((device) => {
                console.log('Devices: ', JSON.stringify(device));
                alert.addInput({
                  name: 'printer',
                  value: device.address,
                  label: device.name,
                  type: 'radio',
                });
              });
              alert.present();
            })
            .catch((error) => {
              console.log(error);
              this.showToast(
                'There was an error connecting the printer, Intente de nuevo!',
              );
              this.mountAlertBt(this.receipt);
            });
        })
        .catch((error) => {
          console.log(error);
          this.showToast('Error al activar el Bluetooth, Intente de nuevo!');
          this.mountAlertBt(this.receipt);
        });




}}
