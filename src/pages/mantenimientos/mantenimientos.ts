import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Segment } from 'ionic-angular';
import { SqliteDbProvider } from '../../providers/sqlite-db/sqlite-db';
import { InformacionApiProvider } from '../../providers/informacion-api/informacion-api';
import { ActulizarMantenimientoPage } from '../actulizar-mantenimiento/actulizar-mantenimiento';

/**
 * Generated class for the MantenimientosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-mantenimientos',
  templateUrl: 'mantenimientos.html',
})
export class MantenimientosPage {

  @ViewChild(Segment) Segment: Segment;

  mantenimientosSQLite = [];
  mantenimientosNoTerminados = [];
  mantenimientosTerminados = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public db: SqliteDbProvider, public info: InformacionApiProvider ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MantenimientosPage');
    this.obtenerMantenimientos();
    this.obtenerMantenimientosNoTerminados();
    this.obtenerMantenimientosTerminados();

    this.Segment.value = 'terminados';

  }

  //obtengo los mantenimientos para desplegarlos en lista
  obtenerMantenimientos(){
    this.db.obtenerMantenimientos().then((data) => {

      this.mantenimientosSQLite= data;
      console.log(this.mantenimientosSQLite);

    },(error) =>{
      console.log(error);
    })
  }

  obtenerMantenimientosNoTerminados(){
    
      this.db.getMantenimientosNoTerminados().then((data) => {
  
        this.mantenimientosNoTerminados = data;
        console.log('Mantenimienos no terminados:',this.mantenimientosNoTerminados);
  
      },(error) =>{
        console.log(error);
      })
  }

  obtenerMantenimientosTerminados(){
    
    this.db.getMantenimientosTerminados().then((data) => {

      this.mantenimientosTerminados = data;
      console.log('Mantenimienos terminados:',this.mantenimientosTerminados);

    },(error) =>{
      console.log(error);
    })
}



  enviarMantenimiento(mantenimiento){


    var trabajo = {
      horaInicio: mantenimiento.horaInicio,
      fechaVisita: mantenimiento.fechavisita,
      observaciones: mantenimiento.observaciones,
      serieTanque: mantenimiento.serieTanque,
      foto1: mantenimiento.foto1,
      foto2: mantenimiento.foto2,
      trabajoRealizado: mantenimiento.trabajorealizado,
      fechaValvula: mantenimiento.fechavalvula,
      gasInstalacion: mantenimiento.porcentajeGasInsta,
      gasDesinstalacion: mantenimiento.porcentajeGasDesin,
      terminado: mantenimiento.terminado,
      horaFin: mantenimiento.horaFin
    }

    //console.log(fac);

    if(mantenimiento.serie !== ''){
      this.info.enviarMantenimientos(trabajo).then(data => {
        console.log('Enviado con Exito');
        console.log(data);

      }, err => {
        console.log(err);
       console.log("Error de envio sin internet");
      })
    }
  }

  irAEditarMantenimiento(mantenimiento:any){
    this.navCtrl.push(ActulizarMantenimientoPage, mantenimiento);
  }

}
