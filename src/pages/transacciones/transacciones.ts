import { SqliteDbProvider } from './../../providers/sqlite-db/sqlite-db';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { MenuGranelPage } from '../menu-granel/menu-granel';


/**
 * Generated class for the TransaccionesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-transacciones',
  templateUrl: 'transacciones.html',
})
export class TransaccionesPage {

  transaccion;
  tipo:AbstractControl;
  documento:AbstractControl;
  banco:AbstractControl;
  factura;
  fecha = new Date();
  ruta;
  id;
  facnumero;
  monto:AbstractControl;
  objDate: Date = new Date();
  formatoFecha = '';
  formgroup: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public db: SqliteDbProvider,
    public aCtrl: AlertController,public formbuilder: FormBuilder) {

      this.factura = this.navParams.get('facnumero');
      this.ruta = this.navParams.get('ruta');
      console.log(this.factura);

      this.formgroup =  formbuilder.group({
        tipo:['', Validators.required],
        documento:['', Validators.required],
        monto:['', Validators.required],
        banco: ['', Validators.required],

    });

    this.tipo = this.formgroup.controls['tipo']
    this.banco =  this.formgroup.controls['banco'];
    this.monto = this.formgroup.controls['monto'];
    this.documento = this.formgroup.controls['documento'];

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TransaccionesPage');
  }

    //metodo que llama al metodo guardarFacturas de DatabaseProvider y armo el objeto factura para guardarlo
    guardarTransacciones(){
      var fecha =  new Date();
      var day;

      var dia = fecha.getDate();
      var mes =  fecha.getMonth();
      var year =  fecha.getFullYear();

      var horas = fecha.getHours();
      var minut = fecha.getMinutes();
      var segun = fecha.getSeconds();
      var minutos;
      var segundos;
      var meses = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

      if(minut <= 9){
            minutos = '0'+minut.toString();
      }else{
            minutos = minut.toString();
      }

      if(segun <= 9){
          segundos = '0'+segun.toString();
      }else{
          segundos = segun.toString();
      }

      if(dia <= 9){
        day = '0'+dia.toString();
      }else{
        day = dia.toString();
      }

      var fechaCompleta = day+'-'+meses[mes]+'-'+year+' '+horas+':'+minutos+':'+segundos;

      let transaccion = {
        factura: this.factura,
        ruta: this.ruta,
        tipo: this.formgroup.value.tipo,
        fecha: fechaCompleta,
        monto: this.formgroup.value.monto,
        banco: this.formgroup.value.banco,
        documento : this.formgroup.value.documento,
      }

      if (this.ruta !== '') {
        this.db.guardarTransaccionesSQLite(transaccion).then(data => {
          console.log(data);
        }).catch( error => { console.error(error) })
      }
    }

    IrAMenu(){
      this.navCtrl.push(MenuGranelPage,{id:this.ruta});
    }


presentAlert() {
  let alert = this.aCtrl.create({
    title: 'Transacción guardados correctamente',

    buttons: ['Aceptar']
  });
  alert.present();
  this.IrAMenu();
}

mensajeDeSeguridad() {
  let alert = this.aCtrl.create({
    title: '¿Está completamente seguro de que completó toda la información?',
    subTitle: 'Verifique si no esta seguro!!',
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
        }
      }, {
        text: 'Si',
        handler: () => {
          console.log('Confirm Okay');
          this.guardarTransacciones();
          this.presentAlert();
         
        }
      }
    ]
  });
  alert.present();
  
}

}
