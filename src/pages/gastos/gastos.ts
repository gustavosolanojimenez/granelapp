import { SqliteDbProvider } from './../../providers/sqlite-db/sqlite-db';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';

/**
 * Generated class for the GastosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-gastos',
  templateUrl: 'gastos.html',
})


export class GastosPage {

  factura:AbstractControl;
  facnumero;
  tipo:AbstractControl;
  ruta;
  fecha= Date.now();
  monto:AbstractControl;
  banco;

  //fecha
objDate: Date = new Date();
formatoFecha = '';
formgroup: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public db:SqliteDbProvider ,
    public aCtrl: AlertController,public formbuilder: FormBuilder) {

      this.ruta = this.navParams.get('id');
      this.formgroup =  formbuilder.group({
        tipo:['', Validators.required],
        factura:['', Validators.required],
        monto:['', Validators.required]
      });

      this.tipo = this.formgroup.controls['tipo'];
      this.monto = this.formgroup.controls['monto'];
      this.factura= this.formgroup.controls['factura'];
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad GastosPage');
  }
  //metodo que llama al metodo guardarFacturas de DatabaseProvider y armo el objeto factura para guardarlo
  guardarGastos() {
    var fecha =  new Date();
    var day;

    var dia = fecha.getDate();
    var mes =  fecha.getMonth();
    var year =  fecha.getFullYear();

    var horas = fecha.getHours();
    var minut = fecha.getMinutes();
    var segun = fecha.getSeconds();
    var minutos;
    var segundos;
    var meses = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

    if(minut <= 9){
          minutos = '0'+minut.toString();
    }else{
          minutos = minut.toString();
    }

    if(segun <= 9){
        segundos = '0'+segun.toString();
    }else{
        segundos = segun.toString();
    }

    if(dia <= 9){
      day = '0'+dia.toString();
    }else{
      day = dia.toString();
    }

    var fechaCompleta = day+'-'+meses[mes]+'-'+year+' '+horas+':'+minutos+':'+segundos;

    let transaccion = {
      factura: '2',
      ruta: this.ruta,
      tipo: this.formgroup.value.tipo,
      fecha: fechaCompleta,
      monto: this.formgroup.value.monto,
      banco: '1',
      documento: this.formgroup.value.factura
    }

    if (this.ruta !== '') {
      this.db.guardarTransaccionesSQLite(transaccion).then(data => {
        console.log(data);
      }).catch(error => { console.error(error) })
    }
  }

  // IrAMenu() {
  //   this.navCtrl.push(MenuPage, { id: this.ruta });
  // }

  presentAlert() {
    let alert = this.aCtrl.create({
      title: 'Gasto guardados correctamente',

      buttons: ['Aceptar']
    });
    alert.present();
    //this.IrAMenu();
  }

  mensajeDeSeguridad() {
    let alert = this.aCtrl.create({
      title: '¿Está completamente seguro de que completó toda la información?',
      subTitle: 'Verifique si no esta seguro!!',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: () => {
            console.log('Confirm Okay');
            this.guardarGastos();
            this.presentAlert();
          }
        }
      ]
    });
    alert.present();
  }

}
