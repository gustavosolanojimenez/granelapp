import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RemesaPage } from './remesa';

@NgModule({
  declarations: [
    RemesaPage,
  ],
  imports: [
    IonicPageModule.forChild(RemesaPage),
  ],
})
export class RemesaPageModule {}
