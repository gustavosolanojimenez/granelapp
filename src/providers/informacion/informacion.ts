import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { enviroment } from '../../env/environment';
enviroment
/*
  Generated class for the InformacionProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class InformacionProvider {
  apiURL = enviroment.url;
  //apiURL = 'http://174.138.38.52:3030/api/';
  //apiURL = 'https://api-tomza.herokuapp.com/api/';

  constructor(public http: HttpClient, public aCtrl: AlertController) {
    console.log('Informaion provider');
  }

  //obtener las rutas desde el api
  obtenerRutasAPI() {
    return new Promise(resolve => {
      this.http.get(`${this.apiURL}rutas`).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);

      });

    })

  }
  //metodo para guardar Descargas
  enviarDescargas(descarga: any) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.apiURL}facturas`, descarga).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
        this.mensajeNoEnvio();
      });
    });
  }

  //metodo para guardar las facturas en la base de datos del API
  enviarFaturas(factura: any) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.apiURL}facturas`, factura).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
        this.mensajeNoEnvio();
      });
    });
  }

  //metodo para guardar el detalle de las facturas en la base de datos del API
  enviarDetalles(detalle: any) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.apiURL}facqueuets`, detalle).subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log(err);
      })
    })
  }

  //metodo para guardar el cliente nuevo en la base de datos de la nube
  enviarClienteNuevo(cliente: any) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.apiURL}clientes`, cliente).subscribe(data => {
        resolve(data);
        console.log("Este es el cliente nuevo enviado:", data);
      }, err => {
        console.log(err);
        this.mensajeNoEnvio();
      })
    })
  }

  //metodo para guardar los motivo de no compra dentro de la base de datos en la nube
  enviarMotivosNoCompra(motivo: any) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.apiURL}noCompras`, motivo).subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log(err);
      })
    })
  }

  //metodo para guardar las transacciones dentro de la base de datos en la nube
  enviarTransacciones(transaccion: any) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.apiURL}trachks`, transaccion).subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log(err);
      })
    })
  }

  //metodo para guardar los billetes dentro de la base de datos en la nube
  enviarBilletes(billete: any) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.apiURL}billetes`, billete).subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log(err);
      })
    })
  }

  //metodo para guardar los gastos dentro de la base de datos en la nube
  enviarGastos(gasto: any) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.apiURL}gastos`, gasto).subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log(err);
      })
    })
  }

  //mensaje que se despliega si no se envia
  mensajeNoEnvio() {
    let alert = this.aCtrl.create({
      title: 'No hay internet, intentalo cuando tengas internet',
      buttons: ['Aceptar']
    });
    alert.present();
  }

  //metodo que actualiza la ruta para tener el consecutivo correcto para el siguiente dia
  actualizarRutaConConsecutivo(ruta) {
    return this.http.put(`${this.apiURL}rutas/${ruta.id}`, ruta);
  }

  enviarClaves(clave: any) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.apiURL}claves`, clave).subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log(err);
      })
    })
  }

  //metodo para guardar los detallestanque en la base de datos de TOMZA
  enviarDetallesTanques(detalleTanque: any) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.apiURL}detalleTanques`, detalleTanque).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

}
