import { MenuGranelPage } from './../menu-granel/menu-granel';
import { SqliteDbProvider } from './../../providers/sqlite-db/sqlite-db';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';

/**
 * Generated class for the BilletesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-billetes',
  templateUrl: 'billetes.html',
})
export class BilletesPage {

  total50mil; total20mil; total10mil; total5mil; total2mil; totalmil; totalQuinientos; totalCien; totalCincuenta; totalVeinticinco; totalDiez; totalCinco; totalDolares;
granTotal;


billetesSQLite: any = [];
  cincuentaMil='';
  veinteMil='';
  diezMil='';
  cincoMil='';
  dosMil='';
  mil='';
  quinientos='';
  cien='';
  cincuenta='';
  veinticinco='';
  diez='';
  cinco='';
  dolares='';
  vale = '';
  ruta;
  fecha = new Date();
  idRuta;
  //fecha
  objDate: Date = new Date();
  formatoFecha = '';

//Datos para la ruta
codRuta; nombreRuta; unidadNegocioRuta; cediRuta; descripcionRuta; consecutivoRuta; cerosTOM; rutaEnUso;

  constructor(public navCtrl: NavController, public navParams: NavParams, public db: SqliteDbProvider, public aCtrl: AlertController) {
    this.idRuta = this.navParams.get('id');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BilletesPage');
    this.obtenerBilletesDeSQLite();
    this.obtenerRutaEnUso();
  }


  addBillete(){
    var fecha =  new Date();
    var day;

    var dia = fecha.getDate();
    var mes =  fecha.getMonth();
    var year =  fecha.getFullYear();

    var horas = fecha.getHours();
    var minut = fecha.getMinutes();
    var segun = fecha.getSeconds();
    var minutos;
    var segundos;
    var meses = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

    if(minut <= 9){
          minutos = '0'+minut.toString();
    }else{
          minutos = minut.toString();
    }

    if(segun <= 9){
        segundos = '0'+segun.toString();
    }else{
        segundos = segun.toString();
    }

    if(dia <= 9){
      day = '0'+dia.toString();
    }else{
      day = dia.toString();
    }

    var fechaCompleta = day+'-'+meses[mes]+'-'+year+' '+horas+':'+minutos+':'+segundos;

    if(this.cincuentaMil === ''){
      this.cincuentaMil = '0';
    }

    if(this.veinteMil === ''){
      this.veinteMil = '0';
    }

    if(this.diezMil === ''){
      this.diezMil = '0';
    }

    if(this.cincoMil === ''){
      this.cincoMil = '0';
    }

    if(this.dosMil === ''){
      this.dosMil = '0';
    }

    if(this.mil === ''){
      this.mil = '0';
    }

    if(this.quinientos === ''){
      this.quinientos = '0';
    }

    if(this.cien === ''){
      this.cien = '0';
    }

    if(this.cincuenta === ''){
      this.cincuenta = '0';
    }

    if(this.veinticinco === ''){
      this.veinticinco= '0';
    }

    if(this.diez === ''){
      this.diez = '0';
    }

    if(this.cinco === ''){
      this.cinco = '0';
    }

    if(this.dolares === ''){
      this.dolares = '0';
    }

    if(this.vale === ''){
      this.vale = '0';
    }

    let billete ={
      cincuentaMil: this.cincuentaMil,
      veinteMil : this.veinteMil,
      diezMil: this.diezMil,
      cincoMil: this.cincoMil,
      dosMil: this.dosMil,
      mil: this.mil,
      quinientos : this.quinientos,
      cien: this.cien,
      cincuenta: this.cincuenta,
      veinticinco: this.veinticinco,
      diez: this.diez,
      cinco: this.cinco,
      dolares: this.dolares,
      vale: this.vale,
      ruta: this.idRuta,
      fecha: fechaCompleta

    }

    if(this.billetesSQLite.length > 0){
      this.db.eliminarBilletesSQLite();
      if(this.ruta !== ''){
        this.db.guardarBillete(billete).then(data =>{
          console.log(data);
          console.log("Billetes guardados con exito")
        }).catch( error => { console.error(error) })
      }
    }else{

      if(this.ruta !== ''){
        this.db.guardarBillete(billete).then(data =>{
          console.log(data);
          console.log("Billetes guardados con exito")
        }).catch( error => { console.error(error) })
      }
    }
  }

  obtenerBilletesDeSQLite(){
    this.db.getBilletesSQLite().then((data) => {
      //allFacturasLoadingController.dismiss();
      this.billetesSQLite = data;
      this.total50mil= this.billetesSQLite.reduce((a, b) => a + (b.cincuentaMil * 50000), 0);
      this.total20mil= this.billetesSQLite.reduce((a, b) => a + (b.veinteMil * 20000), 0);
      this.total10mil= this.billetesSQLite.reduce((a, b) => a + (b.diezMil * 10000), 0);
      this.total5mil= this.billetesSQLite.reduce((a, b) => a + (b.cincoMil * 5000), 0);
      this.total2mil= this.billetesSQLite.reduce((a, b) => a + (b.dosMil * 2000), 0);
      this.totalmil= this.billetesSQLite.reduce((a, b) => a + (b.mil * 1000), 0);
      this.totalQuinientos= this.billetesSQLite.reduce((a, b) => a + (b.quinientos * 500), 0);
      this.totalCien= this.billetesSQLite.reduce((a, b) => a + (b.cien* 100), 0);
      this.totalCincuenta= this.billetesSQLite.reduce((a, b) => a + (b.cincuenta * 50), 0);
      this.totalVeinticinco= this.billetesSQLite.reduce((a, b) => a + (b.veinticinco * 25), 0);
      this.totalDiez = this.billetesSQLite.reduce((a, b) => a + (b.diez * 10), 0);
      this.totalCinco= this.billetesSQLite.reduce((a, b) => a + (b.cinco * 5), 0);

      this.granTotal = this.total50mil + this.total10mil + this.total20mil +this.total5mil + this.total2mil + this.totalmil + this.totalQuinientos + this.totalCien+ this.totalCincuenta + this.totalVeinticinco + this.totalDiez + this.totalCinco;

      console.log(this.billetesSQLite);
    }, (error) => {
      console.log(error);
    })
  }



  //metodo que obtiene la ruta en uso para utilizar sus datos en la actualizacion del consecutivo
  //y armar el TOM para cada factura
  obtenerRutaEnUso(){
    this.db.getRutaUsada(parseInt(this.ruta)).then((data) => {
      this.rutaEnUso = data;
      console.log(this.rutaEnUso);
      this.codRuta =  this.rutaEnUso.cod;
      this.nombreRuta = this.rutaEnUso.nombre;


      //armo el TOM para despues usarlo en la factura

      console.log(this.rutaEnUso);
    },(error) =>{
      console.log(error);
    })
  }


IrAMenu(){
  this.navCtrl.push(MenuGranelPage,{id:this.idRuta});
}


presentAlert() {
  let alert = this.aCtrl.create({
    title: 'Billetes guardados correctamente',

    buttons: ['Aceptar']
  });
  alert.present();
  this.IrAMenu();
}

mensajeDeSeguridad() {
  let alert = this.aCtrl.create({
    title: '¿Está completamente seguro de que completó toda la información?',
    subTitle: 'Verifique si no esta seguro!!',
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
        }
      }, {
        text: 'Si',
        handler: () => {
          console.log('Confirm Okay');
          this.addBillete();
          this.presentAlert();
        }
      }
    ]
  });
  alert.present();
}


}
