import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { enviroment } from '../../env/environment';

/*
  Generated class for the InformacionApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class InformacionApiProvider {
  apiURL= enviroment.url;
 // apiURL = 'http://174.138.38.52:3030/api/';
   //apiURL = 'https://api-tomza.herokuapp.com/api/';


  constructor(public http: HttpClient, public aCtrl: AlertController) {
    console.log('Hello InformacionApiProvider Provider');
  }

  //metodo para guardar los mantenimientos en la base de datos de TOMZA
  enviarMantenimientos(mantenimiento:any){
    return new Promise((resolve,reject) => {
      this.http.post(`${this.apiURL}mantenimientos`,mantenimiento).subscribe(data => {
        resolve(data);
      },err => {
        console.log(err);
      });
    });
  }

  

  //metodo para guardar los tanques en la base de datos de TOMZA
  enviarTanques(tanque:any){
    return new Promise((resolve,reject) => {
      this.http.post(`${this.apiURL}tanques`,tanque).subscribe(data => {
        resolve(data);
      },err => {
        this.mensajeNoEnvio();
        console.log(err);
      });
    });
  }

  mensajeNoEnvio(){
    let alert = this.aCtrl.create({
      title: 'No hay internet, intentalo cuando tengas internet',
      buttons: ['Aceptar']
    });
    alert.present();
  }


  //metodo para guardar los detallestanque en la base de datos de TOMZA
  enviarDetallesTanques(detalleTanque:any){
    return new Promise((resolve,reject) => {
      this.http.post(`${this.apiURL}detalleTanques`,detalleTanque).subscribe(data => {
        resolve(data);
      },err => {
        console.log(err);
      });
    });
  }

  //metodo que actualiza la ruta para tener el consecutivo correcto para el siguiente dia
 actualizarRutaConConsecutivo(ruta){
  return this.http.put(`${this.apiURL}rutas/${ruta.id}`,ruta);
 }

 

}
