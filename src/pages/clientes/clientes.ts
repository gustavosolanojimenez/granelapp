import { UbicacionPage } from './../ubicacion/ubicacion';
import { FacturacionPage } from './../facturacion/facturacion';
import { SqliteDbProvider } from './../../providers/sqlite-db/sqlite-db';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Segment, AlertController, LoadingController } from 'ionic-angular';


/**
 * Generated class for the ClientesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-clientes',
  templateUrl: 'clientes.html',
})
export class ClientesPage {
 //variables a utilizar
 @ViewChild(Segment) Segment: Segment;

 clientesSQLite:any = [];
 clientesRolSQLite:any = [];

 facturas:any = [];
 noCompras:any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,public db:SqliteDbProvider,public lCtrl: LoadingController,public aCtrl: AlertController) {
    this.obtenerClientesDeSQLite();
    this.obtenerMotivosDeSQLite();
    this.obtenerFacturasDeSQLite();
    this.obtenerRolDelDia();
  }

  ionViewDidLoad() {
    //this.obtenerClientesDeSQLite();
    this.Segment.value = 'rol';
    //this.obtenerRolDelDia();
  }

  //metodo que obtiene los clientes almacenados en SQLite utilizando el metodo de DatabaseProvider
  obtenerClientesDeSQLite(){

    let allClientesLoadingController = this.lCtrl.create({
      content: "Obteniendo Clientes de la Base de Datos"
    });

    allClientesLoadingController.present();

    this.db.getClientesSQLite().then((data) => {
      allClientesLoadingController.dismiss();
      this.clientesSQLite = data;
      //console.log(this.clientesSQLite);
    }).catch( error => { console.error(error) });

  }

  obtenerRolDelDia(){

    var dia;
    var fecha = new Date();
    var dias = ['Do','L','K','M','J','V','S'];


    dia =  fecha.getDay();

    console.log('Este es el dia',dia);

    if(dia === 1){

      let allClientesLoadingController = this.lCtrl.create({
        content: "Obteniendo Clientes de la Base de Datos"
      });

      allClientesLoadingController.present();

      this.db.getRolLunesCientesSQLite().then((data) => {
        allClientesLoadingController.dismiss();
        this.clientesRolSQLite = data;
        console.log(data);
        for(var index in this.facturas){

          for(var client in this.clientesRolSQLite){
            if(this.facturas[index].codigo === this.clientesRolSQLite[client].codigo){
              console.log("Esta en la lista");
              this.actualizarHizoFactura(this.clientesRolSQLite[client].codigo);
            }
          }

        }

        for(var index in this.noCompras){
          for(var client in this.clientesRolSQLite){
            if(this.noCompras[index].codigoCliente === this.clientesRolSQLite[client].codigo){
              console.log("Esta en la lista");
              this.actualizarHizoMotivo(this.clientesRolSQLite[client].codigo);
            }
          }
        }
      }).catch( error => { console.error(error) });



    }else if(dia === 2){

       let allClientesLoadingController = this.lCtrl.create({
        content: "Obteniendo Clientes de la Base de Datos"
       });

      allClientesLoadingController.present();

      this.db.getRolMartesCientesSQLite().then((data) => {
        allClientesLoadingController.dismiss();
        this.clientesRolSQLite = data;
        console.log(this.clientesRolSQLite);
        for(var index in this.facturas){

          for(var client in this.clientesRolSQLite){
            if(this.facturas[index].codigo === this.clientesRolSQLite[client].codigo){
              console.log("Esta en la lista");
              this.actualizarHizoFactura(this.clientesRolSQLite[client].codigo);
            }
          }

        }

        for(var index in this.noCompras){
          for(var client in this.clientesRolSQLite){
            if(this.noCompras[index].codigoCliente === this.clientesRolSQLite[client].codigo){
              console.log("Esta en la lista");
              this.actualizarHizoMotivo(this.clientesRolSQLite[client].codigo);
            }
          }
        }
      }).catch( error => { console.error(error) });

    }else if(dia === 3){

      let allClientesLoadingController = this.lCtrl.create({
        content: "Obteniendo Clientes de la Base de Datos"
      });

      allClientesLoadingController.present();

      this.db.getRolMiercolesCientesSQLite().then((data) => {
        allClientesLoadingController.dismiss();
        this.clientesRolSQLite = data;
        console.log(data);

        for(var index in this.facturas){

          for(var client in this.clientesRolSQLite){
            if(this.facturas[index].codigo === this.clientesRolSQLite[client].codigo){
              console.log("Esta en la lista");
              this.actualizarHizoFactura(this.clientesRolSQLite[client].codigo);
            }
          }

        }

        for(var index in this.noCompras){
          for(var client in this.clientesRolSQLite){
            if(this.noCompras[index].codigoCliente === this.clientesRolSQLite[client].codigo){
              console.log("Esta en la lista");
              this.actualizarHizoMotivo(this.clientesRolSQLite[client].codigo);
            }
          }
        }
      }).catch( error => { console.error(error) });



    }else if(dia === 4){

      let allClientesLoadingController = this.lCtrl.create({
        content: "Obteniendo Clientes de la Base de Datos"
      });

      allClientesLoadingController.present();

      this.db.getRolJuevesCientesSQLite().then((data) => {
        allClientesLoadingController.dismiss();
        this.clientesRolSQLite = data;
        //console.log(this.clientesSQLite);
        for(var index in this.facturas){

          for(var client in this.clientesRolSQLite){
            if(this.facturas[index].codigo === this.clientesRolSQLite[client].codigo){
              console.log("Esta en la lista");
              this.actualizarHizoFactura(this.clientesRolSQLite[client].codigo);
            }
          }

        }

        for(var index in this.noCompras){
          for(var client in this.clientesRolSQLite){
            if(this.noCompras[index].codigoCliente === this.clientesRolSQLite[client].codigo){
              console.log("Esta en la lista");
              this.actualizarHizoMotivo(this.clientesRolSQLite[client].codigo);
            }
          }
        }
      }).catch( error => { console.error(error) });

    }else if(dia === 5){

      let allClientesLoadingController = this.lCtrl.create({
        content: "Obteniendo Clientes de la Base de Datos"
      });

      allClientesLoadingController.present();

      this.db.getRolViernerCientesSQLite().then((data) => {
        allClientesLoadingController.dismiss();
        this.clientesRolSQLite = data;
        //console.log(this.clientesSQLite);
        for(var index in this.facturas){

          for(var client in this.clientesRolSQLite){
            if(this.facturas[index].codigo === this.clientesRolSQLite[client].codigo){
              console.log("Esta en la lista");
              this.actualizarHizoFactura(this.clientesRolSQLite[client].codigo);
            }
          }

        }

        for(var index in this.noCompras){
          for(var client in this.clientesRolSQLite){
            if(this.noCompras[index].codigoCliente === this.clientesRolSQLite[client].codigo){
              console.log("Esta en la lista");
              this.actualizarHizoMotivo(this.clientesRolSQLite[client].codigo);
            }
          }
        }
      }).catch( error => { console.error(error) });

    }else if(dia === 6){

      let allClientesLoadingController = this.lCtrl.create({
        content: "Obteniendo Clientes de la Base de Datos"
      });

      allClientesLoadingController.present();

      this.db.getRolSabadoCientesSQLite().then((data) => {
        allClientesLoadingController.dismiss();
        this.clientesRolSQLite = data;
        //console.log(this.clientesSQLite);
        for(var index in this.facturas){

          for(var client in this.clientesRolSQLite){
            if(this.facturas[index].codigo === this.clientesRolSQLite[client].codigo){
              console.log("Esta en la lista");
              this.actualizarHizoFactura(this.clientesRolSQLite[client].codigo);
            }
          }

        }

        for(var index in this.noCompras){
          for(var client in this.clientesRolSQLite){
            if(this.noCompras[index].codigoCliente === this.clientesRolSQLite[client].codigo){
              console.log("Esta en la lista");
              this.actualizarHizoMotivo(this.clientesRolSQLite[client].codigo);
            }
          }
        }
      }).catch( error => { console.error(error) });

    }else if(dia === 0){
      let allClientesLoadingController = this.lCtrl.create({
        content: "Obteniendo Clientes de la Base de Datos"
      });

      allClientesLoadingController.present();

      this.db.getRolDomingoCientesSQLite().then((data) => {
        allClientesLoadingController.dismiss();
        this.clientesRolSQLite = data;
        //console.log(this.clientesSQLite);
        for(var index in this.facturas){

          for(var client in this.clientesRolSQLite){
            if(this.facturas[index].codigo === this.clientesRolSQLite[client].codigo){
              console.log("Esta en la lista");
              this.actualizarHizoFactura(this.clientesRolSQLite[client].codigo);
            }
          }

        }

        for(var index in this.noCompras){
          for(var client in this.clientesRolSQLite){
            if(this.noCompras[index].codigoCliente === this.clientesRolSQLite[client].codigo){
              console.log("Esta en la lista");
              this.actualizarHizoMotivo(this.clientesRolSQLite[client].codigo);
            }
          }
        }
      }).catch( error => { console.error(error) });
    }

  }

  //se ponen para lograr ir de una pagina a otra
  irAFacturacion(cliente: object) {
    this.navCtrl.push(FacturacionPage,  cliente);
  }

  iraUbicacion(cliente: object){
    this.navCtrl.push(UbicacionPage, cliente)
  }
  //metodo para guardar los motivos no compra
  noCompra(cliente:any){
    let alert = this.aCtrl.create();
    alert.setTitle('Motivo no compra');


    alert.addInput({
      type: 'radio',
      label: 'Le compró a competencia',
      value: 'Le compró a competencia',
      checked: false
    });
    alert.addInput({
      type: 'radio',
      label: 'Cliente quincenal',
      value: 'Cliente quincenal',
      checked: false
    });
    alert.addInput({
      type: 'radio',
      label: 'Cliente mensual',
      value: 'Cliente mensual',
      checked: false
    });
    alert.addInput({
      type: 'radio',
      label: 'Cliente por teléfono',
      value: 'Cliente por teléfono',
      checked: false
    });
    alert.addInput({
      type: 'radio',
      label: 'No esta el encargado',
      value: 'No esta el encargado',
      checked: false
    });
    alert.addInput({
      type: 'radio',
      label: 'Lleno Tomza',
      value: 'Lleno Tomza',
      checked: false
    });

    alert.addInput({
      type: 'radio',
      label: 'Camión sin inventario',
      value: 'Camión sin inventario',
      checked: false
    });

    alert.addInput({
      type: 'radio',
      label: 'Problemas mecánicos',
      value: 'Problemas mecánicos',
      checked: false
    });

    alert.addInput({
      type: 'radio',
      label: 'No tiene dinero',
      value: 'No tiene dinero',
      checked: false
    });

    alert.addInput({
      type: 'radio',
      label: 'Cliente moroso',
      value: 'Cliente moroso',
      checked: false
    });

    alert.addInput({
      type: 'radio',
      label: 'Cliente cerrado',
      value: 'Cliente cerrado',
      checked: false
    });

    alert.addInput({
      type: 'radio',
      label: 'Camino cerrado',
      value: 'Camino cerrado',
      checked: false
    });

    alert.addInput({
      type: 'radio',
      label: 'Mucho tiempo en atender',
      value: 'Mucho tiempo en atender',
      checked: false
    });

    alert.addInput({
      type: 'radio',
      label: 'Ya fue atendido en la semana',
      value: 'Ya fue atendido en la semana',
      checked: false
    });
    alert.addInput({
      type: 'radio',
      label: 'Cliente Inactivo',
      value: 'Cliente Inactivo',
      checked: false
    });
    alert.addInput({
      type: 'radio',
      label: 'Cliente solicita retiro de tanque',
      value: 'Cliente solicita retiro de tanque',
      checked: false
    });

    alert.addButton('Cancelar');
    alert.addButton({
      text: 'Aceptar',
      handler: data => {
        var fecha =  new Date();
        var day;

        var dia = fecha.getDate();
        var mes =  fecha.getMonth();
        var year =  fecha.getFullYear();

        var horas = fecha.getHours();
        var minut = fecha.getMinutes();
        var segun = fecha.getSeconds();
        var minutos;
        var segundos;
        var meses = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

        if(minut <= 9){
              minutos = '0'+minut.toString();
        }else{
              minutos = minut.toString();
        }

        if(segun <= 9){
            segundos = '0'+segun.toString();
        }else{
            segundos = segun.toString();
        }

        if(dia <= 9){
          day = '0'+dia.toString();
        }else{
          day = dia.toString();
        }

        var fechaCompleta = year +'-'+meses[mes]+'-'+day+' '+horas+':'+minutos+':'+segundos;

       var motivo = {
         codigoCliente: cliente.codigo,
         fecha: fechaCompleta,
         ruta: cliente.ruta,
         motivo: data,
         fechaRuta: fechaCompleta+'/'+cliente.ruta,
         enviado: 0
       }

       //metodo que guarda el motivo que armamos arriba
       this.guardarMotivoNoCompra(motivo);

       this.obtenerMotivosDeSQLite();
      }
    });
    alert.present();
  }

  //metodo que guarda los motivos en sqlite
  guardarMotivoNoCompra(motivo:any){

    this.db.guardarNoCompra(motivo).then(data =>{
      console.log(data);
      console.log("Motivo guardada con exito")
    }).catch( error => { console.error(error) })

  }

  //metodo que obtiene los motivos de sqlite
  obtenerMotivosDeSQLite(){
    this.db.getMotivosSQLite().then((data) => {
      this.noCompras = data;
      console.log(data);
    },(error) => {
      console.log(error);
    })
  }

  obtenerFacturasDeSQLite(){
    this.db.getFacturasSQLite().then((data) => {
      this.facturas = data;
      console.log(data);
    }, (error) => {
      console.log(error);
    })
  }

  actualizarHizoMotivo(codigoCliente:any){
    this.db.actualizarHizoMotivo({
      codigo: codigoCliente,
      hizoMotivo: 2
    }).then((data) => {
      console.log(data);
      //this.obtenerClientesDeSQLite();
    }).catch(error => {console.error(error)});

    //this.obtenerRol();
  }

  actualizarHizoFactura(codigoCliente:any){
    this.db.actualizarHizoFactura({
      codigo: codigoCliente,
      hizoFactura: 1
    }).then((data) => {
      console.log(data);
      //this.obtenerClientesDeSQLite();
    }).catch(error => {console.error(error)});

    //this.obtenerRolDelDia();
  }

  obtenerRol(){

    var dia;
    var fecha = new Date();
    var dias = ['Do','L','K','M','J','V','S'];


    dia =  fecha.getDay();

    console.log('Este es el dia',dia);

    if(dia === 1){




      this.db.getRolLunesCientesSQLite().then((data) => {

        this.clientesRolSQLite = data;
        console.log(data);
        for(var index in this.facturas){

          for(var client in this.clientesRolSQLite){
            if(this.facturas[index].codigo === this.clientesRolSQLite[client].codigo){
              console.log("Esta en la lista");
              this.actualizarHizoFactura(this.clientesRolSQLite[client].codigo);
            }
          }

        }

        for(var index in this.noCompras){
          for(var client in this.clientesRolSQLite){
            if(this.noCompras[index].codigoCliente === this.clientesRolSQLite[client].codigo){
              console.log("Esta en la lista");
              this.actualizarHizoMotivo(this.clientesRolSQLite[client].codigo);
            }
          }
        }
      }).catch( error => { console.error(error) });



    }else if(dia === 2){



      this.db.getRolMartesCientesSQLite().then((data) => {

        this.clientesRolSQLite = data;
        console.log(this.clientesRolSQLite);
        for(var index in this.facturas){

          for(var client in this.clientesRolSQLite){
            if(this.facturas[index].codigo === this.clientesRolSQLite[client].codigo){
              console.log("Esta en la lista");
              this.actualizarHizoFactura(this.clientesRolSQLite[client].codigo);
            }
          }

        }

        for(var index in this.noCompras){
          for(var client in this.clientesRolSQLite){
            if(this.noCompras[index].codigoCliente === this.clientesRolSQLite[client].codigo){
              console.log("Esta en la lista");
              this.actualizarHizoMotivo(this.clientesRolSQLite[client].codigo);
            }
          }
        }
      }).catch( error => { console.error(error) });

    }else if(dia === 3){


      this.db.getRolMiercolesCientesSQLite().then((data) => {

        this.clientesRolSQLite = data;
        console.log(data);

        for(var index in this.facturas){

          for(var client in this.clientesRolSQLite){
            if(this.facturas[index].codigo === this.clientesRolSQLite[client].codigo){
              console.log("Esta en la lista");
              this.actualizarHizoFactura(this.clientesRolSQLite[client].codigo);
            }
          }

        }

        for(var index in this.noCompras){
          for(var client in this.clientesRolSQLite){
            if(this.noCompras[index].codigoCliente === this.clientesRolSQLite[client].codigo){
              console.log("Esta en la lista");
              this.actualizarHizoMotivo(this.clientesRolSQLite[client].codigo);
            }
          }
        }
      }).catch( error => { console.error(error) });



    }else if(dia === 4){



      this.db.getRolJuevesCientesSQLite().then((data) => {

        this.clientesRolSQLite = data;
        //console.log(this.clientesSQLite);
        for(var index in this.facturas){

          for(var client in this.clientesRolSQLite){
            if(this.facturas[index].codigo === this.clientesRolSQLite[client].codigo){
              console.log("Esta en la lista");
              this.actualizarHizoFactura(this.clientesRolSQLite[client].codigo);
            }
          }

        }

        for(var index in this.noCompras){
          for(var client in this.clientesRolSQLite){
            if(this.noCompras[index].codigoCliente === this.clientesRolSQLite[client].codigo){
              console.log("Esta en la lista");
              this.actualizarHizoMotivo(this.clientesRolSQLite[client].codigo);
            }
          }
        }
      }).catch( error => { console.error(error) });

    }else if(dia === 5){


      this.db.getRolViernerCientesSQLite().then((data) => {

        this.clientesRolSQLite = data;
        //console.log(this.clientesSQLite);
        for(var index in this.facturas){

          for(var client in this.clientesRolSQLite){
            if(this.facturas[index].codigo === this.clientesRolSQLite[client].codigo){
              console.log("Esta en la lista");
              this.actualizarHizoFactura(this.clientesRolSQLite[client].codigo);
            }
          }

        }

        for(var index in this.noCompras){
          for(var client in this.clientesRolSQLite){
            if(this.noCompras[index].codigoCliente === this.clientesRolSQLite[client].codigo){
              console.log("Esta en la lista");
              this.actualizarHizoMotivo(this.clientesRolSQLite[client].codigo);
            }
          }
        }
      }).catch( error => { console.error(error) });

    }else if(dia === 6){


      this.db.getRolSabadoCientesSQLite().then((data) => {

        this.clientesRolSQLite = data;
        //console.log(this.clientesSQLite);
        for(var index in this.facturas){

          for(var client in this.clientesRolSQLite){
            if(this.facturas[index].codigo === this.clientesRolSQLite[client].codigo){
              console.log("Esta en la lista");
              this.actualizarHizoFactura(this.clientesRolSQLite[client].codigo);
            }
          }

        }

        for(var index in this.noCompras){
          for(var client in this.clientesRolSQLite){
            if(this.noCompras[index].codigoCliente === this.clientesRolSQLite[client].codigo){
              console.log("Esta en la lista");
              this.actualizarHizoMotivo(this.clientesRolSQLite[client].codigo);
            }
          }
        }
      }).catch( error => { console.error(error) });

    }else if(dia === 0){


      this.db.getRolDomingoCientesSQLite().then((data) => {

        this.clientesRolSQLite = data;
        //console.log(this.clientesSQLite);
        for(var index in this.facturas){

          for(var client in this.clientesRolSQLite){
            if(this.facturas[index].codigo === this.clientesRolSQLite[client].codigo){
              console.log("Esta en la lista");
              this.actualizarHizoFactura(this.clientesRolSQLite[client].codigo);
            }
          }

        }

        for(var index in this.noCompras){
          for(var client in this.clientesRolSQLite){
            if(this.noCompras[index].codigoCliente === this.clientesRolSQLite[client].codigo){
              console.log("Esta en la lista");
              this.actualizarHizoMotivo(this.clientesRolSQLite[client].codigo);
            }
          }
        }
      }).catch( error => { console.error(error) });
    }
  }
}
