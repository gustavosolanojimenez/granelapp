import { SqliteDbProvider } from './../../providers/sqlite-db/sqlite-db';
import { Component } from '@angular/core';
import {  NavController, NavParams, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { MenuGranelPage } from '../menu-granel/menu-granel';


/**
 * Generated class for the DepositoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-deposito',
  templateUrl: 'deposito.html',
})
export class DepositoPage {

transaccion;
tipo;
documento:AbstractControl;
banco:  AbstractControl;
fechaCompleta;
factura;
fecha ;
ruta:AbstractControl;
id;
facnumero;
monto: AbstractControl;
//fecha
objDate: Date = new Date();
formatoFecha = '';

formgroup: FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams,public formbuilder: FormBuilder,
    public db: SqliteDbProvider,
    public aCtrl: AlertController) {
      this.ruta = this.navParams.get('id');

    this.formgroup =  formbuilder.group({
      documento:['', Validators.required],
      monto:['', Validators.required],
      banco: ['', Validators.required],

  });

  this.banco =  this.formgroup.controls['banco'];
  this.monto = this.formgroup.controls['monto'];
  this.documento = this.formgroup.controls['documento'];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DepositoPage');
  }

  guardarDeposito(){
    var fecha =  new Date();
    var day;

    var dia = fecha.getDate();
    var mes =  fecha.getMonth();
    var year =  fecha.getFullYear();

    var horas = fecha.getHours();
    var minut = fecha.getMinutes();
    var segun = fecha.getSeconds();
    var minutos;
    var segundos;
    var meses = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

    if(minut <= 9){
          minutos = '0'+minut.toString();
    }else{
          minutos = minut.toString();
    }

    if(segun <= 9){
        segundos = '0'+segun.toString();
    }else{
        segundos = segun.toString();
    }

    if(dia <= 9){
      day = '0'+dia.toString();
    }else{
      day = dia.toString();
    }

    this.fechaCompleta = day+'-'+meses[mes]+'-'+year+' '+horas+':'+minutos+':'+segundos;

    let transaccion = {
      factura: '0',
      ruta: this.ruta,
      tipo: 'Deposito',
      fecha: this.fechaCompleta,
      monto: this.formgroup.value.monto,
      banco: this.formgroup.value.banco,
      documento : this.formgroup.value.documento,
    }

    if (this.factura !== '') {
      this.db.guardarTransaccionesSQLite(transaccion).then(data => {
        console.log(data);
      }).catch( error => { console.error(error) })
    }
  }
  IrAMenu(){
    this.navCtrl.push(MenuGranelPage,{id:this.ruta});
  }
  presentAlert() {
    let alert = this.aCtrl.create({
      title: 'Deposito guardado correctamente',
      subTitle: this.tipo,
      buttons: ['Aceptar']
    });
    alert.present();
    // this.IrAMenu();
  }

  mensajeDeSeguridad() {
    let alert = this.aCtrl.create({
      title: '¿Está completamente seguro de que completó toda la información?',
      subTitle: 'Verifique si no esta seguro!!',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: () => {
            console.log('Confirm Okay');
            this.guardarDeposito();
            this.presentAlert();
          }
        }
      ]
    });
    alert.present();
  }

}
