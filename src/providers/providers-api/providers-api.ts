import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable()
export class ProvidersApiProvider {

  constructor(public http: HttpClient) {
    console.log('Hello ProvidersApiProvider Provider');
  }


  get datosapi(){
    return this.http.get('https://api-tomza.herokuapp.com/api/tanques')


  }

}
