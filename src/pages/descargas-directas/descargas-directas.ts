import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Geoposition, Geolocation } from '@ionic-native/geolocation';
import { SqliteDbProvider } from '../../providers/sqlite-db/sqlite-db';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { ImprimirPage } from './../imprimir/imprimir';

@Component({
  selector: 'page-facturacion',
  templateUrl: 'descargas-directas.html',
})
export class DescargasDirectasPage {
  data;
  nombreCliente; codigoCliente; razonCliente; tipoPagoCliente; descuentoCliente; tipoCliente;

  latitud: number; longitud: number;

  //variable para cargar los productos de sqlite
  productosSQLite: any = [];
  productoSQLite: any;
  rutaEnUso: any;

  //variables para el detalle
  productosSeleccionados: any = [];
  detallesTanques: any = [];
  totalFinal = 0;
  totalFinalConDescuento = 0;
  totalDescuentosUnitarios = 0;
  totalDescuentosTotales = 0;
  decimales = "0";
  totalFinalDecimales = "0";
  TOM: any;
  numOC = '';
  contadorde25 = 0;
  contadorVariable = 0;
  formatoFecha = '';
  //fecha
  objDate: Date = new Date();

  //Datos para el cliente
  codigo; facnumero: number; formadepago; precio; razon; nombre; lprecioc; lprecioa; descuento; descuentot; credito; total; ruta; fecha = new Date();
  q25 = 0; q25r = 0; q20 = 0; q20r = 0; q10 = 0; q10r = 0; q35 = 0; q35r = 0; q45 = 0; q45r = 0; q60 = 0; q60r = 0; q100 = 0; q100r = 0; qlts = 0; qkgs = 0; LAT; LONGI; g;
  efectivo; transmonto = 0; transnumero = 0; chknumero = 0; chkbanco = 0; chkmonto = 0; tipo; cz25 = 0; cz25r = 0; cz100r = 0; fechadt = new Date(); act; comentario; identificacion;
  GLN; EAN;

  cuenta25 = 0; cuenta25r = 0; cuenta20 = 0; cuenta20r = 0; cuenta10 = 0; cuenta10r = 0; cuenta35 = 0; cuenta35r = 0; cuenta45 = 0; cuenta45r = 0; cuenta60 = 0; cuenta60r = 0;
  cuenta100 = 0; cuentaLitros = 0; cuentaKilos = 0; cuenta30 = 0; cuenta40 = 0;

  //Datos para la ruta
  codRuta; nombreRuta; unidadNegocioRuta; cediRuta; descripcionRuta; consecutivoRuta; cerosTOM; oc;

  //cambio de precio
  precioviejo: boolean = false;
  miLat: number;
  miLong: number;
  consolidado: boolean = false;

  empresaTomza = {
    'nombreEmpresa': 'Gas Tomza de Costa Rica',
    'codigoPais': '506',
    'cedulaJuridica': '003101349880',
    'codigoSeguridad': '112445776'
  };

  idEmpresa: any; nombreEmpresa: any; codigoPais: any; cedulaJuridica: any; codigoSeguridad: any;
  //Clave, consecutivo, TOM
  claveFactura: String = ''; consecutivoFactura: string = '';
  //Para completar ceros
  unCero: String = '0'; dosCero: String = '00'; tresCero: String = '000'; cuatroCero: String = '0000'; cincoCero: String = '00000';
  seisCero: String = '000000'; sieteCero: String = '0000000'; ochoCero: String = '00000000'; nueveCero: String = '000000000';
  nombreEm: String;
  //datos fecha para clave
  fechaClave = new Date();
  diaClave: string = ''; mesClave: string = ''; anioClave: string = '';
  casaMatriz: String = "001";
  facturaElectronica = "01";
  tiqueteElectronico = "04";

  preciokg;
  qrData = null;
  createdCode = null;
  scannedCode = null;
  tanque;
  serieTanque;
  serie;

  constructor(public barcodeScanner: BarcodeScanner, public navCtrl: NavController, public lCtrl: LoadingController, public navParams: NavParams, public geo: Geolocation, public db: SqliteDbProvider, public aCtrl: AlertController) {

    this.data = this.navParams.get('codigo');
    this.codigoCliente = this.navParams.get('cod');
    this.nombreCliente = this.navParams.get('name');
    this.razonCliente = this.navParams.get('razon');
    this.formadepago = this.navParams.get('formadepago');
    this.credito = this.navParams.get('credito');
    this.descuento = this.navParams.get('descuento');
    this.tipoCliente = this.navParams.get('tipo');
    this.preciokg = this.navParams.get('precioKilos');
    this.serie = this.navParams.get('serie');
    this.ruta = this.navParams.get('id');
    this.GLN = this.navParams.get('GLN');
    this.EAN = this.navParams.get('EAN');
    console.log(this.tipoCliente);
    console.log("prueba del credito", this.credito);
    console.log("prueba de forma de pago", this.formadepago);
    this.g = this.navParams.get('g');
    this.act = this.navParams.get('Activo');
    this.comentario = this.navParams.get('Comentario');
    this.identificacion = this.navParams.get('documento');
    console.log('activo', this.act);
    console.log('coment', this.comentario);
    console.log('identificacion', this.identificacion);
    console.log('tipo', this.tipo);







    console.log("Datos del cliente en facturacion", this.data);
  }

  // getGeolocation(){
  //   this.geo.getCurrentPosition().then((geoposition: Geoposition) =>{

  //     this.latitud = geoposition.coords.latitude;
  //     this.longitud= geoposition.coords.longitude;

  //     console.log("Esta es mi latitud:",this.latitud);
  //     console.log("Esta es mi longitud:",this.longitud);
  //   });
  // }



  cambio() {
    if (this.precioviejo === true) {
      let mensaje = this.aCtrl.create({
        title: '¿Seguro que desea facturar con el precio anterior?',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'primary',
            handler: data => {
              console.log('Cancel clicked');
              this.precioviejo = false;
            }
          },
          {
            text: 'Aceptar',
            handler: () => {

            }
          }]
      });
      mensaje.present();
    }
  }

  cambioConsolidado() {
    if (this.consolidado === true) {
      let mensaje = this.aCtrl.create({
        title: '¿Seguro que desea facturar con consolidado de tanques?',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'primary',
            handler: data => {
              console.log('Cancel clicked');
              this.consolidado = false;
            }
          },
          {
            text: 'Aceptar',
            handler: () => {

            }
          }]
      });
      mensaje.present();
    }
  }

  getGeolocation() {
    this.geo.getCurrentPosition().then((geoposition: Geoposition) => {

      this.miLat = geoposition.coords.latitude;
      this.miLong = geoposition.coords.longitude;

      console.log("Esta es mi latitud:", this.miLat);
      console.log("Esta es mi longitud:", this.miLong);
    });
  }

  ionViewDidLoad() {
    this.obtenerProductosDeSQLite(this.tipoCliente);
    this.obtenerRutaEnUso();
    this.getGeolocation();
  }

  //metodo que obtiene la ruta en uso para utilizar sus datos en la actualizacion del consecutivo
  //y armar el TOM para cada factura
  obtenerRutaEnUso() {
    this.db.getRutaUsada(parseInt(this.ruta)).then((data) => {
      this.rutaEnUso = data;
      console.log(this.rutaEnUso);
      this.codRuta = this.rutaEnUso.id;
      this.nombreRuta = this.rutaEnUso.nombre;
      this.unidadNegocioRuta = this.rutaEnUso.unidad_negocio;
      this.cediRuta = this.rutaEnUso.cedi;
      this.descripcionRuta = this.rutaEnUso.descripcion;
      this.consecutivoRuta = this.rutaEnUso.consecutivo;
      this.cerosTOM = this.rutaEnUso.cerosTOM;

      //armo el TOM para despues usarlo en la factura
      this.TOM = this.codRuta + this.cerosTOM + this.consecutivoRuta;
      console.log("Este es el TOM:", this.TOM);

      //este if es para agregar los 0 necesarios para el numero de terminal o punto de venta
      //que seria el numero de ruta, se completan los 0 para llegar a 5 digitos de ser necesario
      if (this.razon === 'Cliente VIP' || this.razon === 'Tiquete Electronico') {
        this.consecutivoFactura += this.casaMatriz;
        if (this.codRuta < 10) {
          this.consecutivoFactura += this.cuatroCero + this.codRuta + this.tiqueteElectronico;
          console.log(this.consecutivoFactura);
        } else if (this.codRuta >= 10 && this.codRuta <= 99) {
          this.consecutivoFactura += this.tresCero + this.codRuta + this.tiqueteElectronico;
          console.log(this.consecutivoFactura);
        } else if (this.codRuta >= 100 && this.codRuta <= 999) {
          this.consecutivoFactura += this.dosCero + this.codRuta + this.tiqueteElectronico;
          console.log(this.consecutivoFactura);
        } else if (this.codRuta >= 1000 && this.codRuta <= 9999) {
          this.consecutivoFactura += this.unCero + this.codRuta + this.tiqueteElectronico;
          console.log(this.consecutivoFactura);
        }
      } else {
        this.consecutivoFactura += this.casaMatriz;
        if (this.codRuta < 10) {
          this.consecutivoFactura += this.cuatroCero + this.codRuta + this.facturaElectronica;
          console.log(this.consecutivoFactura);
        } else if (this.codRuta >= 10 && this.codRuta <= 99) {
          this.consecutivoFactura += this.tresCero + this.codRuta + this.facturaElectronica;
          console.log(this.consecutivoFactura);
        } else if (this.codRuta >= 100 && this.codRuta <= 999) {
          this.consecutivoFactura += this.dosCero + this.codRuta + this.facturaElectronica;
          console.log(this.consecutivoFactura);
        } else if (this.codRuta >= 1000 && this.codRuta <= 9999) {
          this.consecutivoFactura += this.unCero + this.codRuta + this.facturaElectronica;
          console.log(this.consecutivoFactura);
        }
      }

      //este if-else es para agregar los 0 necesarios para el numero de factura
      //del consecutivo completar los 10 digitos de ser necesario.
      if (this.consecutivoRuta < 10) {
        this.consecutivoFactura += this.nueveCero + this.consecutivoRuta.toString();
        console.log(this.consecutivoFactura);
      } else if (this.consecutivoRuta >= 10 && this.consecutivoRuta <= 99) {
        this.consecutivoFactura += this.ochoCero + this.consecutivoRuta.toString();
        console.log(this.consecutivoFactura);
      } else if (this.consecutivoRuta >= 100 && this.consecutivoRuta <= 999) {
        this.consecutivoFactura += this.sieteCero + this.consecutivoRuta.toString();
        console.log(this.consecutivoFactura);
      } else if (this.consecutivoRuta >= 1000 && this.consecutivoRuta <= 9999) {
        this.consecutivoFactura += this.seisCero + this.consecutivoRuta.toString();
        console.log(this.consecutivoFactura);
      } else if (this.consecutivoRuta >= 10000 && this.consecutivoRuta <= 99999) {
        this.consecutivoFactura += this.cincoCero + this.consecutivoRuta.toString();
        console.log(this.consecutivoFactura);
      } else if (this.consecutivoRuta >= 100000 && this.consecutivoRuta <= 999999) {
        this.consecutivoFactura += this.cuatroCero + this.consecutivoRuta.toString();
        console.log(this.consecutivoFactura);
      } else if (this.consecutivoRuta >= 1000000 && this.consecutivoRuta <= 9999999) {
        this.consecutivoFactura += this.tresCero + this.consecutivoRuta.toString();
        console.log(this.consecutivoFactura);
      } else if (this.consecutivoRuta >= 10000000 && this.consecutivoRuta <= 99999999) {
        this.consecutivoFactura += this.dosCero + this.consecutivoRuta.toString();
        console.log(this.consecutivoFactura);
      } else if (this.consecutivoRuta >= 100000000 && this.consecutivoRuta <= 999999999) {
        this.consecutivoFactura += this.unCero + this.consecutivoRuta.toString();
        console.log(this.consecutivoFactura);
      } else if (this.consecutivoRuta >= 1000000000 && this.consecutivoRuta < 9999999999) {
        this.consecutivoFactura += this.consecutivoRuta.toString();
        console.log('Este es el consecutivo', this.consecutivoFactura);
      }

      //generar clave de la factura
      var anio = this.fechaClave.getFullYear();
      var mes = this.fechaClave.getMonth();
      var dia = this.fechaClave.getDate();
      var meses = ['01', '02', '03', '04', '05', '06', '07', '08', '09'];

      if (dia <= 9) {
        this.diaClave += this.unCero + dia.toString();
        console.log(this.diaClave);
      } else {
        this.diaClave += dia.toString();
        console.log(this.diaClave);
      }

      if (mes <= 9) {
        this.mesClave += this.unCero + (mes + 1).toString();
        console.log(this.mesClave);
      } else {
        this.mesClave += (mes + 1).toString();
        console.log(this.mesClave);
      }

      this.anioClave = anio.toString();

      //armo la clave numerica
      this.claveFactura = this.empresaTomza.codigoPais + this.diaClave + this.mesClave + this.anioClave.slice(2, 4) + this.empresaTomza.cedulaJuridica + this.consecutivoFactura + this.empresaTomza.codigoSeguridad;

      console.log('Esta es la clave', this.claveFactura);

    }, (error) => {
      console.log(error);
    })
  }

  //metodo que obtiene los productos almacenados en SQLite utilizando el metodo de DatabaseProvider
  obtenerProductosDeSQLite(tipo: number) {

    let cargaProductos = this.lCtrl.create({
      content: "Cargando información...Espere"
    });

    cargaProductos.present();

    this.db.getProductosSQLite(tipo).then((data) => {
      this.productosSQLite = data;
      cargaProductos.dismiss();
      console.log("productos", this.productosSQLite);
    }, (error) => {
      console.log(error);
    })
  }

  //metodo que permite hacer la consolidacion de tanques o no
  consolidadoONo(id: any) {
    if (this.consolidado === false) {
      this.obtenerProductoSQLite(id, 2);
    }

    if (this.consolidado === true) {
      console.log("consolidacion de tanques");
      this.barcodeScanner.scan().then(barcodeData => {
        this.scannedCode = barcodeData.text;

        this.tanque = JSON.parse(this.scannedCode);
        this.serieTanque = this.tanque.serie;

        this.obtenerProductoSQLiteParaConsolidado(id, this.serieTanque);


      })
    }
  }


  //metodo que obtiene un producto especifico de la base de datos
  obtenerProductoSQLite(id: any, serieT: any) {
    this.db.getProductoSQLite(id).then((data) => {
      this.productoSQLite = data;
      //console.log(this.productoSQLite);
      //declaro el total de precio del producto
      var totalProducto = 0;
      console.log("Togle:", this.precioviejo);
      //se presenta modal
      let alert = this.aCtrl.create({
        title: 'Producto: ' + this.productoSQLite.producto,
        inputs: [
          {
            name: 'txtCantidad',
            type: 'number',
            placeholder: 'Cantidad'
          },
          {
            name: 'txtAplicaDescuento',
            type: 'string',
            placeholder: '¿Aplicar descuento?'
          }
        ],
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'primary',
            handler: data => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Aceptar',
            handler: (data) => {

              var cantidad = data.txtCantidad;
              var descuento;
              var descuentoTotal;
              console.log(cantidad);
              //variables para controlar los montos
              //var totalDescuento = this.cliente.descuento*cantidad;

              var totalNeto = 0;


              if ((cantidad == '') || (parseInt(cantidad) < 0)) {
                let mensaje = this.aCtrl.create({
                  title: 'Debe ingresar una cantidad para facturar y no se pueden ingresar negativos',
                  buttons: [{
                    text: 'Aceptar',
                    handler: () => {

                    }
                  }]
                });
                mensaje.present();

              } else if ((cantidad != '') && (parseInt(cantidad) > 0)) {

                // if(this.precioviejo === false){

                // }
                // if(this.precioviejo === true){

                // }


                //tipo para granel y gran granel
                if ((this.tipoCliente === "6") || (this.tipoCliente === "7")) {
                  //estos if son para los productos de granel que son litros y kilos de gas
                  if (this.productoSQLite.producto === 'L' && data.txtAplicaDescuento.toLowerCase() === 'no') {
                    this.qlts = cantidad;
                    console.log(this.qlts);
                    this.cuentaLitros = this.cuentaLitros + parseInt(cantidad);
                    descuento = 0;
                    descuentoTotal = 0;
                    totalProducto = this.productoSQLite.precio * cantidad;
                    totalNeto = (totalProducto);
                    console.log("Total descuento: ₡", 0)
                    console.log("Monto total: ₡", totalNeto);

                  } else if ((this.productoSQLite.producto === 'L' && data.txtAplicaDescuento === '') || (this.productoSQLite.producto === 'L' && data.txtAplicaDescuento.length > 0)) {
                    this.qlts = cantidad;
                    console.log(this.qlts);
                    this.cuentaLitros = this.cuentaLitros + parseInt(cantidad);
                    descuento = parseFloat(this.descuento);
                    console.log("Este es el descuento:", descuento);
                    console.log("Descuento BD:", this.descuento);
                    descuentoTotal = descuento * cantidad;
                    totalProducto = this.productoSQLite.precio * cantidad;
                    totalNeto = (totalProducto) - descuentoTotal;
                    console.log("Descuento unitario: ₡", descuento);
                    console.log("Total productos: ₡", totalProducto)
                    console.log("Total descuento: ₡", descuentoTotal);
                    console.log("Monto total: ₡", totalNeto);
                  }

                  if (this.productoSQLite.producto === 'K' && data.txtAplicaDescuento.toLowerCase() === 'no') {
                    this.qkgs = cantidad;
                    console.log(this.qkgs);
                    this.cuentaKilos = this.cuentaKilos + parseInt(cantidad);
                    descuento = 0;
                    descuentoTotal = 0;
                    totalProducto = this.lprecioc * cantidad;
                    totalNeto = (totalProducto);
                    console.log("Total descuento: ₡", 0)
                    console.log("Monto total: ₡", totalNeto);

                  } else if ((this.productoSQLite.producto === 'K' && data.txtAplicaDescuento === '') || (this.productoSQLite.producto === 'K' && data.txtAplicaDescuento.length > 0)) {
                    this.qkgs = cantidad;
                    console.log(this.qkgs);
                    this.cuentaKilos = this.cuentaKilos + parseInt(cantidad);
                    descuento = parseFloat(this.descuento);
                    descuentoTotal = descuento * cantidad;
                    this.productoSQLite.precio = this.lprecioc; //le asigno el precio especial que tiene el cliente
                    totalProducto = this.lprecioc * cantidad;
                    totalNeto = (totalProducto) - descuentoTotal;
                    console.log("Descuento unitario: ₡", descuento);
                    console.log("Total productos: ₡", totalProducto)
                    console.log("Total descuento: ₡", descuentoTotal);
                    console.log("Monto total: ₡", totalNeto);
                  }
                }

                if (this.tipo === '11') {
                  if (this.productoSQLite.producto === 'Car' && data.txtAplicaDescuento.toLowerCase() === 'no') {
                    this.qlts = cantidad;
                    console.log(this.qlts);
                    this.cuentaLitros = this.cuentaLitros + parseInt(cantidad);
                    descuento = 0;
                    descuentoTotal = 0;
                    totalProducto = this.productoSQLite.precio * cantidad;
                    totalNeto = (totalProducto);
                    console.log("Total descuento: ₡", 0)
                    console.log("Monto total: ₡", totalNeto);

                  } else if ((this.productoSQLite.producto === 'Car' && data.txtAplicaDescuento === '') || (this.productoSQLite.producto === 'Car' && data.txtAplicaDescuento.length > 0)) {
                    this.qlts = cantidad;
                    console.log(this.qlts);
                    this.cuentaLitros = this.cuentaLitros + parseInt(cantidad);
                    descuento = parseInt(this.descuento);
                    descuentoTotal = descuento * cantidad;
                    totalProducto = this.productoSQLite.precio * cantidad;
                    totalNeto = (totalProducto) - descuentoTotal;
                    console.log("Descuento unitario: ₡", descuento);
                    console.log("Total productos: ₡", totalProducto)
                    console.log("Total descuento: ₡", descuentoTotal);
                    console.log("Monto total: ₡", totalNeto);
                  }
                }

                if (this.precioviejo === false) {
                  //aqui se arma el detalle de cada linea de producto, adentro se ve cada uno de sus atributos
                  var total = parseInt(data.txtCantidad) * this.productoSQLite.precio;
                  var productoArmado = {
                    'cantidad': parseInt(data.txtCantidad),
                    'detalle': this.productoSQLite.descripcion,
                    'unidadMedida': 'unid',
                    'precioUnitario': this.productoSQLite.precio,
                    'precioConDescuento': this.productoSQLite.precio - descuento,
                    'descuentoUnitario': descuento,
                    'descuentoTotal': descuentoTotal,
                    'montoTotalSinDescuento': total.toFixed(2),
                    'montoTotalConDescuento': totalNeto,
                    'montoTotal': total.toFixed(2),
                    'subtotal': total.toFixed(2),
                    'montoTotalLinea': total.toFixed(2)
                  };

                  var detalleTanque = {


                    'serietanque': this.serie,
                    'cantidadLtKg': parseInt(data.txtCantidad)
                  };


                  console.log(detalleTanque);
                } else {
                  //aqui se arma el detalle de cada linea de producto, adentro se ve cada uno de sus atributos
                  var totalPrecioViejo = parseInt(data.txtCantidad) * this.productoSQLite.precioViejo;
                  var productoArmado = {
                    'cantidad': parseInt(data.txtCantidad),
                    'detalle': this.productoSQLite.descripcion,
                    'unidadMedida': 'unid',
                    'precioUnitario': this.productoSQLite.precioViejo,
                    'precioConDescuento': this.productoSQLite.precioViejo - descuento,
                    'descuentoUnitario': descuento,
                    'descuentoTotal': descuentoTotal,
                    'montoTotalSinDescuento': totalPrecioViejo.toFixed(2),
                    'montoTotalConDescuento': totalNeto,
                    'montoTotal': totalPrecioViejo.toFixed(2),
                    'subtotal': totalPrecioViejo.toFixed(2),
                    'montoTotalLinea': totalPrecioViejo.toFixed(2)
                  };

                  var detalleTanque = {


                    'serietanque': this.serie,
                    'cantidadLtKg': parseInt(data.txtCantidad)
                  }


                  console.log(detalleTanque);
                }

                //despues cada linea se inserta en un arreglo para posteriormene convertir dicho arreglo en string
                //y guardarlo en base de datos
                this.productosSeleccionados.push(productoArmado);
                this.detallesTanques.push(detalleTanque);
                console.log(this.detallesTanques);
                console.log(this.productosSeleccionados);

                //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
                this.totalFinal = (this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0));
                this.totalFinalConDescuento = (this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0));
                this.totalDescuentosUnitarios = (this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0));
                //corrijo los decimales para que solo marque 2
                this.totalFinalDecimales = this.totalFinal.toFixed(2);
                this.decimales = this.totalFinalConDescuento.toFixed(2);
                console.log("El monto total es:" + this.totalFinal);
                console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
                console.log("El descuento total:" + this.totalDescuentosTotales);
              }
            }
          }
        ]
      });
      alert.present();

    }, (error) => {
      console.log(error);
    })
  }

  //metodo que obtiene un producto especifico de la base de datos
  obtenerProductoSQLiteParaConsolidado(id: any, serieT: any) {
    this.db.getProductoSQLite(id).then((data) => {
      this.productoSQLite = data;
      //console.log(this.productoSQLite);
      //declaro el total de precio del producto
      var totalProducto = 0;
      console.log("Togle:", this.precioviejo);
      //se presenta modal
      let alert = this.aCtrl.create({
        title: 'Producto: ' + this.productoSQLite.producto,
        inputs: [
          {
            name: 'txtCantidad',
            type: 'number',
            placeholder: 'Cantidad'
          },
          {
            name: 'txtAplicaDescuento',
            type: 'string',
            placeholder: '¿Aplicar descuento?'
          }
        ],
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'primary',
            handler: data => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Aceptar',
            handler: (data) => {

              var cantidad = data.txtCantidad;
              var descuento;
              var descuentoTotal;
              console.log(cantidad);
              //variables para controlar los montos
              //var totalDescuento = this.cliente.descuento*cantidad;

              var totalNeto = 0;


              if ((cantidad == '') || (parseInt(cantidad) < 0)) {
                let mensaje = this.aCtrl.create({
                  title: 'Debe ingresar una cantidad para facturar y no se pueden ingresar negativos',
                  buttons: [{
                    text: 'Aceptar',
                    handler: () => {

                    }
                  }]
                });
                mensaje.present();

              } else if ((cantidad != '') && (parseInt(cantidad) > 0)) {

                // if(this.precioviejo === false){

                // }
                // if(this.precioviejo === true){

                // }


                //tipo para granel y gran granel
                if ((this.tipoCliente === '6') || (this.tipoCliente === '7')) {
                  //estos if son para los productos de granel que son litros y kilos de gas
                  if (this.productoSQLite.producto === 'L' && data.txtAplicaDescuento.toLowerCase() === 'no') {
                    this.qlts = cantidad;
                    console.log(this.qlts);
                    this.cuentaLitros = this.cuentaLitros + parseInt(cantidad);
                    descuento = 0;
                    descuentoTotal = 0;
                    totalProducto = this.productoSQLite.precio * cantidad;
                    totalNeto = (totalProducto);
                    console.log("Total descuento: ₡", 0)
                    console.log("Monto total: ₡", totalNeto);

                  } else if ((this.productoSQLite.producto === 'L' && data.txtAplicaDescuento === '') || (this.productoSQLite.producto === 'L' && data.txtAplicaDescuento.length > 0)) {
                    this.qlts = cantidad;
                    console.log(this.qlts);
                    this.cuentaLitros = this.cuentaLitros + parseInt(cantidad);
                    descuento = parseFloat(this.descuento);
                    console.log("Este es el descuento:", descuento);
                    console.log("Descuento BD:", this.descuento);
                    descuentoTotal = descuento * cantidad;
                    totalProducto = this.productoSQLite.precio * cantidad;
                    totalNeto = (totalProducto) - descuentoTotal;
                    console.log("Descuento unitario: ₡", descuento);
                    console.log("Total productos: ₡", totalProducto)
                    console.log("Total descuento: ₡", descuentoTotal);
                    console.log("Monto total: ₡", totalNeto);
                  }

                  if (this.productoSQLite.producto === 'K' && data.txtAplicaDescuento.toLowerCase() === 'no') {
                    this.qkgs = cantidad;
                    console.log(this.qkgs);
                    this.cuentaKilos = this.cuentaKilos + parseInt(cantidad);
                    descuento = 0;
                    descuentoTotal = 0;
                    totalProducto = this.lprecioc * cantidad;
                    totalNeto = (totalProducto);
                    console.log("Total descuento: ₡", 0)
                    console.log("Monto total: ₡", totalNeto);

                  } else if ((this.productoSQLite.producto === 'K' && data.txtAplicaDescuento === '') || (this.productoSQLite.producto === 'K' && data.txtAplicaDescuento.length > 0)) {
                    this.qkgs = cantidad;
                    console.log(this.qkgs);
                    this.cuentaKilos = this.cuentaKilos + parseInt(cantidad);
                    descuento = parseFloat(this.descuento);
                    descuentoTotal = descuento * cantidad;
                    this.productoSQLite.precio = this.lprecioc; //le asigno el precio especial que tiene el cliente
                    totalProducto = this.lprecioc * cantidad;
                    totalNeto = (totalProducto) - descuentoTotal;
                    console.log("Descuento unitario: ₡", descuento);
                    console.log("Total productos: ₡", totalProducto)
                    console.log("Total descuento: ₡", descuentoTotal);
                    console.log("Monto total: ₡", totalNeto);
                  }
                }

                if (this.tipo === '11') {
                  if (this.productoSQLite.producto === 'Car' && data.txtAplicaDescuento.toLowerCase() === 'no') {
                    this.qlts = cantidad;
                    console.log(this.qlts);
                    this.cuentaLitros = this.cuentaLitros + parseInt(cantidad);
                    descuento = 0;
                    descuentoTotal = 0;
                    totalProducto = this.productoSQLite.precio * cantidad;
                    totalNeto = (totalProducto);
                    console.log("Total descuento: ₡", 0)
                    console.log("Monto total: ₡", totalNeto);

                  } else if ((this.productoSQLite.producto === 'Car' && data.txtAplicaDescuento === '') || (this.productoSQLite.producto === 'Car' && data.txtAplicaDescuento.length > 0)) {
                    this.qlts = cantidad;
                    console.log(this.qlts);
                    this.cuentaLitros = this.cuentaLitros + parseInt(cantidad);
                    descuento = parseInt(this.descuento);
                    descuentoTotal = descuento * cantidad;
                    totalProducto = this.productoSQLite.precio * cantidad;
                    totalNeto = (totalProducto) - descuentoTotal;
                    console.log("Descuento unitario: ₡", descuento);
                    console.log("Total productos: ₡", totalProducto)
                    console.log("Total descuento: ₡", descuentoTotal);
                    console.log("Monto total: ₡", totalNeto);
                  }
                }

                if (this.precioviejo === false) {
                  //aqui se arma el detalle de cada linea de producto, adentro se ve cada uno de sus atributos
                  var total = parseInt(data.txtCantidad) * this.productoSQLite.precio;
                  var productoArmado = {
                    'cantidad': parseInt(data.txtCantidad),
                    'detalle': this.productoSQLite.descripcion,
                    'unidadMedida': 'unid',
                    'precioUnitario': this.productoSQLite.precio,
                    'precioConDescuento': this.productoSQLite.precio - descuento,
                    'descuentoUnitario': descuento,
                    'descuentoTotal': descuentoTotal,
                    'montoTotalSinDescuento': total.toFixed(2),
                    'montoTotalConDescuento': totalNeto,
                    'montoTotal': total.toFixed(2),
                    'subtotal': total.toFixed(2),
                    'montoTotalLinea': total.toFixed(2)
                  };

                  var detalleTanque = {


                    'serietanque': serieT,
                    'cantidadLtKg': parseInt(data.txtCantidad)
                  };


                  console.log(detalleTanque);
                } else {
                  //aqui se arma el detalle de cada linea de producto, adentro se ve cada uno de sus atributos
                  var totalPrecioViejo = parseInt(data.txtCantidad) * this.productoSQLite.precioViejo;
                  var productoArmado = {
                    'cantidad': parseInt(data.txtCantidad),
                    'detalle': this.productoSQLite.descripcion,
                    'unidadMedida': 'unid',
                    'precioUnitario': this.productoSQLite.precioViejo,
                    'precioConDescuento': this.productoSQLite.precioViejo - descuento,
                    'descuentoUnitario': descuento,
                    'descuentoTotal': descuentoTotal,
                    'montoTotalSinDescuento': totalPrecioViejo.toFixed(2),
                    'montoTotalConDescuento': totalNeto,
                    'montoTotal': totalPrecioViejo.toFixed(2),
                    'subtotal': totalPrecioViejo.toFixed(2),
                    'montoTotalLinea': totalPrecioViejo.toFixed(2)
                  };

                  var detalleTanque = {
                    'serietanque': serieT,
                    'cantidadLtKg': parseInt(data.txtCantidad)
                  };


                  console.log(detalleTanque);
                }

                //despues cada linea se inserta en un arreglo para posteriormene convertir dicho arreglo en string
                //y guardarlo en base de datos
                this.productosSeleccionados.push(productoArmado);
                this.detallesTanques.push(detalleTanque);

                console.log(this.productosSeleccionados);
                console.log(this.detallesTanques);

                //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
                this.totalFinal = (this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0));
                this.totalFinalConDescuento = (this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0));
                this.totalDescuentosUnitarios = (this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0));
                //corrijo los decimales para que solo marque 2
                this.totalFinalDecimales = this.totalFinal.toFixed(2);
                this.decimales = this.totalFinalConDescuento.toFixed(2);
                console.log("El monto total es:" + this.totalFinal);
                console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
                console.log("El descuento total:" + this.totalDescuentosTotales);
              }
            }
          }
        ]
      });
      alert.present();

    }, (error) => {
      console.log(error);
    })
  }


  //metodo que se encarga de eliminar una linea de producto en la pantalla de facturacion y restablecer
  //el totalFinal y totalFinalConDescuento
  eliminarLinea(detalle: string) {

    var resta;

    for (var indice in this.productosSeleccionados) {

      var detail = this.productosSeleccionados[indice].detalle;
      resta = this.productosSeleccionados[indice].cantidad;
      console.log("Este es la linea 1561", detail);
      if (detail == detalle) {

        var index = indice;
        var producto = detail;
        var cantidad = resta;
      }


    }

    if (producto === '10lib Pre') {
      console.log("Este es la linea 1572", producto);
      this.q10 = 0;
      this.cuenta10 = this.cuenta10 - cantidad;
      this.contadorVariable = this.contadorVariable - (resta * 0.4);
      if (this.contadorVariable >= 1 && this.contadorVariable < 11) {

        var descuentoVariable;
        descuentoVariable = 1574;
        this.descuento = descuentoVariable;

        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 11 && this.contadorVariable < 21) {

        var descuentoVariable;
        descuentoVariable = 1754;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }



        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 21 && this.contadorVariable < 31) {

        var descuentoVariable;
        descuentoVariable = 1858;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 31 && this.contadorVariable < 41) {

        var descuentoVariable;
        descuentoVariable = 1898;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 41 && this.contadorVariable < 51) {

        var descuentoVariable;
        descuentoVariable = 1908;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 51 && this.contadorVariable < 61) {

        var descuentoVariable;
        descuentoVariable = 1923;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 61) {

        var descuentoVariable;
        descuentoVariable = 1933;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }
        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      }
    } else if (producto === '10lib Ros') {
      this.q10r = 0;
      this.cuenta10r = this.cuenta10r - cantidad;
      this.contadorVariable = this.contadorVariable - (resta * 0.4);
      if (this.contadorVariable >= 1 && this.contadorVariable < 11) {

        var descuentoVariable;
        descuentoVariable = 1574;
        this.descuento = descuentoVariable;

        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 11 && this.contadorVariable < 21) {

        var descuentoVariable;
        descuentoVariable = 1754;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }



        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 21 && this.contadorVariable < 31) {

        var descuentoVariable;
        descuentoVariable = 1858;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 31 && this.contadorVariable < 41) {

        var descuentoVariable;
        descuentoVariable = 1898;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 41 && this.contadorVariable < 51) {

        var descuentoVariable;
        descuentoVariable = 1908;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 51 && this.contadorVariable < 61) {

        var descuentoVariable;
        descuentoVariable = 1923;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 61) {

        var descuentoVariable;
        descuentoVariable = 1933;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }
        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      }
    } else if (producto === '20lib Pre') {
      this.q20 = 0;
      this.cuenta20 = this.cuenta20 - cantidad;
      this.contadorVariable = this.contadorVariable - (resta * 0.8);
      if (this.contadorVariable >= 1 && this.contadorVariable < 11) {

        var descuentoVariable;
        descuentoVariable = 1574;
        this.descuento = descuentoVariable;

        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 11 && this.contadorVariable < 21) {

        var descuentoVariable;
        descuentoVariable = 1754;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }



        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 21 && this.contadorVariable < 31) {

        var descuentoVariable;
        descuentoVariable = 1858;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 31 && this.contadorVariable < 41) {

        var descuentoVariable;
        descuentoVariable = 1898;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 41 && this.contadorVariable < 51) {

        var descuentoVariable;
        descuentoVariable = 1908;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 51 && this.contadorVariable < 61) {

        var descuentoVariable;
        descuentoVariable = 1923;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 61) {

        var descuentoVariable;
        descuentoVariable = 1933;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }
        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      }
    } else if (producto === '25lib Pre') {
      this.q25 = 0;
      this.cuenta25 = this.cuenta25 - cantidad;
      this.contadorVariable = this.contadorVariable - (resta * 1);
      if (this.contadorVariable >= 1 && this.contadorVariable < 11) {

        var descuentoVariable;
        descuentoVariable = 1574;
        this.descuento = descuentoVariable;

        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 11 && this.contadorVariable < 21) {

        var descuentoVariable;
        descuentoVariable = 1754;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }



        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 21 && this.contadorVariable < 31) {

        var descuentoVariable;
        descuentoVariable = 1858;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 31 && this.contadorVariable < 41) {

        var descuentoVariable;
        descuentoVariable = 1898;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 41 && this.contadorVariable < 51) {

        var descuentoVariable;
        descuentoVariable = 1908;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 51 && this.contadorVariable < 61) {

        var descuentoVariable;
        descuentoVariable = 1923;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 61) {

        var descuentoVariable;
        descuentoVariable = 1933;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }
        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      }
    } else if (producto === '25lib Ros') {
      this.q25r = 0;
      this.cuenta25r = this.cuenta25r - cantidad;
      this.contadorVariable = this.contadorVariable - (resta * 1);
      if (this.contadorVariable >= 1 && this.contadorVariable < 11) {

        var descuentoVariable;
        descuentoVariable = 1574;
        this.descuento = descuentoVariable;

        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 11 && this.contadorVariable < 21) {

        var descuentoVariable;
        descuentoVariable = 1754;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }



        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 21 && this.contadorVariable < 31) {

        var descuentoVariable;
        descuentoVariable = 1858;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 31 && this.contadorVariable < 41) {

        var descuentoVariable;
        descuentoVariable = 1898;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 41 && this.contadorVariable < 51) {

        var descuentoVariable;
        descuentoVariable = 1908;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 51 && this.contadorVariable < 61) {

        var descuentoVariable;
        descuentoVariable = 1923;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 61) {

        var descuentoVariable;
        descuentoVariable = 1933;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }
        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      }
    } else if (producto === '30lib Pre') {
      this.q35 = 0;
      this.cuenta30 = this.cuenta30 - cantidad;
      this.contadorVariable = this.contadorVariable - (resta * 1.2);
      if (this.contadorVariable >= 1 && this.contadorVariable < 11) {

        var descuentoVariable;
        descuentoVariable = 1574;
        this.descuento = descuentoVariable;

        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 11 && this.contadorVariable < 21) {

        var descuentoVariable;
        descuentoVariable = 1754;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }



        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 21 && this.contadorVariable < 31) {

        var descuentoVariable;
        descuentoVariable = 1858;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 31 && this.contadorVariable < 41) {

        var descuentoVariable;
        descuentoVariable = 1898;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 41 && this.contadorVariable < 51) {

        var descuentoVariable;
        descuentoVariable = 1908;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 51 && this.contadorVariable < 61) {

        var descuentoVariable;
        descuentoVariable = 1923;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 61) {

        var descuentoVariable;
        descuentoVariable = 1933;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }
        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      }
    } else if (producto === '35lib Pre') {
      this.q35r = 0;
      this.cuenta35r = this.cuenta35r - cantidad;
      this.contadorVariable = this.contadorVariable - (resta * 1.4);
      if (this.contadorVariable >= 1 && this.contadorVariable < 11) {

        var descuentoVariable;
        descuentoVariable = 1574;
        this.descuento = descuentoVariable;

        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 11 && this.contadorVariable < 21) {

        var descuentoVariable;
        descuentoVariable = 1754;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }



        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 21 && this.contadorVariable < 31) {

        var descuentoVariable;
        descuentoVariable = 1858;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 31 && this.contadorVariable < 41) {

        var descuentoVariable;
        descuentoVariable = 1898;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 41 && this.contadorVariable < 51) {

        var descuentoVariable;
        descuentoVariable = 1908;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 51 && this.contadorVariable < 61) {

        var descuentoVariable;
        descuentoVariable = 1923;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 61) {

        var descuentoVariable;
        descuentoVariable = 1933;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }
        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      }
    } else if (producto === '40lib Pre') {
      this.q45 = 0;
      this.cuenta40 = this.cuenta40 - cantidad;
      this.contadorVariable = this.contadorVariable - (resta * 1.6);
      if (this.contadorVariable >= 1 && this.contadorVariable < 11) {

        var descuentoVariable;
        descuentoVariable = 1574;
        this.descuento = descuentoVariable;

        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 11 && this.contadorVariable < 21) {

        var descuentoVariable;
        descuentoVariable = 1754;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }



        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 21 && this.contadorVariable < 31) {

        var descuentoVariable;
        descuentoVariable = 1858;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 31 && this.contadorVariable < 41) {

        var descuentoVariable;
        descuentoVariable = 1898;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 41 && this.contadorVariable < 51) {

        var descuentoVariable;
        descuentoVariable = 1908;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 51 && this.contadorVariable < 61) {

        var descuentoVariable;
        descuentoVariable = 1923;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 61) {

        var descuentoVariable;
        descuentoVariable = 1933;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }
        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      }
    } else if (producto === '45lib Pre') {
      this.q45r = 0;
      this.cuenta45r = this.cuenta45r - cantidad;
      this.contadorVariable = this.contadorVariable - (resta * 1.8);
      if (this.contadorVariable >= 1 && this.contadorVariable < 11) {

        var descuentoVariable;
        descuentoVariable = 1574;
        this.descuento = descuentoVariable;

        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 11 && this.contadorVariable < 21) {

        var descuentoVariable;
        descuentoVariable = 1754;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }



        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 21 && this.contadorVariable < 31) {

        var descuentoVariable;
        descuentoVariable = 1858;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 31 && this.contadorVariable < 41) {

        var descuentoVariable;
        descuentoVariable = 1898;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 41 && this.contadorVariable < 51) {

        var descuentoVariable;
        descuentoVariable = 1908;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 51 && this.contadorVariable < 61) {

        var descuentoVariable;
        descuentoVariable = 1923;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 61) {

        var descuentoVariable;
        descuentoVariable = 1933;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }
        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      }
    } else if (producto === '50lib Pre') {
      this.q60 = 0;
      this.cuenta60 = this.cuenta60 - cantidad;
      this.contadorVariable = this.contadorVariable - (resta * 2);
      if (this.contadorVariable >= 1 && this.contadorVariable < 11) {

        var descuentoVariable;
        descuentoVariable = 1574;
        this.descuento = descuentoVariable;

        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 11 && this.contadorVariable < 21) {

        var descuentoVariable;
        descuentoVariable = 1754;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }



        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 21 && this.contadorVariable < 31) {

        var descuentoVariable;
        descuentoVariable = 1858;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 31 && this.contadorVariable < 41) {

        var descuentoVariable;
        descuentoVariable = 1898;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 41 && this.contadorVariable < 51) {

        var descuentoVariable;
        descuentoVariable = 1908;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 51 && this.contadorVariable < 61) {

        var descuentoVariable;
        descuentoVariable = 1923;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 61) {

        var descuentoVariable;
        descuentoVariable = 1933;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }
        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      }
    } else if (producto === '60lib Ros') {
      this.q60r = 0;
      this.cuenta60r = this.cuenta60r - cantidad;
      this.contadorVariable = this.contadorVariable - (resta * 2.4);
      if (this.contadorVariable >= 1 && this.contadorVariable < 11) {

        var descuentoVariable;
        descuentoVariable = 1574;
        this.descuento = descuentoVariable;

        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 11 && this.contadorVariable < 21) {

        var descuentoVariable;
        descuentoVariable = 1754;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }



        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 21 && this.contadorVariable < 31) {

        var descuentoVariable;
        descuentoVariable = 1858;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 31 && this.contadorVariable < 41) {

        var descuentoVariable;
        descuentoVariable = 1898;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 41 && this.contadorVariable < 51) {

        var descuentoVariable;
        descuentoVariable = 1908;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 51 && this.contadorVariable < 61) {

        var descuentoVariable;
        descuentoVariable = 1923;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 61) {

        var descuentoVariable;
        descuentoVariable = 1933;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }
        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      }
    } else if (producto === '100lib Ro') {
      this.q100 = 0;
      this.q100r = 0;
      this.cuenta100 = this.cuenta100 - cantidad;
      this.contadorVariable = this.contadorVariable - (resta * 4);
      if (this.contadorVariable >= 1 && this.contadorVariable < 11) {

        var descuentoVariable;
        descuentoVariable = 1574;
        this.descuento = descuentoVariable;

        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 11 && this.contadorVariable < 21) {

        var descuentoVariable;
        descuentoVariable = 1754;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }



        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 21 && this.contadorVariable < 31) {

        var descuentoVariable;
        descuentoVariable = 1858;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 31 && this.contadorVariable < 41) {

        var descuentoVariable;
        descuentoVariable = 1898;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 41 && this.contadorVariable < 51) {

        var descuentoVariable;
        descuentoVariable = 1908;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 51 && this.contadorVariable < 61) {

        var descuentoVariable;
        descuentoVariable = 1923;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }

        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      } else if (this.contadorVariable >= 61) {

        var descuentoVariable;
        descuentoVariable = 1933;
        this.descuento = descuentoVariable;


        for (var indice in this.productosSeleccionados) {

          if (this.productosSeleccionados[indice].detalle === '10lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.6));
          }

          if (this.productosSeleccionados[indice].detalle === '20lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable - (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable - (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '25lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '25lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - descuentoVariable;
            this.productosSeleccionados[indice].descuentoUnitario = descuentoVariable;
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * descuentoVariable;
          }

          if (this.productosSeleccionados[indice].detalle === '30lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.2));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.2));

          }

          if (this.productosSeleccionados[indice].detalle === '35lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.4));

          }

          if (this.productosSeleccionados[indice].detalle === '40lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.6));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.6));

          }

          if (this.productosSeleccionados[indice].detalle === '45lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 0.8));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 0.8));

          }

          if (this.productosSeleccionados[indice].detalle === '50lib Pre') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 2);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 2);

          }

          if (this.productosSeleccionados[indice].detalle === '60lib Ros') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable + (descuentoVariable * 1.4));
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable + (descuentoVariable * 1.4));

          }


          if (this.productosSeleccionados[indice].detalle === '100lib Ro') {
            this.productosSeleccionados[indice].precioConDescuento = this.productosSeleccionados[indice].precioUnitario - (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoUnitario = (descuentoVariable * 4);
            this.productosSeleccionados[indice].descuentoTotal = this.productosSeleccionados[indice].cantidad * (descuentoVariable * 4);

          }
        }

        //calculos del totalfinal y totalfinalConDescuento en caso de que tenga para mostrarlo en el pantall
        this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
        this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
        this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
        //corrijo los decimales para que solo marque 2
        this.decimales = this.totalFinalConDescuento.toFixed(2);
        console.log("El monto total es:" + this.totalFinal);
        console.log("El total descuentos unitarios:" + this.totalDescuentosUnitarios);
        console.log("El descuento total:" + this.totalDescuentosTotales);

      }
      console.log(this.contadorVariable);
    } else if (producto === 'L de gas') {
      this.qlts = 0;
      this.cuentaLitros = this.cuentaLitros - cantidad;
    } else if (producto === 'K de gas') {
      this.qkgs = 0;
      this.cuentaKilos = this.cuentaKilos - cantidad;
    }


    this.productosSeleccionados.splice(index, 1);
    console.log(this.productosSeleccionados);

    this.totalFinal = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioUnitario), 0);
    this.totalFinalConDescuento = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.precioConDescuento), 0);
    this.totalDescuentosUnitarios = this.productosSeleccionados.reduce((a, b) => a + (b.cantidad * b.descuentoUnitario), 0);
    //this. totalDescuentosTotales = this.productosSeleccionados.reduce((a,b) => a+(b.cantidad * b.descuentoTotal),0);
    //console.log(JSON.stringify(this.productosSeleccionados));
    this.totalFinalDecimales = this.totalFinal.toFixed(2);
    this.decimales = this.totalFinalConDescuento.toFixed(2);
    console.log(this.totalFinal);
    console.log(this.totalFinalConDescuento);

  }

  //metodo que llama al actualizarConsecutivoRuta de DatabaseProvider para aumentar en uno el consecutivo
  actualizarConsecutivoRuta() {
    this.db.actualizarConsecutivoRuta({
      id: parseInt(this.ruta),
      // nombre: this.nombreRuta,
      // unidad_negocio: this.unidadNegocioRuta,
      // cedi: this.cediRuta,
      // descripcion: this.descripcionRuta,
      // cerosTOM:this.cerosTOM,
      consecutivo: this.consecutivoRuta + 1
    }).then((data) => {
      console.log(data);
    }).catch(error => { console.error(error) });
  }

  guardarClave() {
    var fecha = new Date();
    var day;

    var dia = fecha.getDate();
    var mes = fecha.getMonth();
    var year = fecha.getFullYear();

    var horas = fecha.getHours();
    var minut = fecha.getMinutes();
    var segun = fecha.getSeconds();
    var minutos;
    var segundos;
    var meses = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

    if (minut <= 9) {
      minutos = '0' + minut.toString();
    } else {
      minutos = minut.toString();
    }

    if (segun <= 9) {
      segundos = '0' + segun.toString();
    } else {
      segundos = segun.toString();
    }

    if (dia <= 9) {
      day = '0' + dia.toString();
    } else {
      day = dia.toString();
    }

    var fechaCompleta = day + '-' + meses[mes] + '-' + year + ' ' + horas + ':' + minutos + ':' + segundos;
    var fechaDT = year + '-' + meses[mes] + '-' + day + ' ' + horas + ':' + minutos + ':' + segundos;

    var clave = {
      clave: this.claveFactura,
      consecutivo: this.consecutivoFactura,
      facnumero: this.TOM,
      fecha: fechaDT
    }

    if (this.claveFactura !== '') {
      this.db.guardarClaves(clave).then(data => {
        console.log(data);
        console.log("clave guardada con exito")
      }).catch(error => { console.error(error) })
    }
  }

  //metodo que llama al metodo guardarFacturas de DatabaseProvider y armo el objeto factura para guardarlo
  guardarFactura() {

    var fecha = new Date();
    var day;

    var dia = fecha.getDate();
    var mes = fecha.getMonth();
    var year = fecha.getFullYear();

    var horas = fecha.getHours();
    var minut = fecha.getMinutes();
    var segun = fecha.getSeconds();
    var minutos;
    var segundos;
    var meses = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

    if (minut <= 9) {
      minutos = '0' + minut.toString();
    } else {
      minutos = minut.toString();
    }

    if (segun <= 9) {
      segundos = '0' + segun.toString();
    } else {
      segundos = segun.toString();
    }

    if (dia <= 9) {
      day = '0' + dia.toString();
    } else {
      day = dia.toString();
    }

    var fechaCompleta = day + '-' + meses[mes] + '-' + year + ' ' + horas + ':' + minutos + ':' + segundos;
    var fechaDT = year + '-' + meses[mes] + '-' + day + ' ' + horas + ':' + minutos + ':' + segundos;

    var name;

    if (this.GLN === null && this.EAN === null) {
      name = this.nombreCliente;
    } else if (this.GLN != null && this.EAN === null) {
      name = this.nombreCliente + '  ' + this.GLN;
    } else if (this.GLN === null && this.EAN != null) {
      name = this.nombreCliente + '  ' + this.EAN;
    }


    let factura = {
      codigo: this.codigoCliente,
      facnumero: this.TOM,
      razon_social: this.razonCliente,
      nombre: name,
      descuento: this.descuento,
      descuentot: this.totalDescuentosUnitarios,
      credito: this.credito,
      total: this.totalFinalConDescuento,
      ruta: this.ruta,
      fecha: fechaCompleta,
      q25: this.cuenta25,
      q25r: this.cuenta25r,
      q20: this.cuenta20,
      q20r: this.cuenta20r,
      q10: this.cuenta10,
      q10r: this.cuenta10r,
      q35: this.cuenta30,
      q35r: this.cuenta35r,
      q45: this.cuenta40,
      q45r: this.cuenta45r,
      q60: this.cuenta60,
      q60r: this.cuenta60r,
      q100: this.cuenta100,
      qtls: this.cuentaLitros,
      qkgs: this.cuentaKilos,
      LAT: this.miLat,
      LONGI: this.miLong,
      g: this.g,
      efectivo: this.totalFinalConDescuento,
      transmonto: this.transmonto,
      transnumero: this.transnumero,
      chknumero: this.chknumero,
      chkbanco: this.chkbanco,
      chkmonto: this.chkmonto,
      tipo: this.tipoCliente,
      cz25: this.cz25,
      cz25r: this.cz25r,
      cz100r: this.cz100r,
      fechadt: fechaDT,
      enviada: '0'
    }

    if (this.nombre !== '') {
      this.db.guardarFacturas(factura).then(data => {
        console.log("Factura generada", data);
        console.log("Factura guardada con exito");

      }).catch(error => { console.error(error) })
    }
  }

  //metodo que llama al metodo guardarFacturas de DatabaseProvider y armo el objeto factura para guardarlo
  guardarDetalleFactura() {

    var fecha = new Date();
    var dia = fecha.getDate();
    var mes = fecha.getMonth();
    var year = fecha.getFullYear();

    var horas = fecha.getHours();
    var minut = fecha.getMinutes();
    var segun = fecha.getSeconds();
    var minutos;
    var segundos;
    var meses = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

    if (minut <= 9) {
      minutos = '0' + minut.toString();
    } else {
      minutos = minut.toString();
    }

    if (segun <= 9) {
      segundos = '0' + segun.toString();
    } else {
      segundos = segun.toString();
    }

    var fechaDT = year + '-' + meses[mes] + '-' + dia + ' ' + horas + ':' + minutos + ':' + segundos;

    if (this.numOC === '') {
      this.numOC = '0';
    }

    let detalle = {
      idcliente: this.codigoCliente,
      ruta: this.ruta,
      facnumero: this.TOM,
      totalexenta: this.totalFinalConDescuento,
      total: this.totalFinalConDescuento,
      fechacreacion: fechaDT,
      detalle: JSON.stringify(this.productosSeleccionados),
      rawdetalle: this.numOC,
      detail: this.numOC,
      corre: this.consecutivoRuta,
      estado: 'PENDIENTE',
      enviada: 0
    }

    if (this.ruta !== '') {
      this.db.guardarDetalle(detalle).then(data => {
        console.log(data);
      }).catch(error => { console.error(error) })
    }
  }

  guardarDetalleTanque() {
    var fecha = new Date();
    var day;

    var dia = fecha.getDate();
    var mes = fecha.getMonth();
    var year = fecha.getFullYear();

    var horas = fecha.getHours();
    var minut = fecha.getMinutes();
    var segun = fecha.getSeconds();
    var minutos;
    var segundos;
    var meses = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

    if (minut <= 9) {
      minutos = '0' + minut.toString();
    } else {
      minutos = minut.toString();
    }

    if (segun <= 9) {
      segundos = '0' + segun.toString();
    } else {
      segundos = segun.toString();
    }

    if (dia <= 9) {
      day = '0' + dia.toString();
    } else {
      day = dia.toString();
    }

    var fechaCompleta = day + '-' + meses[mes] + '-' + year + ' ' + horas + ':' + minutos + ':' + segundos;


    var detailTanque = {
      'ruta': this.ruta,
      'numfactura': this.TOM,
      'detalle': JSON.stringify(this.detallesTanques),
      'codcliente': this.codigoCliente,
      'fecha': fechaCompleta
    };

    if (this.ruta !== '') {
      this.db.guardarDetalleTanque(detailTanque).then(data => {
        console.log(data);
        console.log("Detalle tanque guardado con exito")

      }).catch(error => { console.error(error) })
    }
  }

  presentAlert() {
    let alert = this.aCtrl.create({
      title: 'Factura y detalle creados correctamente',
      subTitle: this.nombre,
      buttons: ['Aceptar']
    });
    alert.present();
  }

  //metodo que activa el guardar Facturas, Detalle y actualizar consecutivo
  eventos() {
    this.guardarFactura();
    this.guardarDetalleFactura();
    this.guardarDetalleTanque();
    this.presentAlert();
    this.actualizarConsecutivoRuta();
  }

  // //metodo que redirige a la vista de facturas
  // irAFacturas(facnumero: object){
  //   this.navCtrl.push(FacturasPage, facnumero);
  // }

  //metodo que lleva a la vista de imprimir
  IrAImprimir() {
    this.navCtrl.push(ImprimirPage, {
      TOM: this.TOM, GLN: this.GLN, EAN: this.EAN,
      claveFactura: this.claveFactura, consecutivoFactura: this.consecutivoFactura, razonSocial: this.razonCliente,
      nombre: this.nombreCliente, codigo: this.codigoCliente, idRuta: this.ruta
    });
  }

  //alerta que aparece cuando tienen que verificar si lleno la informacion
  mensajeDeSeguridad() {
    let alert = this.aCtrl.create({
      title: '¿Está completamente seguro de que completó toda la información?',
      subTitle: 'Verifique si no esta seguro!!',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: () => {
            console.log('Confirm Okay');
            this.guardarFactura();
            this.guardarDetalleFactura();
            this.guardarClave();
            this.guardarDetalleTanque();
            this.presentAlert();
            this.actualizarConsecutivoRuta();
            this.IrAImprimir();
          }
        }
      ]
    });
    alert.present();
  }

}
