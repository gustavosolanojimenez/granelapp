import { SqliteDbProvider } from './../../providers/sqlite-db/sqlite-db';
import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';


/**
 * Generated class for the UbicacionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-ubicacion',
  templateUrl: 'ubicacion.html',
})
export class UbicacionPage {

  nombre;
  razonSocial;
  data;
  codigo;
  ruta;
  LAT;
  LONGI;

  constructor(public navCtrl: NavController, public navParams: NavParams, public db: SqliteDbProvider) {

    this.data = this.navParams.get('codigo');
    this.codigo = this.navParams.get('codigo');
    this.nombre = this.navParams.get('nombre');
    this.razonSocial = this.navParams.get('razon');
    this.ruta = this.navParams.get('id');
    this.LAT = this.navParams.get('LAT');
    this.LONGI = this.navParams.get('LONGI');

  }

  ionViewDidLoad() {
    this.irawaze();
  }

  irawaze(){
    this.navCtrl.push('https://www.waze.com/ul?ll={{LAT}}%2C{{LONGI}}&navigate=yes&zoom=17');
  }

}
