import { Component, Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { InicioPage } from '../pages/inicio/inicio';
import { SqliteDbProvider } from '../providers/sqlite-db/sqlite-db';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { enviroment } from '../env/environment';




@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  
  rootPage:any = InicioPage;
   apiURL= enviroment.url;
 

 

 
  // apiURL = 'http://174.138.38.52:3030/api/';
   //apiURL = 'https://api-tomza.herokuapp.com/api/';

  rutasAPI:any = [];
  rutasSQLite:any = [];
  cantonsSQLite:any = [];
  cantonesAPI:any = [];
  distritosSQLite:any = [];
  distritosAPI:any = [];


  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public db: SqliteDbProvider,public http: HttpClient) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    this.cargarRutas();
    this.cargarCantones();
    this.cargarDistritos();
  }


  //metodo que trae las rutas desde el api para posteriormente poder almacenarlas en SQLite
  cargarRutas(){

    // allRutasLoadingController.present();
    this.db.getRutasSQLite().then((data) => {
      this.rutasSQLite = data;
      console.log(this.rutasSQLite);
      if(this.rutasSQLite.length <= 0){

        let data:Observable<any> = this.http.get(`${this.apiURL}rutas`);

        data.subscribe(result => {
          //allRutasLoadingController.dismiss();
          this.rutasAPI = result;
          console.log('Estas son las rutas',this.rutasAPI);

           //recorro las rutas que obtuve del api
            for(var index in this.rutasAPI){
              //armo cada ruta para despues almacenarlas
              let ruta = {
              id: this.rutasAPI[index].id,
              nombre: this.rutasAPI[index].nombre,
              unidad_negocio: this.rutasAPI[index].unidad_negocio,
              cedi: this.rutasAPI[index].cedi,
              descripcion: this.rutasAPI[index].descripcion,
              cerosTOM: this.rutasAPI[index].cerosTOM,
              consecutivo: this.rutasAPI[index].consecutivo
              }

              //console.log(ruta);
              //llamo al metodo del provider para pasar el objeto ruta y que lo almacene en SQLite
              this.db.guardarRutasSQLite(ruta);
            }

            this.db.getRutasSQLite();

          return result;


        })
      }else{
        console.log("Ya las rutas estan cargadas");
      }
    },(error) => {
      console.log(error);
    })


  }

  //metodo que trae los cantones desde el api para posteriormente poder almacenarlas en SQLite
  cargarCantones(){

    // allRutasLoadingController.present();
    this.db.getCantonesSQLite().then((data) => {
      this.cantonsSQLite = data;
      console.log(this.cantonsSQLite);
      if(this.cantonsSQLite.length <= 0){

        let data:Observable<any> = this.http.get(`${this.apiURL}cantons`);

        data.subscribe(result => {
          //allRutasLoadingController.dismiss();
          this.cantonesAPI = result;
          console.log('Estas son las cantones',this.cantonesAPI);

           //recorro las rutas que obtuve del api
            for(var index in this.cantonesAPI){
              //armo cada ruta para despues almacenarlas
              let canton = {
              id: this.cantonesAPI[index].id,
              idProvincia: this.cantonesAPI[index].idProvincia,
              codigo: this.cantonesAPI[index].codigo,
              canton: this.cantonesAPI[index].canton
              }

              //console.log(ruta);
              //llamo al metodo del provider para pasar el objeto ruta y que lo almacene en SQLite
              this.db.guardarCantonesSQLite(canton);
            }

            this.db.getCantonesSQLite();

          return result;


        })
      }else{
        console.log("Ya los cantones estan cargadas");
      }
    },(error) => {
      console.log(error);
    })


  }

   //metodo que trae las rutas desde el api para posteriormente poder almacenarlas en SQLite
   cargarDistritos(){

    // allRutasLoadingController.present();
    this.db.getDistritosSQLite().then((data) => {
      this.distritosSQLite = data;
      console.log(this.distritosSQLite);
      if(this.distritosSQLite.length <= 0){

        let data:Observable<any> = this.http.get(`${this.apiURL}distritos`);

        data.subscribe(result => {
          //allRutasLoadingController.dismiss();
          this.distritosAPI = result;
          console.log('Estas son las distritos',this.distritosAPI);

           //recorro las rutas que obtuve del api
            for(var index in this.distritosAPI){
              //armo cada ruta para despues almacenarlas
              let distrito = {
              id: this.distritosAPI[index].id,
              idCanton: this.distritosAPI[index].idCanton,
              codigo: this.distritosAPI[index].codigo,
              distrito: this.distritosAPI[index].distrito,
              }

              //console.log(ruta);
              //llamo al metodo del provider para pasar el objeto ruta y que lo almacene en SQLite
              this.db.guardarDistritosSQLite(distrito);
            }

            this.db.getDistritosSQLite();

          return result;


        })
      }else{
        console.log("Ya los distritos estan cargadas");
      }
    },(error) => {
      console.log(error);
    })


  }


}

