import { MenuGranelPage } from './../menu-granel/menu-granel';
import { SqliteDbProvider } from './../../providers/sqlite-db/sqlite-db';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, ToastController, Platform } from 'ionic-angular';
import { commands } from '../../providers/printer/printer/printer-commands';
import { PrinterProvider } from './../../providers/printer/printer/printer';


/**
 * Generated class for the ImprimirPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-imprimir',
  templateUrl: 'imprimir.html',
})
export class ImprimirPage {
  facturaSQLite;
  facqueuetSQLite;
  inputData: any = {};
  receipt: any;
  codigo; facnumero; TOM; razon_social; nombre; fecha; ruta; detalle:any = []; total; detalleFactura=""; credito; GLN; EAN; consecutivoFactura; claveFactura;
  titulo = '  Gas Tomza de  Costa Rica S.A.';
  cedula = '3-101-349880';
  facturaImp;
  detalleImp;
  Footer= 'AUTORIZADO MEDIANTE OFICIO N° DGT-R-033-2019 DE FECHA 01/07/2019 de la DGTD';
  leyenda = 'Este comprobante provisional no puede ser utilizado para el     respaldo de créditos fiscales ni como gastos deducibles';
  articulos;
  productos:any = [];
  copia= '**COPIA**';
  original='**ORIGINAL**';
  credito1="Credito";
  contado="Contado";
  comprobante="Comprobante provisional";
  oc;
  contadorOriginal = 0;
  contadorCopias = 0;
  numeroTra= 'TRA'+ this.ruta;
  Tramite1='Este documento constituye una   constancia de entrega y recibido a satisfacción del cliente del producto por parte de Gas Tomza de Costa Rica S.A. con relacion a la factura número: ';
  tramite='**ORIGINAL TRAMITE**';
  tituloTramiteCopia='*COPIA TRAMITE*';
  Tramite2=' .La factura electrónica será remitida al medio designdo por el cliente, quien asume la  responsabilidad por la exactitud de la información brindada.En  caso de falta de pago, Gas Tomza de Costa Rica S.A. puede ejercer las acciones legales correspondientes con base en la presente constancia.'
  contadorImpresion = 0;
  tipoTiquete:String;
  razonSocial;
  codC;
  idRuta;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadCtrl: LoadingController,
    private toastCtrl: ToastController,
    private printer: PrinterProvider,
    public db: SqliteDbProvider,
    public platform: Platform) {
    //Datos de la pantalla facturacion que vamos a usar para imprimir el tiquete
    this.TOM = this.navParams.get('TOM');
    this.GLN = this.navParams.get('GLN');
    this.EAN = this.navParams.get('EAN');
    this.razonSocial = this.navParams.get('razonSocial');
   // this.consecutivoFactura = this.navParams.get('consecutivoFactura');
   // this.claveFactura = this.navParams.get('claveFactura');
    this.nombre = this.navParams.get('nombre');
    this.idRuta = this.navParams.get('idRuta');
    //this.codigo = this.navParams.get('codigo');


    console.log('El Tom ',this.TOM);
    console.log('Consecutivo ',this.consecutivoFactura);
    console.log('Clave ',this.claveFactura);
    console.log("Este es la linea 53 de imprimir",this.GLN);
    console.log("Esta es la linea 54 de impirmir",this.EAN);
    this.obtenerDetalleDeSQLite();
    this.obtenerFacturaDeSQLite();

    platform.registerBackButtonAction(() => {

    },1);

    if(this.razonSocial === 'Cliente VIP' || this.razonSocial === 'Tiquete Electronico'){
      this.tipoTiquete = 'Tiquete Electronico';
    }else{
      this.tipoTiquete = 'Factura Electronica';
    }
  }
  //metodo que obtiene los clientes almacenados en SQLite utilizando el metodo de DatabaseProvider
  obtenerFacturaDeSQLite(){
    this.db.getFacturaPorTOM(parseInt(this.TOM)).then((data) => {
      this. facturaImp = data;
      this.TOM = this.facturaImp.facnumero;
      this.ruta= this. facturaImp.ruta;

      if(this.GLN === null && this.EAN === null){
        this.nombre = this.facturaImp.nombre;
      }else if(this.GLN != null && this.EAN === null){
        this.nombre = this.facturaImp.nombre;
      }else if(this.GLN === null && this.EAN != null){
        this.nombre = this.facturaImp.nombre;
      }

      console.log("este es el tom",this.facturaImp.facnumero);
      console.log("Este es el codigo:",this.facturaImp.codigo);
      console.log("Este es la razon", this.facturaImp.razon_social);

      this.fecha= this.facturaImp.fecha;
      this.codigo = this.facturaImp.codigo;
      this.razon_social = this.facturaImp.razon_social;
      this.total = this.facturaImp.total;
      this.credito = this.facturaImp.credito;
    }, (error) => {
      console.log(error);
    })
  }

  //metodo que obtiene los detalles de factura almacenados en SQLite utilizando el metodo de DatabaseProvider
  obtenerDetalleDeSQLite() {
    this.db.getDetallePorTOM(parseInt(this.TOM)).then((data) => {
      this.detalle = data;
      this.oc = this.detalle.detail;
      this.articulos = this.detalle.detalle;
      this.productos = JSON.parse(this.articulos);
      console.log(this.productos);


      for (var index in this.productos) {

        var resultado = '';
        var lineaDescuento = '';

        resultado = this.productos[index].cantidad + '  '+this.productos[index].detalle +'   '+this.productos[index].precioUnitario + '    '+this.productos[index].montoTotalSinDescuento + '\n';
        lineaDescuento = '           Descuento:'+ ' - '+this.productos[index].descuentoTotal+ '\n';

        if(this.productos[index].descuentoTotal === 0){
          this.detalleFactura+= resultado
        }else{
          this.detalleFactura+= resultado + lineaDescuento;
        }


      }

      },(error) =>{
      console.log(error);
      })
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ImprimirPage');
  }

  showToast(data) {
    let toast = this.toastCtrl.create({
      duration: 3000,
      message: data,
      position: 'bottom',
    });
    toast.present();
  }

  noSpecialChars(string) {
    var translate = {
      à: 'a',
      á: 'a',
      â: 'a',
      ã: 'a',
      ä: 'a',
      å: 'a',
      æ: 'a',
      ç: 'c',
      è: 'e',
      é: 'e',
      ê: 'e',
      ë: 'e',
      ì: 'i',
      í: '₡',
      î: 'i',
      ï: 'i',
      ð: 'd',
      ñ: 'n',
      ò: 'o',
      ó: 'o',
      ô: 'o',
      õ: 'o',
      ö: 'o',
      ø: 'o',
      ù: 'u',
      ú: 'u',
      û: 'u',
      ü: 'u',
      ý: 'y',
      þ: 'b',
      ÿ: 'y',
      ŕ: 'r',
      À: 'A',
      Á: 'A',
      Â: 'A',
      Ã: 'A',
      Ä: 'A',
      Å: 'A',
      Æ: 'A',
      Ç: 'C',
      È: 'E',
      É: 'E',
      Ê: 'E',
      Ë: 'E',
      Ì: 'I',
      Í: 'I',
      Î: 'I',
      Ï: 'I',
      Ð: 'D',
      Ñ: 'N',
      Ò: 'O',
      Ó: 'O',
      Ô: 'O',
      Õ: 'O',
      Ö: 'O',
      Ø: 'O',
      Ù: 'U',
      Ú: 'U',
      Û: 'U',
      Ü: 'U',
      Ý: 'Y',
      Þ: 'B',
      Ÿ: 'Y',
      Ŕ: 'R',

    },
      translate_re = /[àáâãäåæçèéêëìíîïðñòóôõöøùúûüýþßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŕŕÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÝÝÞŸŔŔ]/gim;
    return string.replace(translate_re, function (match) {
      return translate[match];
    });
  }

  print(device, data) {
    console.log('Device mac: ', device);
    console.log('Data: ', data);
    let load = this.loadCtrl.create({
      content: 'Imprimiendo...',
    });
    load.present();
    this.printer.connectBluetooth(device).subscribe(
      (status) => {
        console.log(status);
        this.printer
          .printData(this.noSpecialChars(data))
          .then((printStatus) => {
            console.log(printStatus);
            let alert = this.alertCtrl.create({
              title: 'Impreso Correctamente!',
              buttons: [
                {
                  text: 'Ok',
                  handler: () => {
                    load.dismiss();
                    this.printer.disconnectBluetooth();
                  },
                },
              ],
              enableBackdropDismiss : false
            });
            alert.present();
          })
          .catch((error) => {
            console.log(error);
            let alert = this.alertCtrl.create({
              title: 'Fue un error en la impresora, Intente de nuevo!',
              buttons: [
                {
                  text: 'Ok',
                  handler: () => {
                    load.dismiss();
                    //this.printer.disconnectBluetooth();
                  },
                },
              ],
            });
            alert.present();
          });
      },
      (error) => {
        console.log(error);
        let alert = this.alertCtrl.create({
          title:
            'Error al conectar la impresora, Intente de nuevo!',
          buttons: [
            {
              text: 'Ok',
              handler: () => {
                load.dismiss();
                //this.printer.disconnectBluetooth();
              },
            },
          ],
        });
        alert.present();
      },
    );
  }

  //metodo para imprimir la factura
  prepareToPrint(data){
  if (this.credito == 0) {
     // u can remove this when generate the receipt using another method
     if (!data.title) {
      data.title = this.titulo;
    }
    if (!data.subtitulo) {
      data.subtitulo = 'Cédula Jur.' + this.cedula;
    }
    if (!data.subtitulo1) {
      data.subtitulo1 = 'La Lima de Cartago, Costa Rica';
    }
    if (!data.subtitulo2) {
      data.subtitulo2 = 'Teléfono: (506) 2201-60-00';
    }
    if (!data.subtitulo3) {
      data.subtitulo3 = 'servicioalcliente.cr@tomza.com';
    }

    // if(!data.consecutivoFactura){
    //   data.consecutivoFactura = 'Consecutivo:'+ this.consecutivoFactura;
    // }
    // if(!data.claveFactura){
    //   data.claveFactura = 'Clave:'+ this.claveFactura;
    // }
    if (!data.facnumero) {
      data.facnumero =
        'TOM: ' + this.TOM;
    }
    if (!data.ruta) {
      data.ruta =
        'Ruta: ' + this.ruta;
    }
    if (!data.fecha) {
      data.fecha =
        'Fecha: ' + this.fecha;
    }
    if (!data.razon_social) {
      data.razon_social =
        'Razon Social: ' + this.razon_social;
    }
    if (!data.nombre) {
      data.nombre =
        'Nombre:' + this.nombre;
    }
    if (!data.codigo) {
      data.codigo =
        'Codigo: ' + this.codigo;
    }
    if (!data.pago) {
      data.pago =
        'Pago: ' + this.credito1;
    }
    if(!data.oc){
      data.oc = 'OC: '+this.oc;
    }
    if (!data.detalle) {
      data.detalle =
        'CANT    ART      P.UNI    TOTAL ';
    }
    if (!data.detalles2) {
      data.detalles2 =this.detalleFactura;
    }
    if (!data.detalle2) {
      data.detalle2 =
        'Total(CRC):'+this.total;
    }
    if (!data.firma1) {
      data.firma1 =
        'Firma Recibido';
    }
    if (!data.firma2) {
      data.firma2 =
        'Nombre';
    }
    if (!data.sello) {
      data.sello =
        'Sellos';
    }
    if (!data.footer) {
      data.footer =
        this.Footer;
    }
    if(!data.original){
      data.original=
      this.original;
    }
    if(!data.comprobante){
      data.comprobante=this.comprobante;
    }
    if(!data.tipoTiquete){
      data.tipoTiquete=this.tipoTiquete;
    }
    if(!data.leyenda){
      data.leyenda = this.leyenda;
    }

    let receipt = '';
    receipt += commands.HARDWARE.HW_INIT;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_ALIGN_CT;
    receipt += data.original;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_2WIDTH;
    receipt += commands.TEXT_FORMAT.TXT_ALIGN_CT;
    receipt += data.title.toUpperCase();
    receipt += commands.EOL;//baba de linea
    receipt += commands.TEXT_FORMAT.TXT_NORMAL;
    receipt += commands.HORIZONTAL_LINE.HR_58MM;
    receipt += commands.EOL;
    receipt += commands.HORIZONTAL_LINE.HR2_58MM;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
    receipt += data.comprobante;
    //receipt += data.tipoTiquete;
    receipt += commands.EOL;
    receipt += data.subtitulo;
    receipt += commands.EOL;
    receipt += data.subtitulo1;
    receipt += commands.EOL;
    receipt += data.subtitulo2;
    receipt += commands.EOL;
    receipt += data.subtitulo3;
    receipt += commands.EOL;
    receipt += commands.HORIZONTAL_LINE.HR2_58MM;
    receipt += commands.EOL;
    receipt += commands.EOL;
    // receipt += data.consecutivoFactura;
    // receipt += commands.EOL;
    // receipt += data.claveFactura;
    // receipt += commands.EOL;
    receipt += data.facnumero;
    receipt += commands.EOL;
    receipt += data.ruta;
    receipt += commands.EOL;
    receipt += data.fecha;
    receipt += commands.EOL;
    receipt += data.razon_social;
    receipt += commands.EOL;
    receipt += data.nombre;
    receipt += commands.EOL;
    receipt += data.codigo;
    receipt += commands.EOL;
    receipt += data.pago;
    receipt += commands.EOL;
    receipt += data.oc;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.HORIZONTAL_LINE.HR2_58MM;
    receipt += commands.TEXT_FORMAT.TXT_NORMAL;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_BOLD_ON;
    receipt += data.detalle;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_NORMAL;
    receipt += data.detalles2;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_BOLD_ON;
    receipt += data.detalle2;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_NORMAL;
    receipt += data.firma1;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_NORMAL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += data.firma2;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_NORMAL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += data.sello;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    //secure space on footer
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += data.footer;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += data.leyenda;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    //this.receipt = receipt;
    this.mountAlertBt(receipt);
  }else{
 // u can remove this when generate the receipt using another method
    if (!data.title) {
      data.title = this.titulo;
    }
    if (!data.subtitulo) {
      data.subtitulo = 'Cédula Jur.' + this.cedula;
    }
    if (!data.subtitulo1) {
      data.subtitulo1 = 'La Lima de Cartago, Costa Rica';
    }
    if (!data.subtitulo2) {
      data.subtitulo2 = 'Teléfono: (506) 2201-60-00';
    }
    if (!data.subtitulo3) {
      data.subtitulo3 = 'servicioalcliente.cr@tomza.com';
    }
    // if(!data.consecutivoFactura){
    //   data.consecutivoFactura = 'Consecutivo:'+ this.consecutivoFactura;
    // }
    // if(!data.claveFactura){
    //   data.claveFactura = 'Clave:'+ this.claveFactura;
    // }
    if (!data.facnumero) {
      data.facnumero =
        'TOM: ' + this.TOM;
    }
    if (!data.ruta) {
      data.ruta =
        'Ruta: ' + this.ruta;
    }
    if (!data.fecha) {
      data.fecha =
        'Fecha: ' + this.fecha;
    }
    if (!data.razon_social) {
      data.razon_social =
        'Razon Social: ' + this.razon_social;
    }
    if (!data.nombre) {
      data.nombre =
        'Nombre:' + this.nombre;
    }
    if (!data.codigo) {
      data.codigo =
        'Codigo: ' + this.codigo;
    }
    if (!data.pago) {
      data.pago=
        'Pago: ' + this.contado;
    }
    if(!data.oc){
      data.oc = 'OC: '+this.oc;
    }
    if (!data.detalle) {
      data.detalle =
        'CANT    ART      P.UNI    TOTAL ';
    }
    if (!data.detalles2) {
      data.detalles2 =this.detalleFactura;
    }
    if (!data.detalle2) {
      data.detalle2 =
        'Total(CRC): '+this.total;
    }
    // if (!data.firma1) {
    //   data.firma1 =
    //     'Firma Recibido';
    // }
    // if (!data.firma2) {
    //   data.firma2 =
    //     'Nombre';
    // }
    // if (!data.sello) {
    //   data.sello =
    //     'Sellos';
    // }
    if (!data.footer) {
      data.footer =
        this.Footer;
    }
    if (!data.original) {
      data.original =
        this.original;
    }
    if(!data.comprobante){
      data.comprobante=this.comprobante;
    }
    if(!data.tipoTiquete){
      data.tipoTiquete=this.tipoTiquete;
    }
    if(!data.leyenda){
      data.leyenda = this.leyenda;
    }


    let receipt = '';

    receipt += commands.HARDWARE.HW_INIT;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_ALIGN_CT;
    receipt += data.original;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_2WIDTH;
    receipt += commands.TEXT_FORMAT.TXT_ALIGN_CT;
    receipt += data.title.toUpperCase();
    receipt += commands.EOL;//baba de linea
    receipt += commands.TEXT_FORMAT.TXT_NORMAL;
    receipt += commands.HORIZONTAL_LINE.HR_58MM;
    receipt += commands.EOL;
    receipt += commands.HORIZONTAL_LINE.HR2_58MM;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
    receipt += data.comprobante;
    //receipt += data.tipoTiquete;
    receipt += commands.EOL;
    receipt += data.subtitulo;
    receipt += commands.EOL;
    receipt += data.subtitulo1;
    receipt += commands.EOL;
    receipt += data.subtitulo2;
    receipt += commands.EOL;
    receipt += data.subtitulo3;
    receipt += commands.EOL;
    receipt += commands.HORIZONTAL_LINE.HR2_58MM;
    receipt += commands.EOL;
    receipt += commands.EOL;
    // receipt += data.consecutivoFactura;
    // receipt += commands.EOL;
    // receipt += data.claveFactura;
    // receipt += commands.EOL;
    receipt += data.facnumero;
    receipt += commands.EOL;
    receipt += data.ruta;
    receipt += commands.EOL;
    receipt += data.fecha;
    receipt += commands.EOL;
    receipt += data.razon_social;
    receipt += commands.EOL;
    receipt += data.nombre;
    receipt += commands.EOL;
    receipt += data.codigo;
    receipt += commands.EOL;
    receipt += data.pago;
    receipt += commands.EOL;
    receipt += data.oc;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.HORIZONTAL_LINE.HR2_58MM;
    receipt += commands.TEXT_FORMAT.TXT_NORMAL;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_BOLD_ON;
    receipt += data.detalle;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_NORMAL;
    receipt += data.detalles2;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_BOLD_ON;
    receipt += data.detalle2;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_NORMAL;
    // receipt += data.firma1;
    // receipt += commands.EOL;
    // receipt += commands.EOL;
    // receipt += commands.EOL;
    // receipt += commands.EOL;
    // receipt += commands.TEXT_FORMAT.TXT_NORMAL;
    // receipt += commands.EOL;
    // receipt += commands.EOL;
    // receipt += commands.EOL;
    // receipt += commands.EOL;
    // receipt += data.firma2;
    // receipt += commands.EOL;
    // receipt += commands.EOL;
    // receipt += commands.EOL;
    // receipt += commands.EOL;
    // receipt += commands.TEXT_FORMAT.TXT_NORMAL;
    // receipt += commands.EOL;
    // receipt += commands.EOL;
    // receipt += commands.EOL;
    // receipt += commands.EOL;
    // receipt += data.sello;
    // receipt += commands.EOL;
    // receipt += commands.EOL;
    // receipt += commands.EOL;
    // //secure space on footer
    // receipt += commands.EOL;
    // receipt += commands.EOL;
    // receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += data.footer;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += data.leyenda;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    //this.receipt = receipt;
    this.mountAlertBt(receipt);
  }}

  //para imprimir la copia de la factura
  prepareToPrintCopy(data){
    if (this.credito == 0) {
       // u can remove this when generate the receipt using another method
       if (!data.title) {
        data.title = this.titulo;
      }
      if (!data.subtitulo) {
        data.subtitulo = 'Cédula Jur.' + this.cedula;
      }
      if (!data.subtitulo1) {
        data.subtitulo1 = 'La Lima de Cartago, Costa Rica';
      }
      if (!data.subtitulo2) {
        data.subtitulo2 = 'Teléfono: (506) 2201-60-00';
      }
      if (!data.subtitulo3) {
        data.subtitulo3 = 'servicioalcliente.cr@tomza.com';
      }
      // if(!data.consecutivoFactura){
      //   data.consecutivoFactura = 'Consecutivo:'+ this.consecutivoFactura;
      // }
      // if(!data.claveFactura){
      //   data.claveFactura = 'Clave:'+ this.claveFactura;
      // }
      if (!data.facnumero) {
        data.facnumero =
          'TOM: ' + this.TOM;
      }
      if (!data.ruta) {
        data.ruta =
          'Ruta: ' + this.ruta;
      }
      if (!data.fecha) {
        data.fecha =
          'Fecha: ' + this.fecha;
      }
      if (!data.razon_social) {
        data.razon_social =
          'Razon Social: ' + this.razon_social;
      }
      if (!data.nombre) {
        data.nombre =
          'Nombre:' + this.nombre;
      }
      if (!data.codigo) {
        data.codigo =
          'Codigo: ' + this.codigo;
      }
      if (!data.pago) {
        data.pago=
          'Pago: ' + this.credito1;
      }
      if(!data.oc){
        data.oc = 'OC: '+this.oc;
      }
      if (!data.detalle) {
        data.detalle =
          'CANT    ART      P.UNI    TOTAL ';
      }
      if (!data.detalles2) {
        data.detalles2 =this.detalleFactura;
      }
      if (!data.detalle2) {
        data.detalle2 =
          'Total(CRC): '+this.total;
      }
      if (!data.firma1) {
        data.firma1 =
          'Firma Recibido';
      }
      if (!data.firma2) {
        data.firma2 =
          'Nombre';
      }
      if (!data.sello) {
        data.sello =
          'Sellos';
      }
      if (!data.footer) {
        data.footer =
          this.Footer;
      }
      if (!data.Copia) {
        data.Copia =
          this.copia;
      }
      if(!data.tipoTiquete){
        data.tipoTiquete=this.tipoTiquete;
      }
      if(!data.comprobante){
        data.comprobante=this.comprobante;
      }
      if(!data.leyenda){
        data.leyenda = this.leyenda;
      }




      let receipt = '';
      receipt += commands.HARDWARE.HW_INIT;
      receipt += commands.TEXT_FORMAT.TXT_ALIGN_CT;
      receipt += data.Copia;
      receipt += commands.EOL;
      receipt += commands.TEXT_FORMAT.TXT_2WIDTH;
      receipt += commands.TEXT_FORMAT.TXT_ALIGN_CT;
      receipt += data.title.toUpperCase();
      receipt += commands.EOL;//salto de linea
      receipt += commands.TEXT_FORMAT.TXT_NORMAL;
      receipt += commands.HORIZONTAL_LINE.HR_58MM;
      receipt += commands.EOL;
      receipt += commands.HORIZONTAL_LINE.HR2_58MM;
      receipt += commands.EOL;
      receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
      receipt += commands.EOL;
      receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
      receipt += data.comprobante;
      //receipt += data.tipoTiquete;
      receipt += commands.EOL;
      receipt += data.subtitulo;
      receipt += commands.EOL;
      receipt += data.subtitulo1;
      receipt += commands.EOL;
      receipt += data.subtitulo2;
      receipt += commands.EOL;
      receipt += data.subtitulo3;
      receipt += commands.EOL;
      receipt += commands.HORIZONTAL_LINE.HR2_58MM;
      receipt += commands.EOL;
      receipt += commands.EOL;
      // receipt += data.consecutivoFactura;
      // receipt += commands.EOL;
      // receipt += data.claveFactura;
      // receipt += commands.EOL;
      receipt += data.facnumero;
      receipt += commands.EOL;
      receipt += data.ruta;
      receipt += commands.EOL;
      receipt += data.fecha;
      receipt += commands.EOL;
      receipt += data.razon_social;
      receipt += commands.EOL;
      receipt += data.nombre;
      receipt += commands.EOL;
      receipt += data.codigo;
      receipt += commands.EOL;
      receipt += data.pago;
      receipt += commands.EOL;
      receipt += data.oc;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.HORIZONTAL_LINE.HR2_58MM;
      receipt += commands.TEXT_FORMAT.TXT_NORMAL;
      receipt += commands.EOL;
      receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.TEXT_FORMAT.TXT_BOLD_ON;
      receipt += data.detalle;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.TEXT_FORMAT.TXT_NORMAL;
      receipt += data.detalles2;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.TEXT_FORMAT.TXT_BOLD_ON;
      receipt += data.detalle2;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.TEXT_FORMAT.TXT_NORMAL;
      receipt += data.firma1;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.TEXT_FORMAT.TXT_NORMAL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += data.firma2;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.TEXT_FORMAT.TXT_NORMAL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += data.sello;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      //secure space on footer
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += data.footer;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += data.leyenda;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      //this.receipt = receipt;
      this.mountAlertBt(receipt);
    }else{
   // u can remove this when generate the receipt using another method
      if (!data.title) {
        data.title = this.titulo;
      }
      if (!data.subtitulo) {
        data.subtitulo = 'Cédula Jur.' + this.cedula;
      }
      if (!data.subtitulo1) {
        data.subtitulo1 = 'La Lima de Cartago, Costa Rica';
      }
      if (!data.subtitulo2) {
        data.subtitulo2 = 'Teléfono: (506) 2201-60-00';
      }
      if (!data.subtitulo3) {
        data.subtitulo3 = 'servicioalcliente.cr@tomza.com';
      }
      if (!data.facnumero) {
        data.facnumero =
          'TOM: ' + this.TOM;
      }
      // if(!data.consecutivoFactura){
      //   data.consecutivoFactura = 'Consecutivo:'+ this.consecutivoFactura;
      // }
      // if(!data.claveFactura){
      //   data.claveFactura = 'Clave:'+ this.claveFactura;
      // }
      if (!data.ruta) {
        data.ruta =
          'Ruta: ' + this.ruta;
      }
      if (!data.fecha) {
        data.fecha =
          'Fecha: ' + this.fecha;
      }
      if (!data.razon_social) {
        data.razon_social =
          'Razon Social: ' + this.razon_social;
      }
      if (!data.nombre) {
        data.nombre =
          'Nombre:' + this.nombre;
      }
      if (!data.codigo) {
        data.codigo =
          'Codigo: ' + this.codigo;
      }
      if(!data.pago){
        data.pago=
        'Pago: ' + this.contado;
      }
      if(!data.oc){
        data.oc = 'OC: '+ this.oc;
      }
      if (!data.detalle) {
        data.detalle =
          'CANT    ART      P.UNI    TOTAL ';
      }
      if (!data.detalles2) {
        data.detalles2 =this.detalleFactura;
      }
      if (!data.detalle2) {
        data.detalle2 =
          'Total(CRC): '+this.total;
      }
      // if (!data.firma1) {
      //   data.firma1 =
      //     'Firma Recibido';
      // }
      // if (!data.firma2) {
      //   data.firma2 =
      //     'Nombre';
      // }
      // if (!data.sello) {
      //   data.sello =
      //     'Sellos';
      // }
      if (!data.footer) {
        data.footer =
          this.Footer;
      }
      if (!data.Copia) {
        data.Copia =
          this.copia;
      }
      if(!data.comprobante){
        data.comprobante=this.comprobante;
      }
      if(!data.tipoTiquete){
        data.tipoTiquete=this.tipoTiquete;
      }
      if(!data.leyenda){
        data.leyenda = this.leyenda;
      }



      let receipt = '';
      receipt += commands.HARDWARE.HW_INIT;
      receipt += commands.TEXT_FORMAT.TXT_ALIGN_CT;
      receipt += data.Copia;
      receipt += commands.EOL;
      receipt += commands.TEXT_FORMAT.TXT_2WIDTH;
      receipt += commands.TEXT_FORMAT.TXT_ALIGN_CT;
      receipt += data.title.toUpperCase();
      receipt += commands.EOL;//baba de linea
      receipt += commands.TEXT_FORMAT.TXT_NORMAL;
      receipt += commands.HORIZONTAL_LINE.HR_58MM;
      receipt += commands.EOL;
      receipt += commands.HORIZONTAL_LINE.HR2_58MM;
      receipt += commands.EOL;
      receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
      receipt += commands.EOL;
      receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
      receipt += data.comprobante;
      //receipt += data.tipoTiquete;
      receipt += commands.EOL;
      receipt += data.subtitulo;
      receipt += commands.EOL;
      receipt += data.subtitulo1;
      receipt += commands.EOL;
      receipt += data.subtitulo2;
      receipt += commands.EOL;
      receipt += data.subtitulo3;
      receipt += commands.EOL;
      receipt += commands.HORIZONTAL_LINE.HR2_58MM;
      receipt += commands.EOL;
      receipt += commands.EOL;
      // receipt += data.consecutivoFactura;
      // receipt += commands.EOL;
      // receipt += data.claveFactura;
      // receipt += commands.EOL;
      receipt += data.facnumero;
      receipt += commands.EOL;
      receipt += data.ruta;
      receipt += commands.EOL;
      receipt += data.fecha;
      receipt += commands.EOL;
      receipt += data.razon_social;
      receipt += commands.EOL;
      receipt += data.nombre;
      receipt += commands.EOL;
      receipt += data.codigo;
      receipt += commands.EOL;
      receipt += data.pago;
      receipt += commands.EOL;
      receipt += data.oc;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.HORIZONTAL_LINE.HR2_58MM;
      receipt += commands.TEXT_FORMAT.TXT_NORMAL;
      receipt += commands.EOL;
      receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.TEXT_FORMAT.TXT_BOLD_ON;
      receipt += data.detalle;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.TEXT_FORMAT.TXT_NORMAL;
      receipt += data.detalles2;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.TEXT_FORMAT.TXT_BOLD_ON;
      receipt += data.detalle2;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.TEXT_FORMAT.TXT_NORMAL;
      // receipt += data.firma1;
      // receipt += commands.EOL;
      // receipt += commands.EOL;
      // receipt += commands.EOL;
      // receipt += commands.EOL;
      // receipt += commands.TEXT_FORMAT.TXT_NORMAL;
      // receipt += commands.EOL;
      // receipt += commands.EOL;
      // receipt += commands.EOL;
      // receipt += commands.EOL;
      // receipt += data.firma2;
      // receipt += commands.EOL;
      // receipt += commands.EOL;
      // receipt += commands.EOL;
      // receipt += commands.EOL;
      // receipt += commands.TEXT_FORMAT.TXT_NORMAL;
      // receipt += commands.EOL;
      // receipt += commands.EOL;
      // receipt += commands.EOL;
      // receipt += commands.EOL;
      // receipt += data.sello;
      // receipt += commands.EOL;
      // receipt += commands.EOL;
      // receipt += commands.EOL;
      // //secure space on footer
      // receipt += commands.EOL;
      // receipt += commands.EOL;
      // receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += data.footer;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += data.leyenda;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      //this.receipt = receipt;
      this.mountAlertBt(receipt);
  }}


  mountAlertBt(data) {
    this.receipt = data;
    let alert = this.alertCtrl.create({
      title: 'Seleccione su Impresora',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
        },
        {
          text: 'Seleccionar Impresora',
          handler: (device) => {
            if (!device) {
              this.showToast('Seleccionar Impresora');
              return false;
            }
            console.log(device);
            this.print(device, this.receipt);
          },
        },
      ],
      enableBackdropDismiss : false
    });
    this.printer
      .enableBluetooth()
      .then(() => {
        this.printer
          .searchBluetooth()
          .then((devices) => {
            devices.forEach((device) => {
              console.log('Devices: ', JSON.stringify(device));
              alert.addInput({
                name: 'printer',
                value: device.address,
                label: device.name,
                type: 'radio',
              });
            });
            alert.present();
          })
          .catch((error) => {
            console.log(error);
            this.showToast(
              'There was an error connecting the printer, Intente de nuevo!',
            );
            this.mountAlertBt(this.receipt);
          });
      })
      .catch((error) => {
        console.log(error);
        this.showToast('Error al activar el Bluetooth, Intente de nuevo!');
        this.mountAlertBt(this.receipt);
      });
  }

  impresionOriginal(data){
   this.contadorOriginal+=1;

   var datos = data;
   this.prepareToPrint(datos);

  }

  impresionCopias(data){
    this.contadorCopias+=1;
    this.contadorImpresion+=1;
    var datos = data;
    this.prepareToPrintCopy(datos);


    if(this.contadorCopias === 3){
      this.IrAMenu();
    }
  }


  IrAMenu(){
    this.navCtrl.push(MenuGranelPage,{id:this.idRuta});
  }

  //Procedimientos para imprimir el Tramite
  prepareToPrintTramite(data){
      // u can remove this when generate the receipt using another method
      if (!data.title) {
        data.title = this.titulo;
      }
      if (!data.subtitulo) {
        data.subtitulo = 'Cédula Jur.' + this.cedula;
      }
      if (!data.subtitulo1) {
        data.subtitulo1 = 'La Lima de Cartago, Costa Rica';
      }
      if (!data.subtitulo2) {
        data.subtitulo2 = 'Teléfono: (506) 2201-60-00';
      }
      if (!data.subtitulo3) {
        data.subtitulo3 = 'servicioalcliente.cr@tomza.com';
      }
      if (!data.factura) {
        data.factura =
          'Factura:'+this.TOM;
      }
      if (!data.ruta) {
        data.ruta =
          'Ruta: ' + this.ruta;
      }
      if (!data.fecha) {
        data.fecha =
          'Fecha: ' + this.fecha;
      }
      if (!data.pago) {
        data.pago=
          'Pago: ' + this.credito1;
      }
      if (!data.Total) {
        data.Total =
          'VALOR(CRC):'+this.total;
      }
      if (!data.firma1) {
        data.firma1 =
          'Firma Recibido';
      }
      if (!data.firma2) {
        data.firma2 =
          'Nombre';
      }
      if (!data.sello) {
        data.sello =
          'Sellos';
      }
      if (!data.footer) {
        data.footer =
          this.Footer;
      }
      if (!data.tituloTramite) {
        data.tituloTramite =
          this.tramite;
      }
      if (!data.tramite1) {
        data.tramite1 = this.Tramite1 + this.TOM + this.Tramite2;
      }
      if (!data.tramite2) {
        data.tramite2 =this.Tramite2;
      }
      if (!data.Total) {
        data.Total =
          'VALOR(CRC):'+this.total;
      }
      if (!data.tramite) {
        data.tramite =
        'Tramite N: TRA'+this.TOM ;
      }
      if (!data.Nombre) {
        data.Nombre =
          'Nombre:'+ this.nombre;
      }
      if (!data.ced) {
        data.ced =
          '# Cedula:';
      }




      let receipt = '';
      receipt += commands.HARDWARE.HW_INIT;
      receipt += commands.TEXT_FORMAT.TXT_ALIGN_CT;
      receipt += data.tituloTramite;
      receipt += commands.EOL;
      receipt += commands.TEXT_FORMAT.TXT_2WIDTH;
      receipt += commands.TEXT_FORMAT.TXT_ALIGN_CT;
      receipt += data.title.toUpperCase();
      receipt += commands.EOL;//salto de linea
      receipt += commands.TEXT_FORMAT.TXT_NORMAL;
      receipt += commands.HORIZONTAL_LINE.HR_58MM;
      receipt += commands.EOL;
      receipt += commands.HORIZONTAL_LINE.HR2_58MM;
      receipt += commands.EOL;
      receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
      receipt += commands.EOL;
      receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
      receipt += commands.EOL;
      receipt += data.subtitulo;
      receipt += commands.EOL;
      receipt += data.subtitulo1;
      receipt += commands.EOL;
      receipt += data.subtitulo2;
      receipt += commands.EOL;
      receipt += data.subtitulo3;
      receipt += commands.EOL;
      receipt += commands.HORIZONTAL_LINE.HR2_58MM;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += data.tramite;
      receipt += commands.EOL;
      receipt += data.factura;
      receipt += commands.EOL;
      receipt += data.Nombre;
      receipt += commands.EOL;
      receipt += data.ruta;
      receipt += commands.EOL;
      receipt += data.fecha;
      receipt += commands.EOL;
      receipt += data.Total;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.HORIZONTAL_LINE.HR2_58MM;
      receipt += commands.TEXT_FORMAT.TXT_NORMAL;
      receipt += commands.EOL;
      receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += data.tramite1;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.TEXT_FORMAT.TXT_NORMAL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.TEXT_FORMAT.TXT_NORMAL;
      receipt += data.firma1;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.TEXT_FORMAT.TXT_NORMAL;
      receipt += data.ced;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += data.firma2;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.TEXT_FORMAT.TXT_NORMAL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += data.sello;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      //secure space on footer
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      receipt += commands.EOL;
      //this.receipt = receipt;
      this.mountAlertBt(receipt);
  }


  prepareToPrintTramiteCopy(data){
      // u can remove this when generate the receipt using another method
      if (!data.title) {
      data.title = this.titulo;
    }
    if (!data.subtitulo) {
      data.subtitulo = 'Cédula Jur.' + this.cedula;
    }
    if (!data.subtitulo1) {
      data.subtitulo1 = 'La Lima de Cartago, Costa Rica';
    }
    if (!data.subtitulo2) {
      data.subtitulo2 = 'Teléfono: (506) 2201-60-00';
    }
    if (!data.subtitulo3) {
      data.subtitulo3 = 'servicioalcliente.cr@tomza.com';
    }
    if (!data.factura) {
      data.factura =
        'Factura:'+this.TOM;
    }
    if (!data.ruta) {
      data.ruta =
        'Ruta: ' + this.ruta;
    }
    if (!data.fecha) {
      data.fecha =
        'Fecha: ' + this.fecha;
    }
    if (!data.pago) {
      data.pago=
        'Pago: ' + this.credito1;
    }
    if (!data.Total) {
      data.Total =
        'VALOR(CRC):'+this.total;
    }
    if (!data.firma1) {
      data.firma1 =
        'Firma Recibido';
    }
    if (!data.firma2) {
      data.firma2 =
        'Nombre';
    }
    if (!data.sello) {
      data.sello =
        'Sellos';
    }
    if (!data.footer) {
      data.footer =
        this.Footer;
    }
    if (!data.tituloTramiteCopia) {
      data.tituloTramiteCopia =
        this.tituloTramiteCopia;
    }
    if (!data.tramite1) {
      data.tramite1 = this.Tramite1 + this.TOM + this.Tramite2;
    }
    if (!data.tramite2) {
      data.tramite2 =this.Tramite2;
    }
    if (!data.Total) {
      data.Total =
        'VALOR(CRC):'+this.total;
    }
    if (!data.tramite) {
      data.tramite =
      'Tramite N: TRA'+this.TOM;
    }
    if (!data.Nombre) {
      data.Nombre =
        'Nombre:'+ this.nombre;
    }
    if (!data.ced) {
      data.ced =
        '# Cedula:';
    }




    let receipt = '';
    receipt += commands.HARDWARE.HW_INIT;
    receipt += commands.TEXT_FORMAT.TXT_ALIGN_CT;
    receipt += data.tituloTramiteCopia;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_2WIDTH;
    receipt += commands.TEXT_FORMAT.TXT_ALIGN_CT;
    receipt += data.title.toUpperCase();
    receipt += commands.EOL;//salto de linea
    receipt += commands.TEXT_FORMAT.TXT_NORMAL;
    receipt += commands.HORIZONTAL_LINE.HR_58MM;
    receipt += commands.EOL;
    receipt += commands.HORIZONTAL_LINE.HR2_58MM;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
    receipt += commands.EOL;
    receipt += data.subtitulo;
    receipt += commands.EOL;
    receipt += data.subtitulo1;
    receipt += commands.EOL;
    receipt += data.subtitulo2;
    receipt += commands.EOL;
    receipt += data.subtitulo3;
    receipt += commands.EOL;
    receipt += commands.HORIZONTAL_LINE.HR2_58MM;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += data.tramite;
    receipt += commands.EOL;
    receipt += data.factura;
    receipt += commands.EOL;
    receipt += data.Nombre;
    receipt += commands.EOL;
    receipt += data.ruta;
    receipt += commands.EOL;
    receipt += data.fecha;
    receipt += commands.EOL;
    receipt += data.Total;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.HORIZONTAL_LINE.HR2_58MM;
    receipt += commands.TEXT_FORMAT.TXT_NORMAL;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += data.tramite1;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_NORMAL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_NORMAL;
    receipt += data.firma1;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_NORMAL;
    receipt += data.ced;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += data.firma2;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_NORMAL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += data.sello;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    //secure space on footer
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    //this.receipt = receipt;
    this.mountAlertBt(receipt);
  }

}
