import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLiteObject, SQLite } from '@ionic-native/sqlite';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Platform } from 'ionic-angular';
import { InformacionApiProvider } from '../informacion-api/informacion-api';

/*
  Generated class for the SqliteDbProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SqliteDbProvider {

  private database: SQLiteObject;
  private dbReady = new BehaviorSubject<boolean>(false);

  constructor(public http: HttpClient, public platform: Platform, public storage: SQLite, public infoAPI: InformacionApiProvider) {

    //montamos la base de datos cuando inicia la plataforma
    this.platform.ready().then(() => {
      this.storage.create({
        name: 'granel.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.database = db;

          this.createTables().then(() => {

            //comunica si esta listo
            this.dbReady.next(true);
          }).catch(error => {
            console.log(error);
          });
        })
    });

  }

  private createTables() {

    return this.database.executeSql(
      `CREATE TABLE IF NOT EXISTS clientes(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        codigo TEXT,GLN TEXT,EAN TEXT, nombre TEXT, razon TEXT,
        descuento TEXT, precio TEXT, tipo TEXT,
        ruta TEXT, subcanal TEXT, formadepago TEXT,
        credito TEXT, LAT TEXT, LONGI TEXT,
        puedeFacturar TEXT, lprecioa TEXT,
        lprecioc TEXT, ldescuento TEXT, especial TEXT,L INTEGER,K INTEGER, M INTEGER, J INTEGER, V INTEGER, S INTEGER, Do INTEGER,
        correo TEXT, tipoCilza TEXT, hizoMotivo INTEGER, hizoFactura INTEGER, nombredoc TEXT, g TEXT, tipoVisita TEXT, telefonoFijo TEXT, telefonoOpcional TEXT, tipodoc INTEGER,documento TEXT,Activo BOOLEAN,Comentario TEXT
      );`
      , [])
      .then(() => {
        return this.database.executeSql(
          `CREATE TABLE IF NOT EXISTS productos(
        id INTEGER,
        producto TEXT,
        tipoPrecio TEXT,
        precio FLOAT,
        tipoCliente INTEGER,
        descripcion TEXT,
        precioViejo FLOAT
      );`, []
        )
      })
      .then(() => {
        return this.database.executeSql(
          `CREATE TABLE IF NOT EXISTS rutas (
          cod INTEGER PRIMARY KEY AUTOINCREMENT,
          id INTEGER,
          nombre TEXT,
          unidad_negocio TEXT,
          cedi TEXT,
          descripcion TEXT,
          cerosTOM TEXT,
          consecutivo INTEGER
        )`, [])
      })
      .then(() => {
        return this.database.executeSql(
          `CREATE TABLE IF NOT EXISTS facturas (
          id INTEGER PRIMARY KEY AUTOINCREMENT, codigo TEXT, facnumero TEXT,
          razon_social TEXT, nombre TEXT, descuento TEXT,
          descuentot TEXT, credito TEXT, total TEXT,
          ruta TEXT, fecha TEXT, q25 INTEGER, q25r INTEGER,q100 INTEGER,q20 INTEGER,q20r INTEGER,
          q10 INTEGER,q10r INTEGER, q35 INTEGER, q35r INTEGER, q45 INTEGER,q45r INTEGER, q60 INTEGER, q60r INTEGER,
          qtls INTEGER, qkgs INTEGER, LAT TEXT, LONGI TEXT,g TEXT, efectivo text, transmonto TEXT,transnumero TEXT,
          chknumero TEXT, chkbanco TEXT,chkmonto TEXT , tipo TEXT, cz25 TEXT, cz25r TEXT, cz100r TEXT, fechadt TEXT, enviada INTEGER
        )`, []
        )
      })
      .then(() => {
        return this.database.executeSql(
          `CREATE TABLE IF NOT EXISTS facqueuet (
          id INTEGER PRIMARY KEY AUTOINCREMENT, ruta TEXT, facnumero TEXT,idcliente TEXT,detalle TEXT, totalexenta TEXT, total TEXT,
          rawdetalle TEXT, detail TEXT, corre TEXT, estado TEXT, fechacreacion TEXT, enviada INTEGER
        )`, []
        )
      })
      .then(() => {
        return this.database.executeSql(
          `CREATE TABLE IF NOT EXISTS billetes(
          id INTEGER PRIMARY KEY AUTOINCREMENT, cincuentaMil INTEGER, veinteMil INTEGER, diezMil INTEGER,
          cincoMil INTEGER, dosMil INTEGER, mil INTEGER, quinientos INTEGER, cien INTEGER, cincuenta INTEGER,
          veinticinco INTEGER,diez INTEGER,cinco INTEGER, dolares INTEGER, vale INTEGER, ruta INTEGER, fecha TEXT
        )`, []
        )
      }).then(() => {
        return this.database.executeSql(
          `CREATE TABLE IF NOT EXISTS transacciones(
          id INTEGER PRIMARY KEY AUTOINCREMENT, tipo TEXT, documento TEXT, factura TEXT , monto TEXT, fecha TEXT,
          ruta TEXT, banco TEXT
        )`, []
        )
      }).then(() => {
        return this.database.executeSql(
          `CREATE TABLE IF NOT EXISTS nocompra(
          id INTEGER PRIMARY KEY AUTOINCREMENT, codigoCliente TEXT, fecha TEXT, ruta TEXT
          , motivo TEXT, enviado INTEGER, fechaRuta TEXT)`, []
        )
      }).then(() => {
        return this.database.executeSql(
          `CREATE TABLE IF NOT EXISTS gastos(
          id INTEGER PRIMARY KEY AUTOINCREMENT, gasto TEXT, fecha TEXT, ruta TEXT
          ,descripcion TEXT,monto TEXT)`, []
        )
      }).then(() => {
        return this.database.executeSql(
          `CREATE TABLE IF NOT EXISTS lecturasAnteriores(
          cod INTEGER PRIMARY KEY AUTOINCREMENT,
          id INTEGER,codigo TEXT, nombre TEXT, razon TEXT,
          fechaActualizacion TEXT, lectura TEXT,
          ruta TEXT
        );`, []
        )
      }).then(() => {
        return this.database.executeSql(
          `CREATE TABLE IF NOT EXISTS respaldoLecturas(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          facnumero TEXT,codigo TEXT, lecturaAnterior TEXT, lecturaActual TEXT,
          fecha TEXT, ruta TEXT,
          litrosVendidos TEXT
        );`, []
        )
      }).then(() => {
        return this.database.executeSql(
          `CREATE TABLE IF NOT EXISTS clientesnuevos(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          codigo TEXT,GLN TEXT, nombre TEXT, razon TEXT,
          descuento TEXT, precio TEXT, tipo TEXT,
          ruta TEXT, subcanal TEXT, formadepago TEXT,
          credito TEXT, LAT TEXT, LONGI TEXT,
          puedeFacturar TEXT, lprecioa TEXT,
          lprecioc TEXT, ldescuento TEXT, especial TEXT,tipodoc INTEGER,documento TEXT,
          correo TEXT,fechacreacion TEXT, tipoCilza TEXT,enviado INTEGER, L , K TEXT , M TEXT,J TEXT, V TEXT, S TEXT, tipoVisita TEXT, telefonoFijo TEXT, telefonoOpcional TEXT
        );`, []
        )
      }).then(() => {
        return this.database.executeSql(
          `CREATE TABLE IF NOT EXISTS clave(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          clave TEXT,consecutivo TEXT,facnumero TEXT, fecha TEXT
        );`, []
        )
      })
      .then(() => {
        return this.database.executeSql(
          `CREATE TABLE IF NOT EXISTS mantenimiento(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          fechavisita TEXT,trabajorealizado TEXT,observaciones TEXT, foto1 TEXT, foto2 TEXT, serieTanque TEXT,
          fechavalvula TEXT, porcentajeGasInsta TEXT, porcentajeGasDesin TEXT, terminado TEXT, horaInicio TEXT, horaFin TEXT
        );`, []
        )
      })
      .then(() => {
        return this.database.executeSql(
          `CREATE TABLE IF NOT EXISTS tanque(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          numserie TEXT,capacidadlitros TEXT,capacidadgalones TEXT, fechafabricacion TEXT, fabricante TEXT, codcliente TEXT,
          numcontrato TEXT, ruta TEXT, propietario TEXT, nombreNegocio TEXT, contratoActivo TEXT, estado TEXT, latitud FLOAT, longitud FLOAT,
          provincia INT, canton INT, distrito INT, marcaTanque TEXT, posicionTanque TEXT , fechaClave TEXT, enviado INTEGER
        );`, []
        )
      })
      .then(() => {
        return this.database.executeSql(
          `CREATE TABLE IF NOT EXISTS canton(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          idProvincia INTEGER,
          codigo INTEGER,
          canton TEXT
        );`, []
        )
      }).then(() => {
        return this.database.executeSql(
          `CREATE TABLE IF NOT EXISTS remesa(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          ruta TEXT,realizadoPor TEXT,chofer INTEGER, placaCamion TEXT,
          q10 INTEGER,q20 INTEGER, q25 INTEGER, q25r INTEGER, q35 INTEGER,
          q45 INTEGER, q60 INTEGER, q100 INTEGER, q30 INTEGER, q40 INTEGER, lts INTEGER
        );`, []
        )
      })
      .then(() => {
        return this.database.executeSql(
          `CREATE TABLE IF NOT EXISTS distrito(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          idCanton INTEGER,
          codigo INTEGER,
          distrito TEXT
        );`, []
        )
      })

      .then(() => {
        return this.database.executeSql(
          `CREATE TABLE IF NOT EXISTS detalletanque(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          ruta TEXT, numfactura TEXT,detalle TEXT,
          codcliente TEXT,
          fecha TEXT
        );`, []
        )
      })
      .then(() => {
        return this.database.executeSql(
          `CREATE TABLE IF NOT EXISTS descargas(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          idBodega INTEGER,
          idGranelera INTEGER,
          totalLitros INTEGER,
          fecha TEXT
        )`, []
        )
      })
      .catch((err) => console.log("Error detectado al crear las tablas", err));

  }

  private isReady() {
    return new Promise((resolve, reject) => {
      //si la dbReady es true, resuelve
      if (this.dbReady.getValue()) {
        resolve();
      } else {
        this.dbReady.subscribe((ready) => {
          if (ready) {
            resolve();
          }
        });
      }
    })
  }
  //metodo que guarda las descargas obtenidas del api en la base de datos SQLite
  guardarRecargaSQLite(descarga: any) {
    return this.isReady()
      .then(() => {
        let sql = `INSERT INT descargas(id,idBodega,idGranelera,totalLitros,fecha) VALUES(?,?,?,?,?)`;
        return this.database.executeSql(sql, [descarga.id, descarga.idBodega, descarga.totalLitros, descarga.fecha]);
      })
  }
  //metodo que guarda las remesas obtenidas del api en la base de datps SQLite
  guardarRemesaSQLite(remesa: any) {
    return this.isReady()
      .then(() => {
        let sql = `INSERT INTO remesa(id,ruta,realizadoPor,chofer,placaCamion,q10,q20,q25,q25r,q30,q35,q40,q5,q60,q100,lts) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`;
        return this.database.executeSql(sql, [remesa.id, remesa.ruta, remesa.realizadoPor, remesa.chofer, remesa.placaCamion, remesa.q10, remesa.q20, remesa.q25, remesa.q25r,
        remesa.q30, remesa.q35, remesa.q40, remesa.q45, remesa.q60, remesa.q100, remesa.lts]);
      })
  }
  //metodo que guarda las rutas obtenidas del api en la base de datos SQLite
  guardarRutasSQLite(ruta: any) {
    return this.isReady()
      .then(() => {
        let sql = `INSERT INTO rutas(id,nombre,unidad_negocio,cedi,descripcion,cerosTOM,consecutivo) VALUES(?,?,?,?,?,?,?)`;
        return this.database.executeSql(sql, [ruta.id, ruta.nombre, ruta.unidad_negocio, ruta.cedi, ruta.descripcion, ruta.cerosTOM, ruta.consecutivo]);
      })
  }

  //metodo que guarda los detallesTanque ligados a las cantidades de litros o kilos
  guardarDetalleTanque(detalle: any) {
    return this.isReady()
      .then(() => {
        let sql = `INSERT INTO detalletanque(ruta,numfactura,detalle, codcliente,fecha) VALUES(?,?,?,?,?)`;
        return this.database.executeSql(sql, [detalle.ruta, detalle.numfactura, detalle.detalle, detalle.codcliente, detalle.fecha]);
      })
  }

  //metodo que guarda los cantones obtenidas del api en la base de datos SQLite
  guardarCantonesSQLite(canton: any) {
    return this.isReady()
      .then(() => {
        let sql = `INSERT INTO canton(id,idProvincia,codigo,canton) VALUES(?,?,?,?)`;
        return this.database.executeSql(sql, [canton.id, canton.idProvincia, canton.codigo, canton.canton]);
      })
  }

  //metodo que guarda las rutas obtenidas del api en la base de datos SQLite
  guardarDistritosSQLite(distrito: any) {
    return this.isReady()
      .then(() => {
        let sql = `INSERT INTO distrito(id,idCanton,codigo,distrito) VALUES(?,?,?,?)`;
        return this.database.executeSql(sql, [distrito.id, distrito.idCanton, distrito.codigo, distrito.distrito]);
      })
  }

  //metodo que obtiene las rutas de la base de datos SQLite
  getRutasSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM rutas", [])
          .then((data) => {
            let rutas = [];
            for (var i = 0; i < data.rows.length; i++) {
              rutas.push(data.rows.item(i));
            }
            console.log("Rutas SQlite", rutas);
            return rutas;
          });
      })
  }

  //metodo que obtiene los cantones de la base de datos SQLite
  getCantonesSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM canton", [])
          .then((data) => {
            let cantones = [];
            for (var i = 0; i < data.rows.length; i++) {
              cantones.push(data.rows.item(i));
            }
            console.log("Cantones SQlite", cantones);
            return cantones;
          });
      })
  }

  //metodo que obtiene los distritos de la base de datos SQLite
  getDistritosSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM distrito", [])
          .then((data) => {
            let distritos = [];
            for (var i = 0; i < data.rows.length; i++) {
              distritos.push(data.rows.item(i));
            }
            console.log("Distritos SQlite", distritos);
            return distritos;
          });
      })
  }
  //metodo que obtiene las rutas de la cedi
  getRutasSQLiteCedi(cedi: any) {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`SELECT * FROM rutas WHERE (cedi = "${cedi}" AND unidad_negocio = "Granel") OR (cedi = "${cedi}" AND unidad_negocio = "Carburacion") OR (cedi = "${cedi}" AND unidad_negocio = "Centros Comerciales")`, [])
          .then((data) => {
            let rutas = [];
            for (var i = 0; i < data.rows.length; i++) {
              rutas.push(data.rows.item(i));
            }

            return rutas;
          });
      })
  }

  //metodo que obtiene los cantones por provincia
  getCantonesSQLiteProvincia(provincia: any) {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`SELECT * FROM canton where idProvincia = ${provincia}`, [])
          .then((data) => {
            let cantones = [];
            for (var i = 0; i < data.rows.length; i++) {
              cantones.push(data.rows.item(i));
            }

            return cantones;
          });
      })
  }

  //metodo que obtiene los distritos por canton
  getDistritosSQLiteCaton(distrito: any) {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`SELECT * FROM distrito where idCanton = ${distrito}`, [])
          .then((data) => {
            let distritos = [];
            for (var i = 0; i < data.rows.length; i++) {
              distritos.push(data.rows.item(i));
            }

            return distritos;
          });
      })
  }

  //metodo que guarda los clientes obtenidos del api por ruta en la base de datos SQLite
  guardarClientesSQLite(cliente: any) {

    console.log("esta entrando");
    return this.isReady()
      .then(() => {
        console.log(" no sesta entrando");
        let sql = `INSERT INTO clientes(codigo,nombre,GLN,EAN,razon,descuento,precio,tipo,ruta,subcanal,formadepago,credito,
        LAT,LONGI,puedeFacturar,lprecioa,lprecioc,ldescuento,especial,L,K,M,J,V,S,Do,correo,tipoCilza,hizoMotivo,hizoFactura,Activo,Comentario) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`;
        return this.database.executeSql(sql, [cliente.codigo, cliente.nombre, cliente.GLN, cliente.EAN, cliente.razon, cliente.descuento, cliente.precio, cliente.tipo, cliente.ruta, cliente.subcanal, cliente.formadepago
          , cliente.credito, cliente.LAT, cliente.LONGI, cliente.puedeFacturar, cliente.lprecioa, cliente.lprecioc, cliente.ldescuento, cliente.especial, cliente.L, cliente.K, cliente.M, cliente.J, cliente.V, cliente.S, cliente.Do, cliente.correo, cliente.tipoCilza, cliente.hizoMotivo, cliente.hizoFactura, cliente.Activo, cliente.Comentario]);
      })
  }

  //metodo que guarda los productos obtenidos del api en la base de datos SQLite
  guardarProductosSQLite(producto: any) {
    return this.isReady()
      .then(() => {
        let sql = `INSERT INTO productos(id,producto,tipoPrecio,precio,tipoCliente,descripcion,precioViejo) VALUES(?,?,?,?,?,?,?)`;
        return this.database.executeSql(sql, [producto.id, producto.producto, producto.tipoPrecio, producto.precio, producto.tipoCliente, producto.descripcion, producto.precioViejo]);
      })
  }

  sumarTotalFacturas() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`SELECT SUM(total) as TOTAL FROM facturas`, [])
      })
  }

  //metodo que guarda un nuevo cliente en SQLite con tipo 500 para despues enviarlo a la base de datos en la nube
  //y puedan rellenar sus datos bien tipodoc INTEGER,documento TEXT
  guardarClienteSQLite(cliente: any) {
    return this.isReady()
      .then(() => {
        let sql = `INSERT INTO clientesnuevos(codigo,nombre,GLN,razon,descuento,precio,tipo,ruta,subcanal,formadepago,credito,
        LAT,LONGI,puedeFacturar,lprecioa,lprecioc,ldescuento,especial,tipodoc,documento,correo,fechacreacion,tipoCilza,enviado,L,K,M,J,V,S,tipoVisita,telefonoFijo,telefonoOpcional) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`;
        return this.database.executeSql(sql, [cliente.codigo, cliente.nombre, cliente.GLN, cliente.razon, cliente.descuento, cliente.precio, cliente.tipo, cliente.ruta, cliente.subcanal, cliente.formadepago
          , cliente.credito, cliente.LAT, cliente.LONGI, cliente.puedeFacturar, cliente.lprecioa, cliente.lprecioc, cliente.ldescuento, cliente.especial, cliente.tipodoc, cliente.documento, cliente.correo, cliente.fechacreacion, cliente.tipoCilza, cliente.enviado, cliente.L, cliente.K, cliente.M, cliente.J, cliente.V, cliente.S, cliente.tipoVisita, cliente.telefonoFijo, cliente.telefonoOpcional]);
      })
  }

  //metodo que guarda facturas
  guardarFacturas(factura: any) {
    return this.isReady()
      .then(() => {
        let sql = `INSERT INTO facturas(codigo,facnumero,razon_social,nombre,descuento, descuentot,credito,total,ruta,fecha,q25,q25r,q100,q20,q20r,q10,q10r,q35,q35r,q45,q45r,q60,q60r,qtls, qkgs, LAT, LONGI, g , efectivo,transmonto,transnumero,chknumero,chkbanco,chkmonto,tipo,cz25,cz25r,cz100r,fechadt,enviada) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`;

        return this.database.executeSql(sql, [factura.codigo, factura.facnumero, factura.razon_social, factura.nombre, factura.descuento,
        factura.descuentot, factura.credito, factura.total, factura.ruta, factura.fecha, factura.q25, factura.q25r, factura.q100, factura.q20,
        factura.q20r, factura.q10, factura.q10r, factura.q35, factura.q35r, factura.q45, factura.q45r, factura.q60, factura.q60r, factura.qtls, factura.qkgs,
        factura.LAT, factura.LONGI, factura.g, factura.efectivo, factura.transnumero, factura.transmonto, factura.chknumero, factura.chkbanco, factura.chkmonto, factura.tipo, factura.cz25,
        factura.cz25r, factura.cz100r, factura.fechadt, factura.enviada
        ]);
      }).catch(error => { console.error(error) })
  }

  //metodo para guardar el detalle de la factura
  guardarDetalle(detalle: any) {
    return this.isReady()
      .then(() => {
        let sql = `INSERT INTO facqueuet(ruta,facnumero,idcliente,detalle,totalexenta,total,
        rawdetalle,detail,corre,estado,fechacreacion,enviada) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)`;
        return this.database.executeSql(sql, [detalle.ruta, detalle.facnumero, detalle.idcliente, detalle.detalle,
        detalle.totalexenta, detalle.total, detalle.rawdetalle, detalle.detail, detalle.corre, detalle.estado, detalle.fechacreacion, detalle.enviada]);
      }).catch(error => { console.error(error) })

  }

  //metodo que guarda los motivos de no compra en SQLite
  guardarNoCompra(motivo: any) {
    return this.isReady()
      .then(() => {
        let sql = `INSERT INTO nocompra(codigoCliente,fecha,ruta,motivo,enviado,fechaRuta) VALUES(?,?,?,?,?,?)`;
        return this.database.executeSql(sql, [motivo.codigoCliente, motivo.fecha, motivo.ruta, motivo.motivo, motivo.enviado, motivo.fechaRuta]);
      })
  }

  //metodo que actualiza el campo enviado de 0 a 1 para que no se vuelva a enviar la factura
  actualizarEnviadoMotivo(motivo: any) {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`UPDATE nocompra set enviado = ? WHERE codigoCliente=?`, [motivo.enviado, motivo.codigoCliente]);
      })
  }

  actualizarEnviadoTanque(tanque: any) {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`UPDATE tanque set enviado = ? WHERE numserie=?`, [tanque.enviado, tanque.numserie])
      })
  }

  actualizarMantenimiento(mantenimiento: any) {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`UPDATE mantenimiento set terminado = ?, fechavalvula = ?,foto1 = ?, foto2 = ?,horaFin = ?
      ,horaInicio = ?,observaciones = ?, porcentajeGasDesin =?, porcentajeGasInsta = ?, serieTanque = ?, trabajorealizado = ?  WHERE id=?`,
          [mantenimiento.terminado, mantenimiento.fechavalvula, mantenimiento.foto1, mantenimiento.foto2, mantenimiento.horaFin, mantenimiento.horaInicio, mantenimiento.observaciones, mantenimiento.porcentajeGasDesin
            , mantenimiento.porcentajeGasInsta, mantenimiento.serieTanque, mantenimiento.trabajorealizado, mantenimiento.id]);
      })
  }

  actualizarHizoMotivo(cliente: any) {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`UPDATE clientes set hizoMotivo = ? WHERE codigo=?`, [cliente.hizoMotivo, cliente.codigo]);
      })
  }

  actualizarHizoFactura(cliente: any) {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`UPDATE clientes set hizoFactura= ? WHERE codigo=?`, [cliente.hizoFactura, cliente.codigo]);
      })
  }

  //metodo que obtiene los motivos de la base de datos SQLite
  getMotivosSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM nocompra", [])
          .then((data) => {
            let motivos = [];
            for (var i = 0; i < data.rows.length; i++) {
              motivos.push(data.rows.item(i));
            }
            return motivos;
          });
      })
  }

  getMotivosSQLiteNoEnviados() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM nocompra WHERE enviado =  0", [])
          .then((data) => {
            let motivos = [];
            for (var i = 0; i < data.rows.length; i++) {
              motivos.push(data.rows.item(i));
            }
            return motivos;
          });
      })
  }

  //Obtener billetes de sqlite
  getBilletesSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM billetes", [])
          .then((data) => {
            let billetes = [];
            for (var i = 0; i < data.rows.length; i++) {
              billetes.push(data.rows.item(i));
            }

            return billetes;
          });
      })
  }

  //metodo que obtiene las factuas SQLite
  getFacturaCredito() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM facturas where credito=0", [])
          .then((data) => {
            let facturas = [];
            for (var i = 0; i < data.rows.length; i++) {
              facturas.push(data.rows.item(i));
            }

            return facturas;
          });
      })
  }

  getFacturaContado() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM facturas where credito=1", [])
          .then((data) => {
            let facturas = [];
            for (var i = 0; i < data.rows.length; i++) {
              facturas.push(data.rows.item(i));
            }

            return facturas;
          });
      })
  }

  guardarTransaccionesSQLite(transaccion: any) {
    return this.isReady()
      .then(() => {
        let sql = `INSERT INTO transacciones(id,tipo,documento,factura,monto,fecha,ruta,banco) VALUES(?,?,?,?,?,?,?,?)`;
        return this.database.executeSql(sql, [transaccion.id, transaccion.tipo, transaccion.documento, transaccion.factura, transaccion.monto, transaccion.fecha, transaccion.ruta, transaccion.banco]);
      })
  }

  getTransacciones() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM transacciones where tipo in('Deposito','Cheque','Transferencia')", [])
          .then((data) => {
            let transacciones = [];
            for (var i = 0; i < data.rows.length; i++) {
              transacciones.push(data.rows.item(i));
            }

            return transacciones;
          });
      })
  }

  getClientesNuevosNoEnviados() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM clientesnuevos where enviado = 0", [])
          .then((data) => {
            let transacciones = [];
            for (var i = 0; i < data.rows.length; i++) {
              transacciones.push(data.rows.item(i));
            }

            return transacciones;
          });
      })
  }

  getTransaccionesParaEnviar() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM transacciones", [])
          .then((data) => {
            let transacciones = [];
            for (var i = 0; i < data.rows.length; i++) {
              transacciones.push(data.rows.item(i));
            }

            return transacciones;
          });
      })
  }

  guardarGastosSQLite(producto: any) {
    return this.isReady()
      .then(() => {
        let sql = `INSERT INTO gastos(id,gasto,fecha,ruta,monto,descripcion) VALUES(?,?,?,?,?,?)`;
        return this.database.executeSql(sql, [producto.id, producto.gasto, producto.fecha, producto.ruta, producto.monto, producto.descripcion]);
      })
  }

  getGastos() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM transacciones where tipo not in ('Deposito','Cheque','Transferencia')", [])
          .then((data) => {
            let gastos = [];
            for (var i = 0; i < data.rows.length; i++) {
              gastos.push(data.rows.item(i));
            }

            return gastos;
          });
      })
  }

  //metodo que obtiene la ruta de un cliente precargado para cuando los botones estan en rojo no se equivoquen de ruta
  obtenerRutaEspecificaCargada() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT ruta  FROM clientes LIMIT 1", [])
          .then((data) => {
            let ruta = [];
            for (var i = 0; i < data.rows.length; i++) {
              ruta.push(data.rows.item(i));
            }

            return ruta;
          });
      })
  }

  //metodo que obtiene la ruta que cargo la primera vez cuando los botones estan en rojo
  obtenerLaRutaCargada(id: any) {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`SELECT * FROM rutas where id = ${id}`, [])
          .then((data) => {
            let ruta = [];
            for (var i = 0; i < data.rows.length; i++) {
              ruta.push(data.rows.item(i));
            }

            return ruta;
          });
      })
  }

  getFacturasSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM facturas", []) //WHERE ruta != '0'
          .then((data) => {
            let facturas = [];
            for (var i = 0; i < data.rows.length; i++) {
              facturas.push(data.rows.item(i));
            }

            return facturas;
          });
      })
  }

  //metodo para obtener una factura por TOM
  getFacturaPorTOM(TOM: any) {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`SELECT * FROM facturas where facnumero = ${TOM}`, [])
          .then((data) => {
            if (data.rows.length) {
              return data.rows.item(0);

            }

            return null;
          })
      })
  }

  //metodo para obtener un detalle por TOM
  getDetallePorTOM(TOM: any) {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`SELECT * FROM facqueuet where facnumero = ${TOM}`, [])
          .then((data) => {
            if (data.rows.length) {
              return data.rows.item(0);

            }

            return null;
          })
      })
  }

  getClaveConsecutivoPorTOM(TOM: any) {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`SELECT * FROM clave where facnumero = ${TOM}`, [])
          .then((data) => {
            if (data.rows.length) {
              return data.rows.item(0);

            }

            return null;
          })
      })
  }

  getDetalleTanquePorTOM(TOM: any) {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`SELECT * FROM detalleTanque where numfactura = ${TOM}`, [])
          .then((data) => {
            if (data.rows.length) {
              return data.rows.item(0);

            }

            return null;
          })
      })
  }

  //metodo que actualiza el campo enviado de 0 a 1 para que no se vuelva a enviar la factura
  actualizarEnviadoDetalle(detalle: any) {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`UPDATE facqueuet set enviada = ? WHERE facnumero=?`, [detalle.enviada, detalle.facnumero]);
      })
  }

  //metodo que elimina todos los clientes que trajo la api cuando se cierra el app para que no se dupliquen
  //al volver iniciar

  eliminarDescargasSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`DELETE FROM Descargas`, [])
      })
  }
  eliminarClientesSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`DELETE FROM clientes`, [])
      })
  }

  eliminaMotivosNoCompraSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`DELETE FROM nocompra`, [])
      })
  }

  eliminarClaves() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`DELETE FROM clave`, [])
      })
  }

  eliminaMotivosNoCompraSQLiteCodigo(motivo: any) {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`DELETE FROM nocompra WHERE codigoCliente = ?`, [motivo.codigoCliente])
      })
  }

  //metodo que elimina todos los productos que trajo la api cuando se cierra el app para que no se dupliquen
  //al volver iniciar
  eliminarProductosSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`DELETE FROM productos`, [])
      })
  }

  //metodo que elimina todos los clientes que trajo la api cuando se cierra el app para que no se dupliquen
  //al volver iniciar
  eliminarRutasSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`DELETE FROM rutas`, [])
      })
  }

  //metodo que elimina todos los clientes que trajo la api cuando se cierra el app para que no se dupliquen
  //al volver iniciar
  eliminarFacturasSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`DELETE FROM facturas`, [])
      })
  }

  //metodo que elimina los detalles de facturas
  eliminarDetallesSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`DELETE FROM facqueuet`, [])
      })
  }


  eliminarTransaccionesSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`DELETE FROM transacciones`, [])
      })
  }

  eliminarBilletesSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`DELETE FROM billetes`, [])
      })
  }

  eliminarClientesNuevosSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`DELETE FROM clientesnuevos`, [])
      })
  }

  getDetallesNoEnviadosSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM facqueuet ", [])
          .then((data) => {
            let detalles = [];
            for (var i = 0; i < data.rows.length; i++) {
              detalles.push(data.rows.item(i));
            }

            return detalles;
          });
      })
  }

  actualizarEnviadoFactura(factura: any) {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`UPDATE facturas set enviada = ? WHERE facnumero=?`, [factura.enviada, factura.facnumero]);
      })
  }

  getFacturasNoEnviadasSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM facturas ", []) //WHERE ruta != '0'
          .then((data) => {
            let facturas = [];
            for (var i = 0; i < data.rows.length; i++) {
              facturas.push(data.rows.item(i));
            }

            return facturas;
          });
      })
  }

  //metodo que obtiene las claves y consecutivos de la base de datos SQLite
  getClavesDeSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM clave", [])
          .then((data) => {
            let claves = [];
            for (var i = 0; i < data.rows.length; i++) {
              claves.push(data.rows.item(i));
            }
            return claves;
          });
      })
  }

  getClavesSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM clave", []) //WHERE ruta != '0'
          .then((data) => {
            let claves = [];
            for (var i = 0; i < data.rows.length; i++) {
              claves.push(data.rows.item(i));
            }

            return claves;

          }
          )
      })
  }

  guardarMantenimiento(mantenimiento: any) {
    return this.isReady()
      .then(() => {
        let sql = "INSERT INTO mantenimiento(fechavisita,trabajorealizado,observaciones,foto1,foto2,serieTanque,fechavalvula,porcentajeGasInsta,porcentajeGasDesin,terminado,horaInicio,horaFin) values(?,?,?,?,?,?,?,?,?,?,?,?)";
        return this.database.executeSql(sql, [mantenimiento.fechavisita, mantenimiento.trabajorealizado, mantenimiento.observaciones, mantenimiento.foto1,
        mantenimiento.foto2, mantenimiento.serieTanque, mantenimiento.fechavalvula, mantenimiento.porcentajeGasInsta, mantenimiento.porcentajeGasDesin, mantenimiento.terminado, mantenimiento.horaInicio, mantenimiento.horaFin]);
      })

  }

  //metodo que obtiene la ruta sobre la que se esta trabajando para actualizar su consecutivo
  getRutaUsada(id: any) {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`SELECT * FROM rutas where id = ${id}`, [])
          .then((data) => {
            if (data.rows.length) {
              return data.rows.item(0);

            }

            return null;
          })
      })
  }

  guardarTanque(tanque: any) {
    return this.isReady()
      .then(() => {
        let sql = "INSERT INTO tanque(numserie, capacidadlitros, capacidadgalones,fechafabricacion,fabricante,codcliente,numcontrato,ruta,propietario,nombreNegocio,contratoActivo,estado,latitud,longitud,provincia,canton,distrito,marcaTanque,posicionTanque,fechaClave,enviado) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        return this.database.executeSql(sql, [tanque.numserie, tanque.capacidadlitros, tanque.capacidadgalones, tanque.fechafabricacion, tanque.fabricante, tanque.codcliente, tanque.numcontrato, tanque.ruta, tanque.propietario, tanque.nombreNegocio, tanque.contratoActivo, tanque.estado, tanque.latitud, tanque.longitud, tanque.provincia, tanque.canton, tanque.distrito, tanque.marcaTanque, tanque.posicionTanque, tanque.fechaClave, tanque.enviado]);
      })
  }

  //metodo que obtiene los clientes de la base de datos SQLite
  getClientesSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM clientes ORDER BY nombre", [])
          .then((data) => {
            let clientes = [];
            for (var i = 0; i < data.rows.length; i++) {
              clientes.push(data.rows.item(i));
            }
            return clientes;
          });
      })
  }

  obtenerTanques() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`SELECT * FROM tanque`, [])
          .then((data) => {
            let tanque = [];
            for (var i = 0; i < data.rows.length; i++) {
              tanque.push(data.rows.item(i));
            }

            return tanque;
          });
      })
  }

  obtenerMantenimientos() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`SELECT * FROM mantenimiento`, [])
          .then((data) => {
            let mantenimiento = [];
            for (var i = 0; i < data.rows.length; i++) {
              mantenimiento.push(data.rows.item(i));
            }

            return mantenimiento;
          });
      })
  }

  //metodo que obtiene el producto especifico por ID de la base de datos SQLite
  getProductoSQLite(id: any) {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`SELECT * FROM productos where id = ${id}`, [])
          .then((data) => {
            if (data.rows.length) {
              return data.rows.item(0);
            }

            return null;
          })
      })
  }

  getAllProductsSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM productos", [])
          .then((data) => {
            let productos = [];
            for (var i = 0; i < data.rows.length; i++) {
              productos.push(data.rows.item(i));
            }
            return productos;
          });
      })
  }

  guardarBillete(billete: any) {
    return this.isReady()
      .then(() => {
        let sql = `INSERT INTO billetes(cincuentaMil,veinteMil,diezMil,cincoMil,dosMil,mil,quinientos,cien,cincuenta,veinticinco,diez,cinco,dolares,vale,ruta,fecha) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`;
        return this.database.executeSql(sql, [billete.cincuentaMil, billete.veinteMil, billete.diezMil, billete.cincoMil,
        billete.dosMil, billete.mil, billete.quinientos, billete.cien, billete.cincuenta, billete.veinticinco, billete.diez, billete.cinco, billete.dolares, billete.vale, billete.ruta, billete.fecha]);
      }).catch(error => { console.error(error) })

  }


  getProductosSQLite(tipo: number) {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`SELECT * FROM productos where tipoCliente =${tipo}`, [])
          .then((data) => {
            let productos = [];
            for (var i = 0; i < data.rows.length; i++) {
              productos.push(data.rows.item(i));
            }

            return productos;
          });
      })
  }

  guardarClaves(clave: any) {
    return this.isReady()
      .then(() => {
        let sql = `INSERT INTO clave(clave,consecutivo,facnumero,fecha) VALUES(?,?,?,?)`;
        return this.database.executeSql(sql, [clave.clave, clave.consecutivo, clave.facnumero, clave.fecha]);
      })
  }

  //metodo que actualiza el consecutivo en 0+1, 1+1, 2+1 y asi sucesivamente por cada factura generada.
  //para que cada factura tenga su numero unico
  actualizarConsecutivoRuta(ruta: any) {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`UPDATE rutas set consecutivo = ? WHERE id=?`, [ruta.consecutivo, ruta.id]);
      }).catch(error => { console.error(error) });
  }


  //metodo que obtiene un cliente especifico por codigo de la base de datos SQLite
  getClienteEspecifico(codigo: any) {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`SELECT * FROM clientes where codigo = ${codigo}`, [])
          .then((data) => {
            if (data.rows.length) {
              return data.rows.item(0);
            }

            return null;
          })
      })
  }

  getDetallesSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM facqueuet", [])
          .then((data) => {
            let detalles = [];
            for (var i = 0; i < data.rows.length; i++) {
              detalles.push(data.rows.item(i));
            }

            return detalles;
          });
      })
  }

  //metodo que obtiene los clientes de la base de datos SQLite
  getCientesNuevosSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM clientesnuevos", [])
          .then((data) => {
            let clientes = [];
            for (var i = 0; i < data.rows.length; i++) {
              clientes.push(data.rows.item(i));
            }
            return clientes;
          });
      })
  }

  actualizarEnviadoCliente(cliente: any) {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`UPDATE clientesnuevos set enviado = ? WHERE id=?`, [cliente.enviado, cliente.id]);
      })
  }

  //metodo que obtiene los clientes de la base de datos SQLite
  getRolLunesCientesSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM clientes WHERE L = 1 ORDER BY nombre", [])
          .then((data) => {
            let clientes = [];
            for (var i = 0; i < data.rows.length; i++) {
              clientes.push(data.rows.item(i));
            }
            return clientes;
          });
      })
  }

  //metodo que obtiene los clientes de la base de datos SQLite
  getRolMartesCientesSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM clientes WHERE K = 1 ORDER BY nombre", [])
          .then((data) => {
            let clientes = [];
            for (var i = 0; i < data.rows.length; i++) {
              clientes.push(data.rows.item(i));
            }
            return clientes;
          });
      })
  }

  //metodo que obtiene los clientes de la base de datos SQLite
  getRolMiercolesCientesSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM clientes WHERE M = 1 ORDER BY nombre", [])
          .then((data) => {
            let clientes = [];
            for (var i = 0; i < data.rows.length; i++) {
              clientes.push(data.rows.item(i));
            }
            return clientes;
          });
      })
  }

  //metodo que obtiene los clientes de la base de datos SQLite
  getRolJuevesCientesSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM clientes WHERE J = 1 ORDER BY nombre", [])
          .then((data) => {
            let clientes = [];
            for (var i = 0; i < data.rows.length; i++) {
              clientes.push(data.rows.item(i));
            }
            return clientes;
          });
      })
  }

  //metodo que obtiene los clientes de la base de datos SQLite
  getRolViernerCientesSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM clientes WHERE V = 1 ORDER BY nombre", [])
          .then((data) => {
            let clientes = [];
            for (var i = 0; i < data.rows.length; i++) {
              clientes.push(data.rows.item(i));
            }
            return clientes;
          });
      })
  }

  //metodo que obtiene los clientes de la base de datos SQLite
  getRolSabadoCientesSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM clientes WHERE S = 1 ORDER BY nombre", [])
          .then((data) => {
            let clientes = [];
            for (var i = 0; i < data.rows.length; i++) {
              clientes.push(data.rows.item(i));
            }
            return clientes;
          });
      })
  }

  //metodo que obtiene los clientes de la base de datos SQLite
  getRolDomingoCientesSQLite() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM clientes WHERE Do = 1 ORDER BY nombre", [])
          .then((data) => {
            let clientes = [];
            for (var i = 0; i < data.rows.length; i++) {
              clientes.push(data.rows.item(i));
            }
            return clientes;
          });
      })
  }

  getMantenimientosTerminados() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM mantenimiento WHERE terminado = 'si'", [])
          .then((data) => {
            let mantenimientos = [];
            for (var i = 0; i < data.rows.length; i++) {
              mantenimientos.push(data.rows.item(i));
            }
            return mantenimientos;
          });
      })
  }

  getMantenimientosNoTerminados() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM mantenimiento WHERE terminado = 'no'", [])
          .then((data) => {
            let mantenimientos = [];
            for (var i = 0; i < data.rows.length; i++) {
              mantenimientos.push(data.rows.item(i));
            }
            return mantenimientos;
          });
      })
  }

  getDetallesTanques() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql("SELECT * FROM detalletanque", [])
          .then((data) => {
            let detalles = [];
            for (var i = 0; i < data.rows.length; i++) {
              detalles.push(data.rows.item(i));
            }
            return detalles;
          });
      })
  }

  eliminarMantenimientosTerminados() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`DELETE FROM mantenimiento WHERE terminado = "si"`)
      })
  }

  eliminarTanquesIngresados() {
    return this.isReady()
      .then(() => {
        return this.database.executeSql(`DELETE FROM tanque`)
      })
  }
}
