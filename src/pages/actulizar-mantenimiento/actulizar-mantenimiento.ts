import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { SqliteDbProvider } from '../../providers/sqlite-db/sqlite-db';
import { ImagePicker } from '@ionic-native/image-picker';
import { Camera } from '@ionic-native/camera';
import { MantenimientosPage } from '../mantenimientos/mantenimientos';
import { MenuMantenimientoPage } from '../menu-mantenimiento/menu-mantenimiento';

/**
 * Generated class for the ActulizarMantenimientoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-actulizar-mantenimiento',
  templateUrl: 'actulizar-mantenimiento.html',
})
export class ActulizarMantenimientoPage {


  idMantenimiento; 
  trabajos:any; serie; observaciones; fechavisita; fotouno; fotodos;
  image:any;
  fotos = [];
  mantenimientos = [];
  fechacompleta;
  fechaV; porcentajeInstalacion; porcentajeDesinstalacion;
  terminado = 'no';
  horaInicio;
  horaFin; 


  constructor(public navCtrl: NavController, public navParams: NavParams, public db: SqliteDbProvider,private camera: Camera,public picker:ImagePicker,public aCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ActulizarMantenimientoPage');
    this.serie =  this.navParams.get('serieTanque');
    this.idMantenimiento = this.navParams.get('id');
    this.fechacompleta = this.navParams.get('fechavisita');
    this.horaInicio = this.navParams.get('horaInicio');
    this.observaciones = this.navParams.get('observaciones');
    this.trabajos = JSON.parse(this.navParams.get('trabajorealizado'));
    this.terminado = this.navParams.get('terminado');
    this.fotouno = this.navParams.get('foto1');
    this.fotodos = this.navParams.get('foto2');
    this.fechaV = this.navParams.get('fechavalvula');
    this.porcentajeDesinstalacion = this.navParams.get('porcentajeGasDesin');
    this.porcentajeInstalacion = this.navParams.get('porcentajeGasInsta');

    if(this.fotouno === null && this.fotodos === null){
      this.fotos = [];
    }else{
      this.fotos.push(this.fotouno,this.fotodos);
    }
  
   
    console.log(this.fotos);
    console.log(this.serie);
    console.log(this.idMantenimiento);
    console.log(this.trabajos);
  }

  actualizarMantenimiento(){

    var fecha =  new Date();
      var day;
  
      var dia = fecha.getDate();
      var mes =  fecha.getMonth();
      var year =  fecha.getFullYear();
      
      var horas = fecha.getHours();
      var minut = fecha.getMinutes();
      var segun = fecha.getSeconds();
      var minutos;
      var segundos;
      var meses = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
      
      if(minut <= 9){
            minutos = '0'+minut.toString();
      }else{
            minutos = minut.toString();
      }
       
      if(segun <= 9){
          segundos = '0'+segun.toString();
      }else{
          segundos = segun.toString();
      }
  
      if(dia <= 9){
        day = '0'+dia.toString();
      }else{
        day = dia.toString();
      }
      
      this.fechacompleta = day+'-'+meses[mes]+'-'+year+' '+horas+':'+minutos+':'+segundos;
      this.horaFin = horas+':'+minutos+':'+segundos;

      console.log(this.fotos);

    this.db.actualizarMantenimiento({
      terminado: this.terminado,
      trabajorealizado : JSON.stringify(this.trabajos),
      fechavalvula: this.fechaV,
      foto1: this.fotos[0],
      foto2: this.fotos[1],
      horaFin: this.horaFin,
      horaInicio: this.horaInicio,
      observaciones: this.observaciones,
      porcentajeGasDesin: this.porcentajeDesinstalacion,
      porcentajeGasInsta: this.porcentajeInstalacion,
      serieTanque:this.serie,
      
      id: this.idMantenimiento
    }).then((data) => {
      console.log(data);
    }).catch( error => { console.error(error) });
  }

  abrirGaleria(){
    this.camera.getPicture({
      quality: 20,
      destinationType:this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit:false,
      encodingType:this.camera.EncodingType.JPEG,
      targetHeight:1024,
      targetWidth:1024,
      correctOrientation:true,
      saveToPhotoAlbum: true
    }).then(resultado => {
      this.image = "data:image/jpeg;base64," + resultado;
      console.log(this.image);
      this.fotos.push(this.image);
      console.log(this.fotos);
    }).catch(error => {
      console.log(error)
    })
  }
  
  borrarFoto(index){
    this.fotos.splice(index, 1);
    console.log(this.fotos);
  }  

  //alerta que aparece cuando tienen que verificar si lleno la informacion
  mensajeDeSeguridad() {
    let alert = this.aCtrl.create({
      title: 'Mantenimiento actualizado correctamente',
      buttons: [
       {
          text: 'Aceptar',
          handler: () => {
            console.log('Confirm Okay');
            this.navCtrl.push(MenuMantenimientoPage);
          }
        }
      ]
    });
    alert.present();
  }

  acciones(){
    this.actualizarMantenimiento();
    this.mensajeDeSeguridad();
  }

  mensajeDeSeguridad1() {
    let alert = this.aCtrl.create({
      title: '¿Está completamente seguro de que completó toda la información?',
      subTitle: 'Verifique si no esta seguro!!',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: () => {
            console.log('Confirm Okay');
            this.actualizarMantenimiento();
            
          }
        }
      ]
    });
    alert.present();
  }
}
