import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { SqliteDbProvider } from '../../providers/sqlite-db/sqlite-db';
import { MenuMantenimientoPage } from '../menu-mantenimiento/menu-mantenimiento';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';

/**
 * Generated class for the RegistroTanquePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-registro-tanque',
  templateUrl: 'registro-tanque.html',
})
export class RegistroTanquePage {

  codigoCliente;  fechacompleta;  fechavisita;  fechafabricacion;  vidautil;  numcontrato;  litros;  galones;  ruta;  estado;  activo;  numContrato;
  fabricante; propietario; numSerie; nombreNegocio; provincia; canton; distrito; marcaTanque; posicionTanque;
  codigo;
  latitud:number;
  longitud:number;
  miLat;
  miLong;

  cantones;
  distritos;
  pro = 9;
  can = 83;
  razonCliente;
  nombreCliente;

  constructor(public navCtrl: NavController, public navParams: NavParams,public db: SqliteDbProvider,public aCtrl: AlertController, public geo:Geolocation) {

    this.codigoCliente = this.navParams.get('codigo');
    this.ruta = this.navParams.get('ruta');
    this.nombreNegocio = this.navParams.get('nombre');
    this.propietario = this.navParams.get('razon');

    console.log('Constructor:',this.pro);
    this.getGeolocation();
    console.log("codigocliente",this.codigo);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistroTanquePage');
  }

  getGeolocation(){
    this.geo.getCurrentPosition().then((geoposition: Geoposition) =>{

      this.latitud = geoposition.coords.latitude;
      this.longitud = geoposition.coords.longitude;

      console.log("Esta es mi latitud:",this.latitud);
      console.log("Esta es mi longitud:",this.longitud);
    });
  }


  guardarNuevoTanque(){
    //recorro los productos que obtuve del api

    var fecha =  new Date();
    var day;

    var dia = fecha.getDate();
    var mes =  fecha.getMonth();
    var year =  fecha.getFullYear();

    var horas = fecha.getHours();
    var minut = fecha.getMinutes();
    var segun = fecha.getSeconds();
    var minutos;
    var segundos;
    var meses = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

    if(minut <= 9){
          minutos = '0'+minut.toString();
    }else{
          minutos = minut.toString();
    }

    if(segun <= 9){
        segundos = '0'+segun.toString();
    }else{
        segundos = segun.toString();
    }

    if(dia <= 9){
      day = '0'+dia.toString();
    }else{
      day = dia.toString();
    }

    this.fechacompleta = day+'-'+meses[mes]+'-'+year+' '+horas+':'+minutos+':'+segundos;

    this.miLat = this.latitud;
    this.miLong =  this.longitud;
    console.log(this.miLat);
    console.log(this.miLong);

    //armo cada cliente para despues almacenarlos
    let tanque = {
      numserie:this.numSerie,
      capacidadlitros: this.litros,
      capacidadgalones: (this.litros/3.785),
      fechafabricacion: this.fechafabricacion,
      fabricante: this.fabricante,
      codcliente: this.codigoCliente,
      numcontrato: this.numContrato,
      ruta: this.ruta,
      propietario: this.propietario,
      nombreNegocio: this.nombreNegocio,
      contratoActivo: this.activo,
      estado: this.estado,
      latitud: this.miLat,
      longitud: this.miLong,
      provincia: this.provincia,
      canton: this.canton,
      distrito: this.distrito,
      marcaTanque: this.marcaTanque,
      posicionTanque: this.posicionTanque,
      fechaClave: this.fechacompleta+"/"+this.ruta,
      enviado: 0
    }

    console.log(tanque);

    this.db.guardarTanque(tanque);
    this.mensajeDeSeguridad();
  }

  //alerta que aparece cuando tienen que verificar si lleno la informacion
  mensajeDeSeguridad() {
    let alert = this.aCtrl.create({
      title: 'Tanque registrado correctamente',
      buttons: [
       {
          text: 'Aceptar',
          handler: () => {
            console.log('Confirm Okay');
            this.navCtrl.push(MenuMantenimientoPage);
          }
        }
      ]
    });
    alert.present();
  }

  cambio(provincia){
    console.log(provincia);

    this.pro = provincia;
    this.provincia = provincia;

    console.log("Nueva", this.pro);
    this.db.getCantonesSQLiteProvincia(parseInt(provincia)).then((data) => {
      this.cantones= data;
      console.log(this.cantones);

    },(error) =>{
      console.log(error);
    })
  }

  cambioCanton(canton){

    this.can = canton;
    this.canton= canton;
    this.db.getDistritosSQLiteCaton(parseInt(canton)).then((data) => {
      this.distritos= data;
      console.log(this.distritos);

    },(error) =>{
      console.log(error);
    })
  }

  cambioDistrito(distrito){

    this.distrito = distrito;
  }

  mensajeDeSeguridad1() {
    let alert = this.aCtrl.create({
      title: '¿Está completamente seguro de que completó toda la información?',
      subTitle: 'Verifique si no esta seguro!!',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: () => {
            console.log('Confirm Okay');
            this.guardarNuevoTanque();
            
          }
        }
      ]
    });
    alert.present();
  }

}
