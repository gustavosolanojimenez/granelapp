import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { InformacionProvider } from './../../providers/informacion/informacion';
import { SqliteDbProvider } from './../../providers/sqlite-db/sqlite-db';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';

/**
 * Generated class for the ClientesNuevosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-clientes-nuevos',
  templateUrl: 'clientes-nuevos.html',
})
export class ClientesNuevosPage {

  miLat:number;
  miLong:number;
  clientesNuevos:any;

  nombreNegocio:AbstractControl;
  razonSocial:AbstractControl;
  subcanal: AbstractControl;
  especial: AbstractControl;
  correo: AbstractControl;
  telefonoFijo: AbstractControl;
  telefonoOpcional: AbstractControl;
  tipoVisita: AbstractControl;
  documento: AbstractControl;
  tipodoc: AbstractControl;
  //infotanque
  //tanque info
  numSerie: AbstractControl;
  capacidadlitros:AbstractControl;
  capacidadG;
  capacidadFinal;
  numcontrato:AbstractControl;
  marcaTanque:AbstractControl;
  fabricante:AbstractControl;
  contratoActivo:AbstractControl;
  posicionTanque:AbstractControl;
  estado:AbstractControl;
  fechafabricacion:AbstractControl;
  provincia:AbstractControl; 
  canton:AbstractControl; 
  distrito:AbstractControl;
  //dias
  days: AbstractControl;
  L:AbstractControl;
  K: AbstractControl;
  M: AbstractControl;
  J:AbstractControl;
  V:AbstractControl;
  S:AbstractControl;
  descuento;
  lat;
  long;
  idRuta;
  contadorenvio = 0;
  formgroup: FormGroup;

  cantones;
  distritos;
  pro = 9;
  can = 83;
  fechacompleta;
  //

  dias = [
    {
      name: 'L',
      value: 1,
      selected: false
    },
    {
      name: 'K',
      value: 1,
      selected: false
    },
    {
      name: 'M',
      value:1,
      selected: false
    },
    {
      name: 'J',
      value:1,
      selected: false
    },
    {
      name:'V',
      value:1,
      selected: false
    },
    {
      name:'S',
      value: 1,
      selected: false
    }
  ]


  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public db: SqliteDbProvider,
    public info:InformacionProvider,
    public alCtrl:AlertController,
    public geo:Geolocation,
    public formbuilder: FormBuilder) {
      this.getGeolocation();
      //id que se pasa desde home
      this.idRuta = this.navParams.get('id');
      this.obtenerNuevosClientes();

      this.formgroup =  formbuilder.group({
        nombreNegocio:['', Validators.required],
        razonSocial:['', Validators.required],
        subcanal:['',Validators.required],
        especial:['',Validators.required],
        correo:['',Validators.required],
        telefonoFijo:['',Validators.required],
        telefonoOpcional:['',Validators.required],
        tipoVisita:['',Validators.required],
        tipodoc:['',Validators.required],
        documento:['',Validators.required],
        L:['0'],
        K:['0'],
        M:['0'],
        J:['0'],
        V:['0'],
        S:['0']
      });

      
      this.nombreNegocio=  this.formgroup.controls['nombreNegocio'];
      this.razonSocial = this.formgroup.controls['razonSocial'];
      this.subcanal = this.formgroup.controls['subcanal'];
      this.especial = this.formgroup.controls['especial'];
      this.correo = this.formgroup.controls['correo'];
      this.telefonoFijo = this.formgroup.controls['telefonoFijo'];
      this.telefonoOpcional = this.formgroup.controls['telefonoOpcional'];
      this.tipoVisita = this.formgroup.controls['tipoVisita'];
      this.tipodoc = this.formgroup.controls['tipodoc'];
      this.documento = this.formgroup.controls['documento'];
      this.L = this.formgroup.controls['L'];
      this.K = this.formgroup.controls['K'];
      this.M = this.formgroup.controls['M'];
      this.J = this.formgroup.controls['J'];
      this.V = this.formgroup.controls['V'];
      this.S = this.formgroup.controls['S'];
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad ClientesNuevosPage');
  }

  onClick(check){
    console.log(check);
  }

  armarCliente(){
    var cliente = {
      codigo: '0',
      nombre: this.nombreNegocio,
      razon: this.razonSocial,
      descuento:this.descuento,
      precio: '0',
      tipo: '500',
      ruta: this.idRuta,
      subcanal: '0',
      formapago: 'efectivo',
      credito: '0',
      puedeFacturar: '0',
      lprecoa:'0',
      lprecioc: '0',
      ldescuento: '0',
      especial: this.especial,
      correo: this.correo,
      tipoCilza:'0',
      lat: this.lat,
      long: this.long,
      fechacreacion: new Date(),
      telefonoFijo: this.telefonoFijo,
      telefonoOpcional: this.telefonoOpcional,
      tipoVisita: this.tipoVisita,
      enviado: 0
    }

    console.log(cliente);
  }

  getGeolocation(){
    this.geo.getCurrentPosition().then((geoposition: Geoposition) =>{

      this.miLat = geoposition.coords.latitude;
      this.miLong = geoposition.coords.longitude;

      console.log("Esta es mi latitud:",this.miLat);
      console.log("Esta es mi longitud:",this.miLong);
    });
  }

  //metodo que guarda los motivos en sqlite
  guardarNuevoCliente(){




    var clienteNuevo = {





      
      codigo: '0',
      nombre: this.formgroup.value.nombreNegocio,
      razon: this.formgroup.value.razonSocial,
      descuento:0,
      precio: '0',
      tipo: '500',
      ruta: this.idRuta,
      subcanal: this.formgroup.value.subcanal,
      formadepago: 'efectivo',
      credito: '1',
      puedeFacturar: '0',
      lprecioa:'0',
      lprecioc: '0',
      ldescuento: '0',
      especial: this.formgroup.value.especial,
      tipodoc: this.formgroup.value.tipodoc,
      documento: this.formgroup.value.documento,
      correo:this.formgroup.value.correo,
      telefonoFijo:this.formgroup.value.telefonoFijo,
      telefonoOpcional:this.formgroup.value.telefonoOpcional,
      tipoVisita:this.formgroup.value.tipoVisita,
      tipoCilza:'22',
      LAT: this.lat,
      LONGI: this.long,
      fechacreacion: new Date(),
      enviado: 0,
      L:  this.formgroup.value.L,
      K:  this.formgroup.value.K,
      M:  this.formgroup.value.M,
      J:  this.formgroup.value.J,
      V: this.formgroup.value.V,
      S: this.formgroup.value.S

    }

    this.db.guardarClienteSQLite(clienteNuevo).then(data =>{
      console.log(data);
      console.log("Cliente guardado con exito")
    }).catch( error => { console.error(error) })

    this.formgroup.reset();
  }

  obtenerNuevosClientes(){
    this.db.getCientesNuevosSQLite().then((data) => {
      //allFacturasLoadingController.dismiss();
      this.clientesNuevos = data;

      console.log("Estos son los clientes nuevos:",this.clientesNuevos);
    }, (error) => {
      console.log(error);
    })
  }


  eventos(){
    this.guardarNuevoCliente();
    this.obtenerNuevosClientes();


    //this.navCtrl.push(MenuPage,{id:this.idRuta});
  }

  //metodo que envia la factura a la base de datos de TOMZA por medio del Api
  //se ocupa tener internet para enviarla
  enviarCliente(cliente:any){

    this.contadorenvio = this.contadorenvio +1;

    if(this.contadorenvio == 1){

      var clienteNuevo = {
        codigo: cliente.codigo,
        codhis: '0',
        proveedor: '0',
        GLN:null,
        EAN: null,
        codigoscad: '0',
        nombre: cliente.nombre,
        razon: cliente.razon,
        descuento: cliente.descuento,
        precio: cliente.precio,
        tipo: cliente.tipo,
        ruta: cliente.ruta,
        subcanal: cliente.subcanal,
        formapago: cliente.formapago,
        credito: cliente.credito,
        LAT: cliente.LAT,
        LONGI: cliente.LONGI,
        g: '0',
        puedeFacturar: cliente.puedeFacturar,
        lprecioa: cliente.lprecioa,
        lprecioc: cliente.lprecioc,
        ldescuento: cliente.ldescuento,
        especial: cliente.especial,
        L: cliente.L,
        K: cliente.K,
        M: cliente.M,
        J: cliente.J,
        V: cliente.V,
        S: cliente.S,
        Do: null,
        fechacreacion: cliente.fechacreacion,
        tipodoc: cliente.tipodoc,
        documento: cliente.documento,
        correo: cliente.correo,
        nombredoc: null,
        tipoCilza: cliente.tipoCilza,
        tipoVisita: cliente.tipoVisita,
        telefonoFijo: cliente.telefonoFijo,
        telefonoOpcional: cliente.telefonoOpcional
      }

      //console.log(fac);

      if(cliente.nombre !== ''){
        this.info.enviarClienteNuevo(clienteNuevo).then(data => {
          console.log('Enviado con Exito');
          console.log(data);
          this.actualizarEnviado(cliente.id);
          this.mensajeEnvio();
        }, err => {
        console.log(err);
        console.log("Error de envio sin internet");
        })
      }
    }else{
    console.log("ya se envio");
    }

    // this.info.enviarFaturas(fac);

    // this.actualizarEnviado(fac.facnumero);
  }

 //metodo que actualiza el campo enviado para cambiar el color en la interfaz
  actualizarEnviado(idCliente:any){
    this.db.actualizarEnviadoCliente({
      id: idCliente,
      enviado: 1
    }).then((data) => {
      console.log(data);
    }).catch(error => {console.error(error)});

    this.obtenerNuevosClientes();
  }

  mensajeEnvio() {
    let alert = this.alCtrl.create({
      title: 'Cliente Enviado',
      buttons: ['Aceptar']
    });
    alert.present();
  }




}
