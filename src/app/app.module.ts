import { ClientesNuevoTanquePage } from './../pages/clientes-nuevo-tanque/clientes-nuevo-tanque';
import { ClientesMantePage } from './../pages/clientes-mante/clientes-mante';
import { ImprimirMantePage } from './../pages/imprimir-mante/imprimir-mante';
import { UbicacionPage } from './../pages/ubicacion/ubicacion';
import { ClientesPage } from './../pages/clientes/clientes';
import { ClientesNuevosPage } from './../pages/clientes-nuevos/clientes-nuevos';
import { FacturasPipe } from './../pipes/facturas/facturas';
import { LoginPage } from './../pages/login/login';
import { FacturasPage } from './../pages/facturas/facturas';
import { InformacionProvider } from './../providers/informacion/informacion';
import { CierrePage } from './../pages/cierre/cierre';
import { TransaccionesPage } from './../pages/transacciones/transacciones';
import { DepositoPage } from './../pages/deposito/deposito';
import { GestionesPage } from './../pages/gestiones/gestiones';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';
import { PrinterProvider } from './../providers/printer/printer/printer';
import { ImprimirPage } from './../pages/imprimir/imprimir';
import { GastosPage } from './../pages/gastos/gastos';
import { BilletesPage } from './../pages/billetes/billetes';
import { BancoPage } from './../pages/banco/banco';
import { FacturacionPage } from './../pages/facturacion/facturacion';
import { MenuGranelPage } from './../pages/menu-granel/menu-granel';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, Injectable } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Camera } from '@ionic-native/camera';
import { InicioPage } from '../pages/inicio/inicio';
import { MenuMantenimientoPage } from '../pages/menu-mantenimiento/menu-mantenimiento';
import { NuevoMantenimientoPage } from '../pages/nuevo-mantenimiento/nuevo-mantenimiento';
import { RegistroTanquePage } from '../pages/registro-tanque/registro-tanque';
import { SqliteDbProvider } from '../providers/sqlite-db/sqlite-db';
import { InformacionApiProvider } from '../providers/informacion-api/informacion-api';
import { SQLite } from '@ionic-native/sqlite';
import { HttpClientModule } from '@angular/common/http';
import { ImagePicker } from '@ionic-native/image-picker';
import { MantenimientosPage } from '../pages/mantenimientos/mantenimientos';
import { TanquesPage } from '../pages/tanques/tanques';
import { Geolocation } from '@ionic-native/geolocation';
import { FiltroPipe } from '../pipes/filtro/filtro';
import { SearchPipe } from '../pipes/search/search';
import { SortPipe } from '../pipes/sort/sort';
import { ActulizarMantenimientoPage } from '../pages/actulizar-mantenimiento/actulizar-mantenimiento';
import { InicioMantenimientoPage } from '../pages/inicio-mantenimiento/inicio-mantenimiento';
import { ProvidersApiProvider } from '../providers/providers-api/providers-api';
import { enviroment } from '../env/environment';
import { DescargasDirectasPage } from '../pages/descargas-directas/descargas-directas';








@Injectable()
export class Service {
  constructor() { }
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    MenuGranelPage,
    InicioPage, MenuMantenimientoPage, NuevoMantenimientoPage, RegistroTanquePage, MantenimientosPage, TanquesPage
    , FacturacionPage, CierrePage, BancoPage, BilletesPage, ImprimirPage, GastosPage, GestionesPage, DepositoPage, TransaccionesPage, FacturasPage, ClientesNuevosPage,
    FiltroPipe,
    FacturasPipe,
    SearchPipe,
    SortPipe, LoginPage, ClientesPage, UbicacionPage,
    ImprimirMantePage,
    ClientesMantePage,
    ActulizarMantenimientoPage,
    ClientesNuevoTanquePage,
    InicioMantenimientoPage,
    DescargasDirectasPage,

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    NgxQRCodeModule,
    HttpClientModule,

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage, MenuGranelPage,
    InicioPage, MenuMantenimientoPage, NuevoMantenimientoPage, RegistroTanquePage, MantenimientosPage, TanquesPage, LoginPage
    , FacturacionPage, CierrePage, BancoPage, BilletesPage, ImprimirPage, GastosPage, GestionesPage, DepositoPage, TransaccionesPage, CierrePage, FacturasPage, ClientesNuevosPage, ClientesPage, UbicacionPage,
    ImprimirMantePage, ClientesMantePage,
    ActulizarMantenimientoPage, ClientesNuevoTanquePage, InicioMantenimientoPage, DescargasDirectasPage


  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    BarcodeScanner,
    Camera,
    SqliteDbProvider,
    InformacionApiProvider,
    SQLite,
    ImagePicker,
    PrinterProvider,
    BluetoothSerial,
    Geolocation,
    InformacionProvider,
    ProvidersApiProvider,


  ]
})

export class AppModule { }
