import { TransaccionesPage } from './../transacciones/transacciones';
import { SqliteDbProvider } from './../../providers/sqlite-db/sqlite-db';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';


/**
 * Generated class for the BancoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-banco',
  templateUrl: 'banco.html',
})
export class BancoPage {

  facturasSQLite:any = [];
  detallesSQLite:any = [];
  totalfac;
  ruta;
  idRuta;


  constructor(public navCtrl: NavController, public navParams: NavParams,
    public db: SqliteDbProvider) {
      this.idRuta = this.navParams.get('idRuta');
  }

  ionViewDidLoad() {
    this.obtenerFacturasDeSQLite();
    console.log('ionViewDidLoad BancoPage');
  }

  obtenerFacturasDeSQLite(){
    this.db.getFacturasSQLite().then((data) => {
      this.facturasSQLite = data;

      console.log(this.facturasSQLite);
    }, (error) => {
      console.log(error);
      console.log(this.facturasSQLite);
    })
  }

  IrATransacciones(factura: object){
this.navCtrl.push(TransaccionesPage, factura);
  }

}
