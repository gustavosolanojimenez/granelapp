import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { SqliteDbProvider } from '../../providers/sqlite-db/sqlite-db';
import { HttpClient } from '@angular/common/http';
import { MenuGranelPage } from '../menu-granel/menu-granel';
import { Observable } from 'rxjs/Observable';
import { enviroment } from '../../env/environment';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  apiURL= enviroment.url;
  qrData = null;
  createdCode = null;
  scannedCode = null;
  myPhoto: any;
  cod;
  name;
  razonSocial;
  pago;
  client;
  idRuta:number;
  clientesSQLite:any = [];
  clientesAPI:any = [];
  productosAPI:any = [];
  clientes:any = [];
  productos:any = [];
  contador = 0;

  factura;
  detalle;
  motivoNoCompra;
  clave;

  facturaAnular;
  TOMFactura;
  rutaFactura;
  nombreFactura;
  fechaFactura;
  detalleFactura;
  enviadaDetalle;

  //variables de ruta
  rutaEnUso; nombreRuta; unidadNegocioRuta; cediRuta; descripcionRuta;consecutivoRuta;cerosTOM;
  codRuta;


  //apiURL = 'http://174.138.38.52:3030/api/';
  //apiURL = 'https://api-tomza.herokuapp.com/api/';
  //variables para almacenar los datos que vienen del api
  rutasAPI:any = [];

  //otras variables
  rutasString: String;
  cedi:string;

  rutaCargada:any;
  primerRuta:any;
  rutasCedi:any = [];
  productosSQLite:any = [];

  rutasSQLite:any = [];


  constructor(public barcodeScanner: BarcodeScanner, public aCtrl: AlertController,  public http: HttpClient,private camera: Camera, public navCtrl: NavController, public db: SqliteDbProvider, public lCtrl: LoadingController) {

    this.getProductos();
  }

  crearCodigo(){
    this.createdCode =  this.qrData;

  }


  cargarRutas(){

    let allRutasLoadingController = this.lCtrl.create({
      content: "Obteniendo Rutas Desde el Servidor"
    });

    allRutasLoadingController.present();

    let data:Observable<any> = this.http.get(`${this.apiURL}rutas?filter[where][cedi]=${this.cedi}`);

    data.subscribe(result => {
      allRutasLoadingController.dismiss();
      this.rutasAPI = result;
      //console.log(this.rutasAPI);

      return result;
    })
  }

  obtenerRutaCedi(){

    let allRutasLoadingController = this.lCtrl.create({
      content: "Obteniendo Rutas Desde la Base de datos"
    });

    allRutasLoadingController.present();
    console.log("Este es el cedi",this.cedi);
    this.db.getRutasSQLiteCedi(this.cedi).then((data) => {
      allRutasLoadingController.dismiss();
      this.rutasAPI= data;
      console.log(this.rutasAPI);

    },(error) =>{
      console.log(error);
    })
  }

    //metodo que obtiene los clientes del api filtrando por ruta para posteriormente guardarlos en SQLite
    // getClientesRuta(){
    //   let data:Observable<any> =  this.http.get(`${this.apiURL}clientes?filter[where][ruta]=${this.idRuta}&filter[where][tipo][neq]=500`);

    //   let allClientesLoadingController = this.lCtrl.create({
    //     content: "Insertando clientes y productos...Espere"
    //   });

    //   allClientesLoadingController.present();

    //   data.subscribe(result => {
    //     this.clientesAPI = result;
    //     // console.log('***************');
    //     // console.log(this.clientesAPI);
    //     // console.log('***************');

    //        //recorro las rutas que obtuve del api
    //         for(var index in this.clientesAPI){
    //           //armo cada cliente para despues almacenarlos
    //           let cliente = {
    //             codigo: this.clientesAPI[index].codigo,
    //             nombre: this.clientesAPI[index].nombre,
    //             GLN: this.clientesAPI[index].GLN,
    //             EAN: this.clientesAPI[index].EAN,
    //             razon: this.clientesAPI[index].razon,
    //             descuento: this.clientesAPI[index].descuento,
    //             precio: this.clientesAPI[index].precio,
    //             tipo: this.clientesAPI[index].tipo,
    //             ruta: this.clientesAPI[index].ruta,
    //             subcanal: this.clientesAPI[index].subcanal,
    //             formadepago: this.clientesAPI[index].formadepago,
    //             credito: this.clientesAPI[index].credito,
    //             LAT: this.clientesAPI[index].LAT,
    //             LONGI: this.clientesAPI[index].LONGI,
    //             puedeFacturar: this.clientesAPI[index].puedeFacturar,
    //             lprecioa: this.clientesAPI[index].lprecioa,
    //             lprecioc: this.clientesAPI[index].lprecioc,
    //             ldescuento: this.clientesAPI[index].ldescuento,
    //             especial: this.clientesAPI[index].especial,
    //             L: this.clientesAPI[index].L,
    //             K: this.clientesAPI[index].K,
    //             M: this.clientesAPI[index].M,
    //             J: this.clientesAPI[index].J,
    //             V: this.clientesAPI[index].V,
    //             S: this.clientesAPI[index].S,
    //             Do: this.clientesAPI[index].Do,
    //             correo: this.clientesAPI[index].correo,
    //             tipoCilza: this.clientesAPI[index].tipoCilza,
    //             hizoMotivo: 0,
    //             hizoFactura:0
    //           }

    //           console.log("Clientes",cliente);
    //           //guardamos los objetos cliente en la BD SQLite
    //           this.db.guardarClientesSQLite(cliente);
    //         }
    //         allClientesLoadingController.dismiss();
    //     return result;
    //   })
    // }


    ionViewDidLoad() {
      this.obtenerDatos();
      //this.obtenerRutaDesdeCliente();
      this.guardarProductos();

    }

  // getProductos(){
  //   let data:Observable<any> =  this.http.get(`${this.apiURL}productos`);

  //   data.subscribe(result => {
  //     this.productosAPI = result;
  //     //console.log(this.productosAPI);

  //     return result;
  //   })
  // }

  //  //llamo el metodo del DatabaseProvider para almacenar los productos en SQLite
  //  guardarProductos(){
  //   //recorro los productos que obtuve del api
  //   for(var index in this.productosAPI){
  //     //armo cada cliente para despues almacenarlos
  //     let producto = {
  //       id:this.productosAPI[index].id,
  //       producto: this.productosAPI[index].producto,
  //       tipoPrecio: this.productosAPI[index].tipoPrecio,
  //       precio: this.productosAPI[index].precio,
  //       tipoCliente: this.productosAPI[index].tipoCliente,
  //       descripcion: this.productosAPI[index].descripcion,
  //       precioViejo: this.productosAPI[index].precioViejo
  //     }

  //     this.db.guardarProductosSQLite(producto);
  //   }
  // }

    //metodo que redirige a MenuPage, obtiene los clientes por ruta y llena la BD sqlite
    llenarSQLite(){

      //metodo que obtiene los clientes de la ruta seleccionada en el login
      //linea de codigo que redirige a la pantalla de menu y se lleva el id de la ruta seleccionada
      this.navCtrl.push(MenuGranelPage,{id:this.idRuta});
      //llamada a los metodos que guardan los datos en la bd de sqlite
      //this.guardarRutas();
      this.guardarProductos();

      //ver rutas guardadas en SQLite
      //this.obtenerRutasDeSQLite();
    }
    obtenerDatos(){

      this.db.getAllProductsSQLite().then((data) => {
        //allFacturasLoadingController.dismiss();
        this.productosSQLite = data;
        //console.log("Estas son los clientes de SQLite",this.productosSQLite);
        //console.log(this.facturasSQLite);
      }, (error) => {
        console.log(error);
      })


      this.db.getRutasSQLite().then((data) => {
        this.rutasSQLite = data;
      },(error) => {
        console.log(error);
      })
      }

  escanearCodigo(){
    this.barcodeScanner.scan().then(barcodeData => {
      this.scannedCode = barcodeData.text;

      this.client = JSON.parse(this.scannedCode);
      this.cod = this.client.codigo;
      this.name = this.client.nombre;

    })
  }

  //metodo para ir a la vista de facturacion y listar los clientes
  // iraMenu(){
  //   this.navCtrl.push(MenuGranelPage);
  // }

  tomarFoto(){

    const options: CameraOptions = {
      quality: 20,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {

      this.myPhoto = 'data:image/jpeg;base64,'+ imageData;
      console.log(this.myPhoto);

    }, (err) => {

    });
  }

  obtenerRutaCedi1(){

    let allRutasLoadingController = this.lCtrl.create({
      content: "Obteniendo Rutas Desde la Base de datos"
    });

    allRutasLoadingController.present();
    console.log("Este es el cedi",this.cedi);
    this.db.getRutasSQLiteCedi(this.cedi).then((data) => {
      allRutasLoadingController.dismiss();
      this.rutasAPI= data;
      console.log(this.rutasAPI);

    },(error) =>{
      console.log(error);
    })
  }
  guardarRutas(){

    //recorro las rutas que obtuve del api
   for(var index in this.rutasAPI){
     //armo cada ruta para despues almacenarlas
     let ruta = {
      id: this.rutasAPI[index].id,
      nombre: this.rutasAPI[index].nombre,
      unidad_negocio: this.rutasAPI[index].unidad_negocio,
      cedi: this.rutasAPI[index].cedi,
      descripcion: this.rutasAPI[index].descripcion,
      cerosTOM: this.rutasAPI[index].cerosTOM,
      consecutivo: this.rutasAPI[index].consecutivo
     }

     //console.log(ruta);
     //llamo al metodo del provider para pasar el objeto ruta y que lo almacene en SQLite
     this.db.guardarRutasSQLite(ruta);
   }

  }


  getProductos(){
    let data:Observable<any> =  this.http.get(`${this.apiURL}productos`);

    data.subscribe(result => {
      this.productosAPI = result;
      //console.log(this.productosAPI);

      return result;
    })
  }

  //metodo que guarda las rutas dentro de SQLite utilizando el metodo del DatabaseProvider
  // guardarRutas(){

  //   //recorro las rutas que obtuve del api
  //  for(var index in this.rutasAPI){
  //    //armo cada ruta para despues almacenarlas
  //    let ruta = {
  //     id: this.rutasAPI[index].id,
  //     nombre: this.rutasAPI[index].nombre,
  //     unidad_negocio: this.rutasAPI[index].unidad_negocio,
  //     cedi: this.rutasAPI[index].cedi,
  //     descripcion: this.rutasAPI[index].descripcion,
  //     cerosTOM: this.rutasAPI[index].cerosTOM,
  //     consecutivo: this.rutasAPI[index].consecutivo
  //    }

  //    //console.log(ruta);
  //    //llamo al metodo del provider para pasar el objeto ruta y que lo almacene en SQLite
  //    this.db.guardarRutasSQLite(ruta);
  //  }

  // }

 //llamo el metodo del DatabaseProvider para almacenar los productos en SQLite
  guardarProductos(){
    //recorro los productos que obtuve del api
    for(var index in this.productosAPI){
      //armo cada cliente para despues almacenarlos
      let producto = {
        id:this.productosAPI[index].id,
        producto: this.productosAPI[index].producto,
        tipoPrecio: this.productosAPI[index].tipoPrecio,
        precio: this.productosAPI[index].precio,
        tipoCliente: this.productosAPI[index].tipoCliente,
        descripcion: this.productosAPI[index].descripcion,
        precioViejo: this.productosAPI[index].precioViejo
      }

      this.db.guardarProductosSQLite(producto);
    }
  }


  }

