import { Component } from '@angular/core';
import { NavController, NavParams,LoadingController, AlertController } from 'ionic-angular';
import { InformacionProvider } from '../../providers/informacion/informacion';
import { ImprimirPage } from '../imprimir/imprimir';
import { SqliteDbProvider } from '../../providers/sqlite-db/sqlite-db';
/**
 * Generated class for the FacturasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-facturas',
  templateUrl: 'facturas.html',
})

export class FacturasPage {

   //variables a utilizar
   facturasSQLite:any = [];
   detallesSQLite:any = [];
   motivoNoCompra:any = [];
   motivos: any = [];
   claves:any = [];
   clase = '';

   //para factura

   //variables para la anulacion de la factura
   facturaAnular:any;
   detalleFactura:any;
   TOMFactura; rutaFactura; nombreFactura; fechaFactura; enviadaFactura; enviadaDetalle;
   claveConsecutivo:any;
  factura;
   consecutivo;
   clave;
   detalleTanque:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public db:SqliteDbProvider,
              public lCtrl: LoadingController, public info: InformacionProvider,public aCtrl: AlertController)
  {

  }

  ionViewDidLoad() {
    this.obtenerFacturasDeSQLite();
    this.obtenerDetallesDeSQLite();
  }

  //metodo que obtiene las facturas almacenados en SQLite utilizando el metodo de DatabaseProvider
  obtenerFacturasDeSQLite(){

    // let allFacturasLoadingController = this.lCtrl.create({
    //   content: "Obteniendo facturas de la Base de Datos"
    // });

    // allFacturasLoadingController.present();

    this.db.getFacturasSQLite().then((data) => {
      //allFacturasLoadingController.dismiss();
      this.facturasSQLite = data;
      console.log("Estas son las facturas de SQLite",this.facturasSQLite);
      //console.log(this.facturasSQLite);
    }, (error) => {
      console.log(error);
    })
  }

  //metodo que obtiene los detalles almacenados en SQLite utilizando el metodo de DatabaseProvider
  obtenerDetallesDeSQLite(){

    // let allDetallesLoadingController = this.lCtrl.create({
    //   content: "Obteniendo detalles de la Base de Datos"
    // });

    // allDetallesLoadingController.present();

    this.db.getDetallesSQLite().then((data) => {
      //allDetallesLoadingController.dismiss();
      this.detallesSQLite = data;
      console.log('Estos son los detalles de SQLite',this.detallesSQLite);
    }, (error) => {
      console.log(error);
    })
  }

  //metodo que obtiene la factura especifica para actualizarla
  obtenerFactura(TOM:any){
    this.db.getFacturaPorTOM(parseInt(TOM)).then((data) => {
      this.facturaAnular = data;
      this.TOMFactura =  this.facturaAnular.facnumero;
      this.rutaFactura =  this.facturaAnular.ruta;
      this.nombreFactura =  this.facturaAnular.nombre;
      this.fechaFactura = this.facturaAnular.fechaFactura;

    },(error) =>{
      console.log(error);
    })
  }

  //metodo que obtiene los detalle especifico de una factura para enviarlo y actualizar el dato enviada
  obtenerDetalle(TOM:any){
    this.db.getDetallePorTOM(parseInt(TOM)).then((data) => {
      this.detalleFactura = data;
      this.TOMFactura =  this.detalleFactura.facnumero;
      this.enviadaDetalle = this.detalleFactura.enviada;

      //llamo al metodo enviar factura
      this.enviarDetalle(this.detalleFactura);
    })
  }

  obtenerClaveConsecutivo(TOM:any){
    this.db.getClaveConsecutivoPorTOM(parseInt(TOM)).then((data) => {
      this.claveConsecutivo = data;

      this.enviarClaveConsecutivo(this.claveConsecutivo);

    })
  }

  obtenerDetalleTanque(TOM:any){
    this.db.getDetalleTanquePorTOM(parseInt(TOM)).then((data) => {

      this.detalleTanque = data;
 
      console.log("Este es el detalle",this.detalleTanque);
      this.enviarDetalleTanque(this.detalleTanque);
    })
  }

  //metodo que actualiza la ruta de la factura para que quede nula en el sistema
  // anularFactura(TOM:any){
  //   this.db.anularFacturas({
  //    ruta: 0,
  //    facnumero: TOM
  //   }).then((data) => {
  //     console.log(data);
  //   }).catch( error => { console.error(error) });

  //   //this.obtenerFacturasDeSQLite();
  // }

  //metodo que actualiza el campo enviado para cambiar el color en la interfaz
  actualizarEnviado(TOM:any){
    this.db.actualizarEnviadoFactura({
      facnumero: TOM,
      enviada: 1
    }).then((data) => {
      console.log(data);
    }).catch(error => {console.error(error)});

    this.obtenerFacturasDeSQLite();
  }

  //metodo que envia la factura a la base de datos de TOMZA por medio del Api
  //se ocupa tener internet para enviarla
  enviarFactura(factura){


      var fac = {
        codigo: factura.codigo,
        facnumero: factura.facnumero,
        razon_social: factura.razon_social,
        nombre: factura.nombre,
        descuento: factura.descuento,
        descuentot: factura.descuentot,
        credito: factura.credito,
        total:factura.total,
        ruta:factura.ruta,
        fecha: factura.fecha,
        q25: factura.q25,
        q25r: factura.q25r,
        q100: factura.q100,
        q20: factura.q20,
        q20r: factura.q20r,
        q10: factura.q10,
        q10r: factura.q10r,
        q35: factura.q35,
        q35r: factura.q35r,
        q45: factura.q45,
        q45r: factura.q45r,
        q60:factura.q60,
        q60r:factura.q60r,
        qlts: factura.qtls,
        qkgs: factura.qkgs,
        LAT: factura.LAT,
        LONGI: factura.LONGI,
        g: factura.g,
        efectivo: factura.efectivo,
        transmonto: factura.transmonto,
        transnumero: factura.transnumero,
        chknumero : factura.chknumero,
        chkbanco: factura.chkbanco,
        chkmonto: factura.chkmonto,
        tipo: factura.tipo,
        cz25: factura.cz25,
        cz25r: factura.cz25r,
        cz100r:factura.cz100r,
        fechadt: factura.fechadt
      }

      //console.log(fac);

      if(factura.nombre !== ''){
        this.info.enviarFaturas(fac).then(data => {
          this.EnviarMotivosDeSQLite();

          console.log('Enviado con Exito');
          console.log(data);
          this.actualizarEnviado(fac.facnumero);
          this.mensajeEnvio();
        }, err => {
          this.mensajeNoEnvio();
          console.log(err);
         console.log("Error de envio sin internet");
        })
      }

      // this.info.enviarFaturas(fac);

      // this.actualizarEnviado(fac.facnumero);
  }


   //metodo que envia el detalle de la factura a la base de datos de  TOMZA por medio del API
   //se ocupa tener internet para enviarla
   enviarDetalle(detalle){

    var detail = {
      idcliente: detalle.idcliente,
      ruta: detalle.ruta,
      facnumero: detalle.facnumero,
      totalexenta: detalle.totalexenta,
      total: detalle.total,
      fechacreacion: detalle.fechacreacion,
      detalle: detalle.detalle,
      rawdetalle: detalle.rawdetalle,
      detail : detalle.detail,
      corre: detalle.corre,
      estado: detalle.estado,
    }

    this.info.enviarDetalles(detail);
   }

   enviarClaveConsecutivo(claveConsecutivo){

    var clave = {
      clave: claveConsecutivo.clave,
      consecutivo: claveConsecutivo.consecutivo,
      facnumero: claveConsecutivo.facnumero,
      fecha: claveConsecutivo.fecha
    }

    this.info.enviarClaves(clave);
   }

   enviarDetalleTanque(detalleTanque){

    var deTanque = {
      ruta: detalleTanque.ruta,
      numfactura: detalleTanque.numfactura,
      detalle : detalleTanque.detalle,
      codcliente: detalleTanque.codcliente,
      fecha: detalleTanque.fecha
    }

    this.info.enviarDetallesTanques(deTanque);
   }

    mensajeEnvio() {
      let alert = this.aCtrl.create({
        title: 'Factura enviada',
        buttons: ['Aceptar']
      });
      alert.present();
    }

    mensajeNoEnvio() {
      let alert = this.aCtrl.create({
        title: 'No hay internet, intentalo cuando tengas internet',
        buttons: ['Aceptar']
      });
      alert.present();
    }

    obtenerMotivosNoCompra(){
      this.db.getMotivosSQLiteNoEnviados().then((data) => {
        this.motivoNoCompra = data;
        console.log(this.motivoNoCompra);

        for(var index in this.motivoNoCompra){

          var motivo = {
            codigoCliente: this.motivoNoCompra[index].codigoCliente,
            fecha: this.motivoNoCompra[index].fecha,
            ruta: this.motivoNoCompra[index].ruta,
            motivo:this.motivoNoCompra[index].motivo,
            fechaRuta:this.motivoNoCompra[index].fechaRuta
          }


         // this.info.enviarMotivosNoCompraAutomaticamente(motivo);

          this.actualizarEnviadoMotivo(motivo.codigoCliente);

          //this.eliminarMotivoNoCompra(motivo.codigoCliente);


        }
      }).catch( error => { console.error(error) })
    }

    EnviarMotivosDeSQLite() {
      this.db.getMotivosSQLite().then((data) => {
        this.motivos = data;
        console.log('Se enviaron desde fac', this.motivos);
        for (var index in this.motivos) {
          //armo cada cliente para despues almacenarlos
          let motivo = {
            codigoCliente: this.motivos[index].codigoCliente,
            fecha: this.motivos[index].fecha,
            ruta: this.motivos[index].ruta,
            motivo: this.motivos[index].motivo,
            fechaRuta:this.motivos[index].fechaRuta
          }
          console.log('Linea 281', motivo);
          this.info.enviarMotivosNoCompra(motivo);
        }
        console.log(data);
      }).catch(error => { console.error(error) })
    }


    actualizarEnviadoMotivo(codigo:any){
      this.db.actualizarEnviadoMotivo({
        codigoCliente: codigo,
        enviado: 1
      }).then((data) => {
        console.log(data);
      }).catch(error => {console.error(error)});

    }

    //metodo que lleva a la vista de imprimir
    irAImprimir(facnumero:any,name:any) {

      this.db.getClaveConsecutivoPorTOM(parseInt(facnumero)).then((data) => {

        this.claves = data;
        this.consecutivo = this.claves.consecutivo;
        this.clave= this.claves.clave;

        this.navCtrl.push(ImprimirPage,{TOM: facnumero , nombre: name, claveFactura: this.clave, consecutivoFactura:this.consecutivo});

      }, (error) => {
        console.log(error);
      })


    }
}
