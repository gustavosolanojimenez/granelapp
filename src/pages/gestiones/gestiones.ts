import { DepositoPage } from './../deposito/deposito';
import { BilletesPage } from './../billetes/billetes';
import { GastosPage } from './../gastos/gastos';
import { BancoPage } from './../banco/banco';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';


/**
 * Generated class for the GestionesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-gestiones',
  templateUrl: 'gestiones.html',
})
export class GestionesPage {
idRuta;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.idRuta = this.navParams.get('id');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GestionesPage');
  }

  IrABanco() {
    this.navCtrl.push(BancoPage, { id: this.idRuta })
   // this.navCtrl.push(BancoPage)
  }

  IrABilletes() {
    this.navCtrl.push(BilletesPage, { id: this.idRuta })
    //this.navCtrl.push(BilletesPage)
  }

  irAGastos() {
    this.navCtrl.push(GastosPage, { id: this.idRuta })
    //this.navCtrl.push(GastosPage)
  }

  irADepositos() {
    //this.navCtrl.push(DepositoPage)
   this.navCtrl.push(DepositoPage, { id: this.idRuta })
  }

}
