import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ToastController, AlertController, LoadingController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { MenuMantenimientoPage } from '../menu-mantenimiento/menu-mantenimiento';
import { HttpClient } from '@angular/common/http';
import { SqliteDbProvider } from '../../providers/sqlite-db/sqlite-db';
import { InformacionApiProvider } from '../../providers/informacion-api/informacion-api';
import { InicioPage } from '../inicio/inicio';
import { MantenimientosPage } from '../mantenimientos/mantenimientos';
import { enviroment } from '../../env/environment';

/**
 * Generated class for the InicioMantenimientoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-inicio-mantenimiento',
  templateUrl: 'inicio-mantenimiento.html',
})
export class InicioMantenimientoPage {
  apiURL= enviroment.url;
  login = 0;
  //apiURL = 'http://174.138.38.52:3030/api/';
   //apiURL = 'https://api-tomza.herokuapp.com/api/';
  //variables para almacenar los datos que vienen del api
  rutasAPI:any = [];
  clientesAPI:any = [];
  productosAPI:any = [];
  //otras variables
  rutasString: String;
  cedi:string;
  idRuta:string;
  rutaCargada:any;
  primerRuta:any;


  rutasCedi:any = [];
  productosSQLite:any = [];
  clientesSQLite:any = [];
  rutasSQLite:any = [];


  codRuta;
  nombreRuta;
  unidadNegocioRuta;
  cediRuta;
  descripcionRuta;
  cerosTOM;
  consecutivoRuta;
  toastController: any;


  constructor(public navCtrl: NavController,public platform:Platform, public toast: ToastController,public aCtrl: AlertController,public http: HttpClient, public navParams: NavParams, public db: SqliteDbProvider, public lCtrl: LoadingController, public info: InformacionApiProvider) {




  }

  ionViewDidLoad() {
    this.obtenerDatos();
    this.obtenerRutaDesdeCliente();
  }

  volver(){
    this.login = 0;
  }
 

  irAMenu(){
    //this.login = 1;
    this.navCtrl.push(MenuMantenimientoPage);
  }

  irAMenuMantenimiento(){
    this.navCtrl.push(MenuMantenimientoPage);
  }

  volverInicio(){
    this.navCtrl.push(InicioPage);
  }

  llenarSoloClientes(){
    this.getClientesRuta();
    this.navCtrl.push(MenuMantenimientoPage,{id:this.idRuta});
  }

  

  obtenerRutaCedi(){

    let allRutasLoadingController = this.lCtrl.create({
      content: "Obteniendo Rutas Desde la Base de datos"
    });

    allRutasLoadingController.present();
    console.log("Este es el cedi",this.cedi);
    this.db.getRutasSQLiteCedi(this.cedi).then((data) => {
      allRutasLoadingController.dismiss();
      this.rutasAPI= data;
      console.log(this.rutasAPI);

    },(error) =>{
      console.log(error);
    })
  }

  validacion(){

    if(this.clientesSQLite.length === 0 ){
      this.getClientesGranel()
    }else{
      console.log("ya los clientes han sido ingresados");
    }
  }

  //metodo que obtiene los clientes del api filtrando por ruta para posteriormente guardarlos en SQLite
  getClientesRuta(){
    let data:Observable<any> =  this.http.get(`${this.apiURL}clientes?filter[where][ruta]=${this.idRuta}&filter[where][tipo][neq]=500`);

    let allClientesLoadingController = this.lCtrl.create({
      content: "Insertando clientes... Espere"
    });

    allClientesLoadingController.present();

    data.subscribe(result => {
      this.clientesAPI = result;
      // console.log('***************');
      // console.log(this.clientesAPI);
      // console.log('***************');

         //recorro las rutas que obtuve del api
          for(var index in this.clientesAPI){
            //armo cada cliente para despues almacenarlos
            let cliente = {
              codigo: this.clientesAPI[index].codigo,
              nombre: this.clientesAPI[index].nombre,
              GLN: this.clientesAPI[index].GLN,
              EAN: this.clientesAPI[index].EAN,
              razon: this.clientesAPI[index].razon,
              descuento: this.clientesAPI[index].descuento,
              precio: this.clientesAPI[index].precio,
              tipo: this.clientesAPI[index].tipo,
              ruta: this.clientesAPI[index].ruta,
              subcanal: this.clientesAPI[index].subcanal,
              formadepago: this.clientesAPI[index].formadepago,
              credito: this.clientesAPI[index].credito,
              LAT: this.clientesAPI[index].LAT,
              LONGI: this.clientesAPI[index].LONGI,
              puedeFacturar: this.clientesAPI[index].puedeFacturar,
              lprecioa: this.clientesAPI[index].lprecioa,
              lprecioc: this.clientesAPI[index].lprecioc,
              ldescuento: this.clientesAPI[index].ldescuento,
              especial: this.clientesAPI[index].especial,
              L: this.clientesAPI[index].L,
              K: this.clientesAPI[index].K,
              M: this.clientesAPI[index].M,
              J: this.clientesAPI[index].J,
              V: this.clientesAPI[index].V,
              S: this.clientesAPI[index].S,
              Do: this.clientesAPI[index].Do,
              correo: this.clientesAPI[index].correo,
              tipoCilza: this.clientesAPI[index].tipoCilza,
              hizoMotivo: 0,
              hizoFactura:0
            }

            console.log("Clientes",cliente);
            //guardamos los objetos cliente en la BD SQLite
            this.db.guardarClientesSQLite(cliente);
          }
          allClientesLoadingController.dismiss();
      return result;
    })
  }

 
  //metodo que obtiene los clientes del api filtrando por ruta para posteriormente guardarlos en SQLite
  getClientesGranel(){
    let data:Observable<any> =  this.http.get(`${this.apiURL}clientes?filter[where][tipo][inq]=6&filter[where][tipo][inq]=7`);

    let allClientesLoadingController = this.lCtrl.create({
      content: "Insertando clientes...Espere"
    });

    allClientesLoadingController.present();

    data.subscribe(result => {
      this.clientesAPI = result;
      // console.log('***************');
      console.log(this.clientesAPI);
      // console.log('***************');

         //recorro las rutas que obtuve del api
          for(var index in this.clientesAPI){
            //armo cada cliente para despues almacenarlos
            let cliente = {
              codigo: this.clientesAPI[index].codigo,
              nombre: this.clientesAPI[index].nombre,
              GLN: this.clientesAPI[index].GLN,
              EAN: this.clientesAPI[index].EAN,
              razon: this.clientesAPI[index].razon,
              descuento: this.clientesAPI[index].descuento,
              precio: this.clientesAPI[index].precio,
              tipo: this.clientesAPI[index].tipo,
              ruta: this.clientesAPI[index].ruta,
              subcanal: this.clientesAPI[index].subcanal,
              formadepago: this.clientesAPI[index].formadepago,
              credito: this.clientesAPI[index].credito,
              LAT: this.clientesAPI[index].LAT,
              LONGI: this.clientesAPI[index].LONGI,
              puedeFacturar: this.clientesAPI[index].puedeFacturar,
              lprecioa: this.clientesAPI[index].lprecioa,
              lprecioc: this.clientesAPI[index].lprecioc,
              ldescuento: this.clientesAPI[index].ldescuento,
              especial: this.clientesAPI[index].especial,
              L: this.clientesAPI[index].L,
              K: this.clientesAPI[index].K,
              M: this.clientesAPI[index].M,
              J: this.clientesAPI[index].J,
              V: this.clientesAPI[index].V,
              S: this.clientesAPI[index].S,
              Do: this.clientesAPI[index].Do,
              correo: this.clientesAPI[index].correo,
              tipoCilza: this.clientesAPI[index].tipoCilza,
              hizoMotivo: 0,
              hizoFactura:0
            }

            //console.log("Clientes",cliente);
            //guardamos los objetos cliente en la BD SQLite
            this.db.guardarClientesSQLite(cliente);
          }
          allClientesLoadingController.dismiss();
      return result;
    })
  }



  //metodo que redirige a MenuPage, obtiene los clientes por ruta y llena la BD sqlite
  llenarSQLite(){

    //metodo que obtiene los clientes de la ruta seleccionada en el login
    this.getClientesRuta();
    //linea de codigo que redirige a la pantalla de menu y se lleva el id de la ruta seleccionada
    this.navCtrl.push(MenuMantenimientoPage,{id:this.idRuta});
    //llamada a los metodos que guardan los datos en la bd de sqlite
    //this.guardarRutas();


    //ver rutas guardadas en SQLite
    //this.obtenerRutasDeSQLite();
  }

  //metodo que redirige a MenuPage, obtiene los clientes por ruta y llena la BD sqlite
  llenarSQLiteProductosClientes(){

    //metodo que obtiene los clientes de la ruta seleccionada en el login
    this.getClientesRuta();
    //linea de codigo que redirige a la pantalla de menu y se lleva el id de la ruta seleccionada
    this.navCtrl.push(MenuMantenimientoPage,{id:this.idRuta});
    //llamada a los metodos que guardan los datos en la bd de sqlite


    //ver rutas guardadas en SQLite
    //this.obtenerRutasDeSQLite();
  }

  obtenerLaRutaQueSeCargo(){

    let allRutasLoadingController = this.lCtrl.create({
    content: "Obteniendo Ruta Desde la Base de datos"
    });

    allRutasLoadingController.present();

    this.db.obtenerLaRutaCargada(parseInt(this.rutaCargada[0].ruta)).then((data) => {
    //allFacturasLoadingController.dismiss();
    allRutasLoadingController.dismiss();
    this.rutasAPI = data;
    console.log("Esta es la ruta cargada:" ,this.rutasAPI);
    //console.log("Estas son los clientes de SQLite",this.productosSQLite);
    //console.log(this.facturasSQLite);
    }, (error) => {
    console.log(error);
    })
  }
  

  obtenerDatos(){

    // this.db.getAllProductsSQLite().then((data) => {
    //   //allFacturasLoadingController.dismiss();
    //   this.productosSQLite = data;
    //   //console.log("Estas son los clientes de SQLite",this.productosSQLite);
    //   //console.log(this.facturasSQLite);
    // }, (error) => {
    //   console.log(error);
    // })
  
    this.db.getClientesSQLite().then((data) => {
      this.clientesSQLite =data;
  
    }, (error) => {
      console.log(error);
    })
  
    this.db.getRutasSQLite().then((data) => {
      this.rutasSQLite = data;
    },(error) => {
      console.log(error);
    })
  }

  obtenerRutaDesdeCliente(){
    this.db.obtenerRutaEspecificaCargada().then((data) => {
      //allFacturasLoadingController.dismiss();
      this.rutaCargada = data;
      console.log("Esta es la ruta cargada:" ,this.rutaCargada[0].ruta);
      //console.log("Estas son los clientes de SQLite",this.productosSQLite);
      //console.log(this.facturasSQLite);
    }, (error) => {
      console.log(error);
    })
  }
  
}
