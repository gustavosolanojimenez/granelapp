import { DescargasDirectasPage } from './../descargas-directas/descargas-directas';
import { ClientesPage } from './../clientes/clientes';
import { ClientesNuevosPage } from './../clientes-nuevos/clientes-nuevos';
import { FacturasPage } from './../facturas/facturas';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { BilletesPage } from './../billetes/billetes';
import { InformacionProvider } from './../../providers/informacion/informacion';
import { SqliteDbProvider } from './../../providers/sqlite-db/sqlite-db';
import { InicioPage } from './../inicio/inicio';
import { GestionesPage } from './../gestiones/gestiones';
import { CierrePage } from './../cierre/cierre';
import { FacturacionPage } from './../facturacion/facturacion';
import { Component } from '@angular/core';
import { NavController, NavParams, Platform, AlertController, LoadingController } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { enviroment } from '../../env/environment';


@Component({
  selector: 'page-menu-granel',
  templateUrl: 'menu-granel.html',
})
export class MenuGranelPage {
  apiURL = enviroment.url;
  idRuta: any
  qrData = null;
  createdCode = null;
  scannedCode = null;
  myPhoto: any;

  cod; name; razonSocial; tipoPago; client; descuento; tipo; Comentario; Activo; g; nombredoc;


  clientesSQLite: any = [];
  clientesAPI: any = [];
  productosAPI: any = [];
  clientes: any = [];
  productos: any = [];
  contador = 0;

  factura;
  detalle;
  motivoNoCompra;
  clave;

  facturaAnular;
  TOMFactura;
  rutaFactura;
  nombreFactura;
  fechaFactura;
  detalleFactura;
  enviadaDetalle;
  serieTanque;

  //variables de ruta
  rutaEnUso; nombreRuta; unidadNegocioRuta; cediRuta; descripcionRuta; consecutivoRuta; cerosTOM;
  codRuta;
  precio;
  GLN;
  EAN;
  credito;
  lon
  latitud = '9.89348225';
  longitud = '-84.10609696';
  cliente: any;
  razon;

  //apiURL = 'http://174.138.38.52:3030/api/';
  // apiURL = 'https://api-tomza.herokuapp.com/api/';
  rutaWaze = 'https://www.waze.com/ul?ll=';

  constructor(public platform: Platform, public navCtrl: NavController, public navParams: NavParams, public barcodeScanner: BarcodeScanner,
    public db: SqliteDbProvider, public info: InformacionProvider, public aCtrl: AlertController, public lCtrl: LoadingController, public http: HttpClient) {

    this.idRuta = this.navParams.get('id');
    this.clientesSQLite;
    this.obtenerRutaEnUso();



    platform.registerBackButtonAction(() => {

    }, 1);

  }

  // obtenerwaze(){
  //   return new Promise(resolve => {
  //     this.http.get(`${this.rutaWaze}`+ this.latitud +`%2C`+ this.longitud + `&navigate=yes&zoom=17`  ).subscribe( data => {
  //       resolve(data);
  //     }, err => {
  //       console.log(err);

  //     });

  //   })

  // }

  iraCierre() {
    this.navCtrl.push(CierrePage, { id: this.idRuta });
  }
  iraInicio() {
    this.navCtrl.push(InicioPage);
  }

  iraGestiones() {
    this.navCtrl.push(GestionesPage, { id: this.idRuta });
  }

  iraFacturas() {
    this.navCtrl.push(FacturasPage);
  }

  iraClientes() {
    this.navCtrl.push(ClientesPage)
  }

  // iraDescargas() {
  //   this.navCtrl.push(DescargasDirectasPage)
  // }

  escanearCodigo1() {
    this.barcodeScanner.scan().then(barcodeData => {
      this.scannedCode = barcodeData.text;

      this.client = JSON.parse(this.scannedCode);
      this.cod = this.client.codigo;
      // this.name = this.client.nombre;
      //this.razonSocial = this.client.razon;
      this.tipoPago = this.client.credito;
      this.serieTanque = this.client.serie;

      //this.descuento = this.client.descuento;
      //this.tipo = this.client.tipo;

      this.db.getClienteEspecifico(this.cod).then((data) => {
        this.cliente = data;
        console.log("Cliente enviado a la facturacion", this.cliente);
        this.tipo = this.cliente.tipo;
        this.razonSocial = this.cliente.razon;
        this.name = this.cliente.nombre;
        this.descuento = this.cliente.descuento;
        this.precio = this.cliente.lprecioc;
        this.GLN = this.cliente.GLN;
        this.EAN = this.cliente.EAN;
        this.credito = this.cliente.credito;
        this.Activo = this.cliente.Activo;
        this.Comentario = this.cliente.Comentario;
        this.nombredoc = this.cliente.nombredoc;
        this.g = this.cliente.g;
        console.log("contado o credito", this.credito);
        console.log("Activoo", this.Activo);

        this.navCtrl.push(FacturacionPage, {
          GLN: this.GLN,
          EAN: this.EAN,
          id: this.idRuta,
          serie: this.serieTanque,
          codigo: this.scannedCode,
          cod: this.cod,
          name: this.name,
          razon: this.razonSocial,
          tipoPago: this.tipoPago,
          descuento: this.descuento,
          tipo: this.tipo,
          precioKilos: this.precio,
          credito: this.credito,
          Activo: this.Activo,
          Comentario: this.Comentario,
          g: this.g,
          nombredoc: this.nombredoc
        });

      }, (error) => {
        console.log(error);
      })

      //this.navCtrl.push(FacturacionPage, {codigo:this.scannedCode, cod: this.cod,name: this.name, razon: this.razonSocial, tipoPago: this.tipoPago, descuento: this.descuento,tipo:this.tipo});
    })
    console.log(this.scannedCode);
  }


  escanearCodigo() {
    this.barcodeScanner.scan().then(barcodeData => {
      this.scannedCode = barcodeData.text;
      this.client = JSON.parse(this.scannedCode);
      this.cod = this.client.codigo;
      this.serieTanque = this.client.serie;
      this.db.getClienteEspecifico(this.cod).then((data) => {
        this.cliente = data;

        this.navCtrl.push(FacturacionPage, this.cliente);

      }, (error) => {
        console.log(error);
      })

    })
    console.log(this.scannedCode);
  }


  ionViewDidLoad() {
    //  this.envioAutomatico();
  }


  // envio(){
  // setInterval( () => { this.empezarContador(); }, 2000);
  // }

  // empezarContador(){

  // let conexion = this.network.onchange().subscribe(() => {


  //   if(this.network.type == '4g' || this.network.type == '3g' || this.network){


  //   console.log('Se enviaron datos');

  ///}else{

  //console.log('No se envio nada');
  //}

  //});

  //conexion.unsubscribe();
  //}

  // envioAutomatico(){
  //   let hayConexion = this.network.onConnect().subscribe(() => {

  //     setTimeout(() => {
  //       // if(this.network.type == 'wifi'){
  //       //   this.toast.create({
  //       //     message: 'Conexion Wifi',
  //       //     duration: 3000
  //       //   }).present();
  //       // }



  //       if(this.network.type == '4g' || this.network.type == '3g' || this.network.type == 'wifi'){
  //         // this.toast.create({
  //         //   message: 'Conexion 4g O 3g',
  //         //   duration: 3000
  //         // }).present();

  //         console.log('Hay conexion');
  //         this.enviarFacturasRestantes();
  //         this.enviarDetallesFaltantes();
  //         //this.actualizarRuta();

  //         this.obtenerMotivosNoCompra(); //ver si se envian automaticamente los motivos de no compra
  //         this.obtenerClaves();

  //         // console.log("Me acabo de ejecutar");

  //         // setInterval( () => { this.envioAutomatico(); }, 4000);

  //       }

  //     },1000)
  //   });
  // }

  obtenerMotivosNoCompra() {
    this.db.getMotivosSQLiteNoEnviados().then((data) => {
      this.motivoNoCompra = data;
      console.log(this.motivoNoCompra);

      for (var index in this.motivoNoCompra) {

        var motivo = {
          codigoCliente: this.motivoNoCompra[index].codigoCliente,
          fecha: this.motivoNoCompra[index].fecha,
          ruta: this.motivoNoCompra[index].ruta,
          motivo: this.motivoNoCompra[index].motivo,
          fechaRuta: this.motivoNoCompra[index].fechaRuta
        }


        // this.info.enviarMotivosNoCompraAutomaticamente(motivo);

        this.actualizarEnviadoMotivo(motivo.codigoCliente);

        //this.eliminarMotivoNoCompra(motivo.codigoCliente);


      }
    }).catch(error => { console.error(error) })
  }

  obtenerClaves() {
    this.db.getClavesSQLite().then((data) => {
      this.clave = data;
      console.log(this.clave);

      for (var index in this.clave) {

        var claveF = {
          clave: this.clave[index].clave,
          consecutivo: this.clave[index].consecutivo,
          facnumero: this.clave[index].facnumero,
          fecha: this.clave[index].fecha
        }


        //  this.info.enviarClavesAutomaticamente(claveF);

      }
    })
  }

  actualizarEnviadoMotivo(codigo: any) {
    this.db.actualizarEnviadoMotivo({
      codigoCliente: codigo,
      enviado: 1
    }).then((data) => {
      console.log(data);
    }).catch(error => { console.error(error) });

  }

  //metodo que borra los clientes almacenados en SQLite
  eliminarMotivoNoCompra(codigo: any) {
    this.db.eliminaMotivosNoCompraSQLiteCodigo(codigo).then((data) => {
      console.log(data);
      this.actualizarEnviadoMotivo(codigo);

    }, (error) => {
      console.log(error)
    })
  }

  //metodo que obtiene la ruta en uso para utilizar sus datos en la actualizacion del consecutivo
  //y armar el TOM para cada factura
  obtenerRutaEnUso() {
    this.db.getRutaUsada(this.idRuta).then((data) => {
      this.rutaEnUso = data;
      console.log(this.rutaEnUso);
      this.codRuta = this.rutaEnUso.id;
      this.nombreRuta = this.rutaEnUso.nombre;
      this.unidadNegocioRuta = this.rutaEnUso.unidad_negocio;
      this.cediRuta = this.rutaEnUso.cedi;
      this.descripcionRuta = this.rutaEnUso.descripcion;
      this.consecutivoRuta = this.rutaEnUso.consecutivo;
      this.cerosTOM = this.rutaEnUso.cerosTOM;

    }, (error) => {
      console.log(error);
    })
  }

  //metodo que actualiza la ruta en la base de datos para que al siguiente dia se utilice el ultimo consecutivo
  actualizarRuta() {
    this.info.actualizarRutaConConsecutivo({
      id: this.codRuta,
      nombre: this.nombreRuta,
      unidad_negocio: this.unidadNegocioRuta,
      cedi: this.cediRuta,
      descripcion: this.descripcionRuta,
      cerosTOM: this.cerosTOM,
      consecutivo: this.consecutivoRuta
    })
      .subscribe(data => {
        console.log(data)
      });
  }

  //metodo que obtiene las rutas almacenadas en SQLite utilizando el metodo de DatabaseProvider
  obtenerRutasDeSQLite() {
    this.db.getRutasSQLite().then((data) => {

      console.log(data);
    }, (error) => {
      console.log(error);
    })
  }

  //metodo que borra los clientes almacenados en SQLite
  eliminarClientes() {
    this.db.eliminarClientesSQLite().then((data) => {
      console.log(data);
    }, (error) => {
      console.log(error)
    })
  }

  //metodo que borra los productos almacenados en SQlite
  eliminarProductos() {
    this.db.eliminarProductosSQLite().then((data) => {
      console.log(data);
    }, (error) => {
      console.log(error)
    })
  }

  //metodo que borra las rutas almacenadas en SQLite
  eliminarRutas() {
    this.db.eliminarRutasSQLite().then((data) => {
      console.log(data);
    }, (error) => {
      console.log(error)
    })
  }

  //metodo para ir a la vista de facturacio y listar los clientes
  // irAClientes(){
  //   this.navCtrl.push(ClientesPage);
  // }

  irAFacturas() {
    this.navCtrl.push(FacturasPage);
  }

  salir() {
    //this.actualizarRuta();
    this.eliminarClientes();
    //this.eliminarProductos();
    //this.eliminarRutas();
  }

  //alerta que aparece cuando tienen que verificar si lleno la informacion
  mensajeDeSeguridad() {
    let alert = this.aCtrl.create({
      title: 'Se va eliminar los clientes de la ruta y necesita señal internet para volver a cargarlos',
      subTitle: '¿Seguro que desea salir?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');

          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            console.log('Confirm Okay');
            this.actualizarRuta();
            this.eliminarClientes();
            this.eliminarProductos();
            // this.eliminarRutas();
            // setTimeout( () => {
            //    this.platform.exitApp();
            // }, 1000);

            this.contador = 1;
          }
        }
      ]
    });
    alert.present();
  }

  //alerta que aparece cuando tienen que verificar si lleno la informacion
  mensajeDeSeguridad2() {
    let alert = this.aCtrl.create({
      title: 'Se van a cargar los clientes de la ruta y necesita señal internet para hacerlo',
      subTitle: '¿Seguro que desea volver a cargarlos?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
            this.contador = 0;
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            console.log('Confirm Okay');
            this.getClientesRuta();
            this.getProductos();
            // this.eliminarRutas();
            // setTimeout( () => {
            //    this.platform.exitApp();
            // }, 1000);

            this.contador = 0;
          }
        }
      ]
    });
    alert.present();
  }

  mensajeDeSeguridad3() {
    let alert = this.aCtrl.create({
      title: 'Se van a eliminar los clientes de la ruta y necesita señal internet para hacerlo',
      subTitle: '¿Seguro que desea volver a cargarlos?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
            this.contador = 0;
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            console.log('Confirm Okay');
            this.actualizarRuta();
            this.eliminarClientes();
            // this.eliminarRutas();
            setTimeout(() => {
              this.platform.exitApp();
            }, 1000);


          }
        }
      ]
    });
    alert.present();
  }

  irACierre() {
    this.navCtrl.push(CierrePage, { id: this.idRuta });
  }

  IrABilletes() {
    this.navCtrl.push(BilletesPage, { id: this.idRuta });
  }
  // IrABanco(){
  //   this.navCtrl.push(BancoPage,{id:this.idRuta});
  // }
  IrAGestiones() {
    this.navCtrl.push(GestionesPage, { id: this.idRuta });
  }

  IrAFacturas() {
    this.navCtrl.push(FacturasPage);
  }

  irAClientesNuevos() {
    this.navCtrl.push(ClientesNuevosPage, { id: this.idRuta });
  }

  irADescargas() {
    this.navCtrl.push(DescargasDirectasPage, { id: this.idRuta });
  }

  //metodo que obtiene la factura especifica para actualizarla
  obtenerFactura(TOM: any) {
    this.db.getFacturaPorTOM(parseInt(TOM)).then((data) => {
      this.facturaAnular = data;
      this.TOMFactura = this.facturaAnular.facnumero;
      this.rutaFactura = this.facturaAnular.ruta;
      this.nombreFactura = this.facturaAnular.nombre;
      this.fechaFactura = this.facturaAnular.fechaFactura;

    }, (error) => {
      console.log(error);
    })
  }

  //metodo que obtiene los detalle especifico de una factura para enviarlo y actualizar el dato enviada
  obtenerDetalle(TOM: any) {
    this.db.getDetallePorTOM(parseInt(TOM)).then((data) => {
      this.detalleFactura = data;
      this.TOMFactura = this.detalleFactura.facnumero;
      this.enviadaDetalle = this.detalleFactura.enviada;

      //llamo al metodo enviar factura
      this.enviarDetalle(this.detalleFactura);
    })
  }

  // obtenerClientes(){
  //   this.db.getCientesSQLite().then((data) => {
  //     this.clientes = data;


  //   })
  // }

  //metodo que actualiza el campo enviado para cambiar el color en la interfaz
  actualizarEnviado(TOM: any) {
    this.db.actualizarEnviadoFactura({
      facnumero: TOM,
      enviada: 1
    }).then((data) => {
      console.log(data);
    }).catch(error => { console.error(error) });


  }

  actualizarEnviadoDetalle(TOM: any) {
    this.db.actualizarEnviadoDetalle({
      facnumero: TOM,
      enviada: 1
    }).then((data) => {
      console.log(data);
    }).catch(error => { console.error(error) });

  }

  //metodo que envia la factura a la base de datos de TOMZA por medio del Api
  //se ocupa tener internet para enviarla
  enviarFactura(factura) {


    var fac = {
      codigo: factura.codigo,
      facnumero: factura.facnumero,
      razon_social: factura.razon_social,
      nombre: factura.nombre,
      descuento: factura.descuento,
      descuentot: factura.descuentot,
      credito: factura.credito,
      total: factura.total,
      ruta: factura.ruta,
      fecha: factura.fecha,
      q25: factura.q25,
      q25r: factura.q25r,
      q100: factura.q100,
      q20: factura.q20,
      q20r: factura.q20r,
      q10: factura.q10,
      q10r: factura.q10r,
      q35: factura.q35,
      q35r: factura.q35r,
      q45: factura.q45,
      q45r: factura.q45r,
      q60: factura.q60,
      q60r: factura.q60r,
      qlts: factura.qtls,
      qkgs: factura.qkgs,
      LAT: factura.LAT,
      LONGI: factura.LONGI,
      g: factura.g,
      efectivo: factura.efectivo,
      transmonto: factura.transmonto,
      transnumero: factura.transnumero,
      chknumero: factura.chknumero,
      chkbanco: factura.chkbanco,
      chkmonto: factura.chkmonto,
      tipo: factura.tipo,
      cz25: factura.cz25,
      cz25r: factura.cz25r,
      cz100r: factura.cz100r,
      fechadt: factura.fechadt
    }

    //console.log(fac);

    if (factura.nombre !== '') {
      this.info.enviarFaturas(fac).then(data => {
        console.log('Enviado con Exito');
        console.log(data);
        this.actualizarEnviado(fac.facnumero);

      }, err => {

        console.log(err);
        console.log("Error de envio sin internet");
      })
    }

    // this.info.enviarFaturas(fac);

    // this.actualizarEnviado(fac.facnumero);
  }

  //metodo que envia el detalle de la factura a la base de datos de  TOMZA por medio del API
  //se ocupa tener internet para enviarla
  enviarDetalle(detalle) {

    var detail = {
      idcliente: detalle.idcliente,
      ruta: detalle.ruta,
      facnumero: detalle.facnumero,
      totalexenta: detalle.totalexenta,
      total: detalle.total,
      fechacreacion: detalle.fechacreacion,
      detalle: detalle.detalle,
      rawdetalle: detalle.rawdetalle,
      detail: detalle.detail,
      corre: detalle.corre,
      estado: detalle.estado,
    }

    this.info.enviarDetalles(detail);
  }

  enviarFacturasRestantes() {

    this.db.getFacturasNoEnviadasSQLite().then((data) => {
      this.factura = data;
      console.log(this.factura);

      for (var index in this.factura) {


        var fac = {
          codigo: this.factura[index].codigo,
          facnumero: this.factura[index].facnumero,
          razon_social: this.factura[index].razon_social,
          nombre: this.factura[index].nombre,
          descuento: this.factura[index].descuento,
          descuentot: this.factura[index].descuentot,
          credito: this.factura[index].credito,
          total: this.factura[index].total,
          ruta: this.factura[index].ruta,
          fecha: this.factura[index].fecha,
          q25: this.factura[index].q25,
          q25r: this.factura[index].q25r,
          q100: this.factura[index].q100,
          q20: this.factura[index].q20,
          q10: this.factura[index].q10,
          q10r: this.factura[index].q10r,
          q35: this.factura[index].q35,
          q35r: this.factura[index].q35r,
          q45: this.factura[index].q45,
          q45r: this.factura[index].q45r,
          q60: this.factura[index].q60,
          q60r: this.factura[index].q60r,
          qlts: this.factura[index].qtls,
          qkgs: this.factura[index].qkgs,
          LAT: this.factura[index].LAT,
          LONGI: this.factura[index].LONGI,
          g: this.factura[index].g,
          efectivo: this.factura[index].efectivo,
          transmonto: this.factura[index].transmonto,
          transnumero: this.factura[index].transnumero,
          chknumero: this.factura[index].chknumero,
          chkbanco: this.factura[index].chkbanco,
          chkmonto: this.factura[index].chkmonto,
          tipo: this.factura[index].tipo,
          cz25: this.factura[index].cz25,
          cz25r: this.factura[index].cz25r,
          cz100r: this.factura[index].cz100r,
          fechadt: this.factura[index].fechadt
        }

        //  this.info.enviarFaturasAutomaticamente(fac).then(data => {
        //   console.log('Enviado con Exito');
        //   console.log(data);
        //   this.actualizarEnviado(fac.facnumero);

        // }, err => {

        //  console.log(err);
        //  console.log("Error de envio sin internet");
        // });

        //this.actualizarEnviado(fac.facnumero);



      }
    }).catch(error => { console.error(error) })
  }

  //metodo que envia los detalles que no se habian enviado a la base de datos
  enviarDetallesFaltantes() {

    this.db.getDetallesNoEnviadosSQLite().then((data) => {
      this.detalle = data;
      console.log(this.detalle);

      for (var index in this.detalle) {

        var detail = {
          idcliente: this.detalle[index].idcliente,
          ruta: this.detalle[index].ruta,
          facnumero: this.detalle[index].facnumero,
          totalexenta: this.detalle[index].totalexenta,
          total: this.detalle[index].total,
          fechacreacion: this.detalle[index].fechacreacion,
          detalle: this.detalle[index].detalle,
          rawdetalle: this.detalle[index].rawdetalle,
          detail: this.detalle[index].detail,
          corre: this.detalle[index].corre,
          estado: this.detalle[index].estado,
        }

        // this.info.enviarDetallesAutomaticamente(detail);

        //this.actualizarEnviadoDetalle(detail.facnumero);
      }
    }).catch(error => { console.error(error) })
  }


  //metodo que obtiene los clientes del api filtrando por ruta para posteriormente guardarlos en SQLite
  // getClientesRuta(){
  //   let data:Observable<any> =  this.http.get(`${this.apiURL}clientes?filter[where][ruta]=${this.idRuta}&filter[where][tipo][neq]=500`);

  //   let allClientesLoadingController = this.lCtrl.create({
  //     content: "Insertando clientes y productos...Espere"
  //   });

  //   allClientesLoadingController.present();

  //   data.subscribe(result => {
  //     this.clientesAPI = result;
  //     // console.log('***************');
  //     // console.log(this.clientesAPI);
  //     // console.log('***************');

  //        //recorro las rutas que obtuve del api
  //         for(var index in this.clientesAPI){
  //           //armo cada cliente para despues almacenarlos
  //           let cliente = {
  //             codigo: this.clientesAPI[index].codigo,
  //             nombre: this.clientesAPI[index].nombre,
  //             GLN: this.clientesAPI[index].GLN,
  //             EAN: this.clientesAPI[index].EAN,
  //             razon: this.clientesAPI[index].razon,
  //             descuento: this.clientesAPI[index].descuento,
  //             precio: this.clientesAPI[index].precio,
  //             tipo: this.clientesAPI[index].tipo,
  //             ruta: this.clientesAPI[index].ruta,
  //             subcanal: this.clientesAPI[index].subcanal,
  //             formadepago: this.clientesAPI[index].formadepago,
  //             credito: this.clientesAPI[index].credito,
  //             LAT: this.clientesAPI[index].LAT,
  //             LONGI: this.clientesAPI[index].LONGI,
  //             puedeFacturar: this.clientesAPI[index].puedeFacturar,
  //             lprecioa: this.clientesAPI[index].lprecioa,
  //             lprecioc: this.clientesAPI[index].lprecioc,
  //             ldescuento: this.clientesAPI[index].ldescuento,
  //             especial: this.clientesAPI[index].especial,
  //             L: this.clientesAPI[index].L,
  //             K: this.clientesAPI[index].K,
  //             M: this.clientesAPI[index].M,
  //             J: this.clientesAPI[index].J,
  //             V: this.clientesAPI[index].V,
  //             S: this.clientesAPI[index].S,
  //             Do: this.clientesAPI[index].Do,
  //             correo: this.clientesAPI[index].correo,
  //             tipoCilza: this.clientesAPI[index].tipoCilza,
  //             hizoMotivo: 0,
  //             hizoFactura:0
  //           }

  //           console.log("Clientes",cliente);
  //           //guardamos los objetos cliente en la BD SQLite
  //           this.db.guardarClientesSQLite(cliente);
  //         }
  //         allClientesLoadingController.dismiss();
  //     return result;
  //   })
  // }

  //metodo que obtiene los productos del api para posteriormente guardarlos en SQLite
  getProductos() {
    let data: Observable<any> = this.http.get(`${this.apiURL}productos`);

    data.subscribe(result => {
      this.productosAPI = result;
      //console.log(this.productosAPI);

      //recorro los productos que obtuve del api
      for (var index in this.productosAPI) {
        //armo cada cliente para despues almacenarlos
        let producto = {
          id: this.productosAPI[index].id,
          producto: this.productosAPI[index].producto,
          tipoPrecio: this.productosAPI[index].tipoPrecio,
          precio: this.productosAPI[index].precio,
          tipoCliente: this.productosAPI[index].tipoCliente,
          descripcion: this.productosAPI[index].descripcion,
          precioViejo: this.productosAPI[index].precioViejo
        }

        this.db.guardarProductosSQLite(producto);
      }

      return result;

    })
  }

  //metodo que obtiene los clientes del api filtrando por ruta para posteriormente guardarlos en SQLite
  getClientesRuta() {
    let data: Observable<any> = this.http.get(`${this.apiURL}clientes?filter[where][ruta]=${this.idRuta}&filter[where][tipo][neq]=500`);

    let allClientesLoadingController = this.lCtrl.create({
      content: "Insertando clientes y productos...Espere"
    });

    allClientesLoadingController.present();

    data.subscribe(result => {
      this.clientesAPI = result;
      // console.log('***************');
      // console.log(this.clientesAPI);
      // console.log('***************');

      //recorro las rutas que obtuve del api
      for (var index in this.clientesAPI) {
        //armo cada cliente para despues almacenarlos
        let cliente = {
          codigo: this.clientesAPI[index].codigo,
          nombre: this.clientesAPI[index].nombre,
          GLN: this.clientesAPI[index].GLN,
          EAN: this.clientesAPI[index].EAN,
          razon: this.clientesAPI[index].razon,
          descuento: this.clientesAPI[index].descuento,
          precio: this.clientesAPI[index].precio,
          tipo: this.clientesAPI[index].tipo,
          ruta: this.clientesAPI[index].ruta,
          subcanal: this.clientesAPI[index].subcanal,
          formadepago: this.clientesAPI[index].formadepago,
          credito: this.clientesAPI[index].credito,
          LAT: this.clientesAPI[index].LAT,
          LONGI: this.clientesAPI[index].LONGI,
          puedeFacturar: this.clientesAPI[index].puedeFacturar,
          lprecioa: this.clientesAPI[index].lprecioa,
          lprecioc: this.clientesAPI[index].lprecioc,
          ldescuento: this.clientesAPI[index].ldescuento,
          especial: this.clientesAPI[index].especial,
          L: this.clientesAPI[index].L,
          K: this.clientesAPI[index].K,
          M: this.clientesAPI[index].M,
          J: this.clientesAPI[index].J,
          V: this.clientesAPI[index].V,
          S: this.clientesAPI[index].S,
          Do: this.clientesAPI[index].Do,
          correo: this.clientesAPI[index].correo,
          tipoCilza: this.clientesAPI[index].tipoCilza,
          nombredoc: this.clientesAPI[index].nombredoc,
          g: this.clientesAPI[index].g,
          hizoMotivo: 0,
          hizoFactura: 0,
          tipoVisita: this.clientesAPI[index].tipoVisita,
          telefonoFijo: this.clientesAPI[index].telefonoFijo,
          telefonoOpcional: this.clientesAPI[index].telefonoOpcional,
          tipodoc: this.clientesAPI[index].tipodoc,
          documento: this.clientesAPI[index].documento,
          Activo: this.clientesAPI[index].Activo,
          Comentario: this.clientesAPI[index].Comentario
        }

        console.log("Clientes menu granel", cliente);
        //guardamos los objetos cliente en la BD SQLite
        this.db.guardarClientesSQLite(cliente);
      }
      allClientesLoadingController.dismiss();
      return result;
    })
  }

}
