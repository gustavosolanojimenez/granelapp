import { ClientesNuevoTanquePage } from './../clientes-nuevo-tanque/clientes-nuevo-tanque';
import { Component } from '@angular/core';
import {  NavController, NavParams, Platform, AlertController } from 'ionic-angular';
import { NuevoMantenimientoPage } from '../nuevo-mantenimiento/nuevo-mantenimiento';
import { RegistroTanquePage } from '../registro-tanque/registro-tanque';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { MantenimientosPage } from '../mantenimientos/mantenimientos';
import { TanquesPage } from '../tanques/tanques';
import { ClientesMantePage } from '../clientes-mante/clientes-mante';
import { SqliteDbProvider } from '../../providers/sqlite-db/sqlite-db';
import { InformacionApiProvider } from '../../providers/informacion-api/informacion-api';



/**
 * Generated class for the MenuMantenimientoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-menu-mantenimiento',
  templateUrl: 'menu-mantenimiento.html',
})
export class MenuMantenimientoPage {

  idRuta:any
  qrData = null;
  createdCode = null;
  scannedCode = null;
  myPhoto: any;
  tanques = [];
  mantenimientos = [];

  cod; name; razonSocial; tipoPago; client; descuento; tipo; ruta; numserie;

  constructor(public navCtrl: NavController, public navParams: NavParams,public barcodeScanner: BarcodeScanner,public platform: Platform, public aCtrl: AlertController, public db: SqliteDbProvider,public info: InformacionApiProvider) {

    platform.registerBackButtonAction(() => {

    },1);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuMantenimientoPage');
  }

  irANuevoMantenimiento(){
    this.navCtrl.push(NuevoMantenimientoPage);
  }

  irANuevoTanque(){
    this.navCtrl.push(RegistroTanquePage);
  }

  irMantenimientos(){
    this.navCtrl.push(MantenimientosPage);
  }
  irTanques(){
    this.navCtrl.push(TanquesPage);
  }

  irAclientesMante(){
    this.navCtrl.push(ClientesMantePage);
  }



  escanearCodigo(){
    this.barcodeScanner.scan().then(barcodeData => {
      this.scannedCode = barcodeData.text;

      this.client = JSON.parse(this.scannedCode);
      this.cod = this.client.codigo;
      this.name = this.client.nombre;
      this.razonSocial = this.client.razon;
      this.tipoPago = this.client.credito;
      this.descuento = this.client.descuento;
      this.tipo = this.client.tipo;
      this.ruta =  this.client.ruta;

      this.navCtrl.push(RegistroTanquePage, {codigo:this.scannedCode, cod: this.cod,name: this.name, razon: this.razonSocial, tipoPago: this.tipoPago, descuento: this.descuento,tipo:this.descuento, ruta:this.ruta});
    })
    console.log(this.scannedCode);
  }

  escanearCodigoMantenimiento(){
    this.barcodeScanner.scan().then(barcodeData => {
      this.scannedCode = barcodeData.text;

      this.client = JSON.parse(this.scannedCode);
      this.cod = this.client.codigo;
      this.name = this.client.nombre;
      this.razonSocial = this.client.razon;
      this.tipoPago = this.client.credito;
      this.descuento = this.client.descuento;
      this.tipo = this.client.tipo;
      this.ruta =  this.client.ruta;
      this.numserie =  this.client.serie

      this.navCtrl.push(NuevoMantenimientoPage, {codigo:this.scannedCode,serie: this.numserie});
    })

    console.log(this.scannedCode);
  }


  irRegistroTanque(){
    this.navCtrl.push(ClientesNuevoTanquePage);
  }

   //alerta que aparece cuando tienen que verificar si lleno la informacion
   mensajeDeSeguridad() {
    let alert = this.aCtrl.create({
      title: 'Se va a enviar la informacion recolectada durante el dia y se procede a limpiar el telefono excepto los mantenimientos no terminados.',
      subTitle: 'Si esta seguro presione aceptar.',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        },
         {
          text: 'Aceptar',
          handler: () => {
            console.log('Confirm Okay');
            this.limpiaTelefono();
            setTimeout( () => {
              this.platform.exitApp();
            }, 2000);
          }
        }
      ],
      enableBackdropDismiss : false
    });
    alert.present();
  }

  mensajeDeSeguridad2() {
    let alert = this.aCtrl.create({
      title: 'Se va a eliminar los clientes para poder cambiar de ruta',
      subTitle: 'Si esta seguro presione aceptar.',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        },
         {
          text: 'Aceptar',
          handler: () => {
            console.log('Confirm Okay');
            this.eliminarClientes();
            setTimeout( () => {
              this.platform.exitApp();
            }, 2000);
          }
        }
      ],
      enableBackdropDismiss : false
    });
    alert.present();
  }

  envios(){
    this.enviarMantenimientosTerminaods();
    this.enviarTanques();
  }

  //metodo para enviar los tanques
  enviarTanques(){
    this.db.obtenerTanques().then((data) => {
      this.tanques = data;
      console.log(this.tanques);

      for(var index in this.tanques){

        let tanque = {
          numSerie: this.tanques[index].numserie,
          capacidadLitros: this.tanques[index].capacidadlitros,
          capacidadGalones: this.tanques[index].capacidadgalones,
          fechaFabricacion: this.tanques[index].fechafabricacion,
          fabricante: this.tanques[index].fabricante,
          codigoCliente: this.tanques[index].codcliente,
          numContrato: this.tanques[index].numcontrato,
          ruta: this.tanques[index].ruta,
          propietario:this.tanques[index].propietario,
          nombreNegocio: this.tanques[index].nombreNegocio,
          contratoActivo: this.tanques[index].contratoActivo,
          estado: this.tanques[index].estado,
          Latitud: this.tanques[index].latitud,
          Longitud:this.tanques[index].longitud,
          provincia: this.tanques[index].provincia,
          canton: this.tanques[index].canton,
          distrito: this.tanques[index].distrito,
          marcaTanque: this.tanques[index].marcaTanque,
          posicionTanque: this.tanques[index].posicionTanque
        }

        console.log(tanque);
        this.info.enviarTanques(tanque);
      }


    }).catch( error => { console.error(error) })
  }

  enviarMantenimientosTerminaods(){
    this.db.getMantenimientosTerminados().then((data) => {
      this.mantenimientos = data;
      console.log(this.mantenimientos);

      for(var index in this.mantenimientos){

        let mantenimiento = {
          horaInicio: this.mantenimientos[index].horaInicio,
          fechaVisita: this.mantenimientos[index].fechavisita,
          observaciones: this.mantenimientos[index].observaciones,
          serieTanque:this.mantenimientos[index].serieTanque,
          foto1: this.mantenimientos[index].foto1,
          foto2:this.mantenimientos[index].foto2,
          trabajoRealizado: this.mantenimientos[index].trabajorealizado,
          fechaValvula: this.mantenimientos[index].fechavalvula,
          gasInstalacion: this.mantenimientos[index].porcentajeGasInsta,
          gasDesinstalacion: this.mantenimientos[index].porcentajeGasDesin,
          terminado:  this.mantenimientos[index].terminado,
          horaFin: this.mantenimientos[index].horaFin
        }

        console.log(mantenimiento);
        this.info.enviarMantenimientos(mantenimiento);
      }


    }).catch( error => { console.error(error) })
  }

  eliminarMantenimientos(){
    this.db.eliminarMantenimientosTerminados().then((data)=>{
      console.log(data);
    },(error) => {
      console.log(error);
    })
  }

  eliminarTanques(){
    this.db.eliminarTanquesIngresados().then((data)=>{
      console.log(data);
    },(error) => {
      console.log(error);
    })
  }

  eliminarClientes(){
    this.db.eliminarClientesSQLite().then((data) => {
      console.log(data);
    }, (error) => {
      console.log(error);
    })
  }

  limpiaTelefono(){
    this.eliminarMantenimientos();
    this.eliminarTanques();
    this.eliminarClientes();
  }

}
