import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ToastController, Platform } from 'ionic-angular';
import { HomePage } from '../home/home';
import { MenuGranelPage } from '../menu-granel/menu-granel';
import { MenuMantenimientoPage } from '../menu-mantenimiento/menu-mantenimiento';
import { SqliteDbProvider } from '../../providers/sqlite-db/sqlite-db';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { InformacionApiProvider } from '../../providers/informacion-api/informacion-api';
import { InicioMantenimientoPage } from '../inicio-mantenimiento/inicio-mantenimiento';
import { enviroment } from '../../env/environment';

/**
 * Generated class for the InicioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-inicio',
  templateUrl: 'inicio.html',
})
export class InicioPage {
  apiURL= enviroment.url;
  login = 0;
  //apiURL = 'http://174.138.38.52:3030/api/';
   //apiURL = 'https://api-tomza.herokuapp.com/api/';
  //variables para almacenar los datos que vienen del api
  rutasAPI:any = [];
  clientesAPI:any = [];
  productosAPI:any = [];
  //otras variables
  rutasString: String;
  cedi:string;
  idRuta:string;
  rutaCargada:any;
  primerRuta:any;


  rutasCedi:any = [];
  productosSQLite:any = [];
  clientesSQLite:any = [];
  rutasSQLite:any = [];


  codRuta;
  nombreRuta;
  unidadNegocioRuta;
  cediRuta;
  descripcionRuta;
  cerosTOM;
  consecutivoRuta;
  toastController: any;


  constructor(public navCtrl: NavController,public platform:Platform, public toast: ToastController,public aCtrl: AlertController,public http: HttpClient, public navParams: NavParams, public db: SqliteDbProvider, public lCtrl: LoadingController, public info: InformacionApiProvider) {

    this.getProductos();
   
  
  }


  volver(){
    this.login = 0;
  }

  irAMenuGranel(){
    //this.login = 1;
    //this.navCtrl.push(HomePage);
  }

  irAMenuMantenimiento(){
    this.navCtrl.push(MenuMantenimientoPage);
  }

  IrAMenu(){
    this.navCtrl.push(MenuGranelPage,{id:this.idRuta});
  }

  irAInicioMantenimiento(){
    this.navCtrl.push(InicioMantenimientoPage);
  }

  obtenerRutaCedi(){

    let allRutasLoadingController = this.lCtrl.create({
      content: "Obteniendo Rutas Desde la Base de datos"
    });

    allRutasLoadingController.present();
    console.log("Este es el cedi",this.cedi);
    this.db.getRutasSQLiteCedi(this.cedi).then((data) => {
      allRutasLoadingController.dismiss();
      this.rutasAPI= data;
      console.log(this.rutasAPI);

    },(error) =>{
      console.log(error);
    })
  }

  validacion(){

    if(this.clientesSQLite.length === 0 ){
      this.getClientesGranel()
    }else{
      console.log("ya los clientes han sido ingresados");
    }
  }

  //metodo que obtiene los clientes del api filtrando por ruta para posteriormente guardarlos en SQLite
  getClientesRuta(){
    let data:Observable<any> =  this.http.get(`${this.apiURL}clientes?filter[where][ruta]=${this.idRuta}&filter[where][tipo][neq]=500`);

    let allClientesLoadingController = this.lCtrl.create({
      content: "Insertando clientes y productos... Espere"
    });

    allClientesLoadingController.present();

    data.subscribe(result => {
      this.clientesAPI = result;
      // console.log('***************');
       console.log("Clientes api inicio",this.clientesAPI);
      // console.log('***************');

         //recorro las rutas que obtuve del api
          for(var index in this.clientesAPI){
            //armo cada cliente para despues almacenarlos
            let cliente = {
              codigo: this.clientesAPI[index].codigo,
              nombre: this.clientesAPI[index].nombre,
              GLN: this.clientesAPI[index].GLN,
              EAN: this.clientesAPI[index].EAN,
              razon: this.clientesAPI[index].razon,
              descuento: this.clientesAPI[index].descuento,
              precio: this.clientesAPI[index].precio,
              tipo: this.clientesAPI[index].tipo,
              ruta: this.clientesAPI[index].ruta,
              subcanal: this.clientesAPI[index].subcanal,
              formadepago: this.clientesAPI[index].formadepago,
              credito: this.clientesAPI[index].credito,
              LAT: this.clientesAPI[index].LAT,
              LONGI: this.clientesAPI[index].LONGI,
              puedeFacturar: this.clientesAPI[index].puedeFacturar,
              lprecioa: this.clientesAPI[index].lprecioa,
              lprecioc: this.clientesAPI[index].lprecioc,
              ldescuento: this.clientesAPI[index].ldescuento,
              especial: this.clientesAPI[index].especial,
              L: this.clientesAPI[index].L,
              K: this.clientesAPI[index].K,
              M: this.clientesAPI[index].M,
              J: this.clientesAPI[index].J,
              V: this.clientesAPI[index].V,
              S: this.clientesAPI[index].S,
              Do: this.clientesAPI[index].Do,
              correo: this.clientesAPI[index].correo,
              tipoCilza: this.clientesAPI[index].tipoCilza,
              nombredoc: this.clientesAPI[index].nombredoc,
              g: this.clientesAPI[index].g,
              hizoMotivo: 0,
              hizoFactura: 0,
              tipoVisita: this.clientesAPI[index].tipoVisita,
              telefonoFijo: this.clientesAPI[index].telefonoFijo,
              telefonoOpcional: this.clientesAPI[index].telefonoOpcional,
              tipodoc: this.clientesAPI[index].tipodoc,
              documento: this.clientesAPI[index].documento,
              Activo: this.clientesAPI[index].Activo,
              Comentario: this.clientesAPI[index].Comentario
            }

            console.log("Clientes por ruta",cliente);
            //guardamos los objetos cliente en la BD SQLite
            this.db.guardarClientesSQLite(cliente);
          }
          allClientesLoadingController.dismiss();
      return result;
    })
  }


  //metodo que obtiene los clientes del api filtrando por ruta para posteriormente guardarlos en SQLite
  getClientesGranel(){
    let data:Observable<any> =  this.http.get(`${this.apiURL}clientes?filter[where][tipo][inq]=6&filter[where][tipo][inq]=7`);

    let allClientesLoadingController = this.lCtrl.create({
      content: "Insertando clientes...Espere"
    });

    allClientesLoadingController.present();

    data.subscribe(result => {
      this.clientesAPI = result;
      // console.log('***************');
      console.log(this.clientesAPI);
      // console.log('***************');

         //recorro las rutas que obtuve del api
          for(var index in this.clientesAPI){
            //armo cada cliente para despues almacenarlos
            let cliente = {
              codigo: this.clientesAPI[index].codigo,
              nombre: this.clientesAPI[index].nombre,
              GLN: this.clientesAPI[index].GLN,
              EAN: this.clientesAPI[index].EAN,
              razon: this.clientesAPI[index].razon,
              descuento: this.clientesAPI[index].descuento,
              precio: this.clientesAPI[index].precio,
              tipo: this.clientesAPI[index].tipo,
              ruta: this.clientesAPI[index].ruta,
              subcanal: this.clientesAPI[index].subcanal,
              formadepago: this.clientesAPI[index].formadepago,
              credito: this.clientesAPI[index].credito,
              LAT: this.clientesAPI[index].LAT,
              LONGI: this.clientesAPI[index].LONGI,
              puedeFacturar: this.clientesAPI[index].puedeFacturar,
              lprecioa: this.clientesAPI[index].lprecioa,
              lprecioc: this.clientesAPI[index].lprecioc,
              ldescuento: this.clientesAPI[index].ldescuento,
              especial: this.clientesAPI[index].especial,
              L: this.clientesAPI[index].L,
              K: this.clientesAPI[index].K,
              M: this.clientesAPI[index].M,
              J: this.clientesAPI[index].J,
              V: this.clientesAPI[index].V,
              S: this.clientesAPI[index].S,
              Do: this.clientesAPI[index].Do,
              correo: this.clientesAPI[index].correo,
              tipoCilza: this.clientesAPI[index].tipoCilza,
              nombredoc: this.clientesAPI[index].nombredoc,
              g: this.clientesAPI[index].g,
              hizoMotivo: 0,
              hizoFactura: 0,
              tipoVisita: this.clientesAPI[index].tipoVisita,
              telefonoFijo: this.clientesAPI[index].telefonoFijo,
              telefonoOpcional: this.clientesAPI[index].telefonoOpcional,
              tipodoc: this.clientesAPI[index].tipodoc,
              documento: this.clientesAPI[index].documento,
              Activo: this.clientesAPI[index].Activo,
              Comentario: this.clientesAPI[index].Comentario
            }

            console.log("Clientes en inicio granel",cliente);
            //guardamos los objetos cliente en la BD SQLite
            this.db.guardarClientesSQLite(cliente);
          }
          allClientesLoadingController.dismiss();
      return result;
    })
  }





  //metodo que obtiene los productos del api para posteriormente guardarlos en SQLite
  getProductos(){
    let data:Observable<any> =  this.http.get(`${this.apiURL}productos`);

    data.subscribe(result => {
      this.productosAPI = result;
     console.log("Productos del API:",this.productosAPI);

      return result;
    })
  }

   //llamo el metodo del DatabaseProvider para almacenar los productos en SQLite
  guardarProductos(){
    //recorro los productos que obtuve del api
    for(var index in this.productosAPI){
      //armo cada cliente para despues almacenarlos
      let producto = {
        id:this.productosAPI[index].id,
        producto: this.productosAPI[index].producto,
        tipoPrecio: this.productosAPI[index].tipoPrecio,
        precio: this.productosAPI[index].precio,
        tipoCliente: this.productosAPI[index].tipoCliente,
        descripcion: this.productosAPI[index].descripcion,
        precioViejo: this.productosAPI[index].precioViejo
      }

      console.log("Productos",producto);


      this.db.guardarProductosSQLite(producto);
    }
  }

  //metodo que redirige a MenuPage, obtiene los clientes por ruta y llena la BD sqlite
  llenarSQLite(){

    //metodo que obtiene los clientes de la ruta seleccionada en el login
    this.getClientesRuta();
    //linea de codigo que redirige a la pantalla de menu y se lleva el id de la ruta seleccionada
    this.navCtrl.push(MenuGranelPage,{id:this.idRuta});
    //llamada a los metodos que guardan los datos en la bd de sqlite
    //this.guardarRutas();
    this.guardarProductos();

    //ver rutas guardadas en SQLite
    //this.obtenerRutasDeSQLite();
  }

  //metodo que redirige a MenuPage, obtiene los clientes por ruta y llena la BD sqlite
  llenarSQLiteProductosClientes(){

    //metodo que obtiene los clientes de la ruta seleccionada en el login
    this.getClientesRuta();
    //linea de codigo que redirige a la pantalla de menu y se lleva el id de la ruta seleccionada
    this.navCtrl.push(MenuGranelPage,{id:this.idRuta});
    //llamada a los metodos que guardan los datos en la bd de sqlite
    this.guardarProductos();

    //ver rutas guardadas en SQLite
    //this.obtenerRutasDeSQLite();
  }

  llenarSoloClientes(){
    this.getClientesRuta();
    this.navCtrl.push(MenuGranelPage,{id:this.idRuta});
  }

  //alerta que aparece cuando tienen que verificar si lleno la informacion
  mensajeCodigoDeSeguridad() {
    let alert = this.aCtrl.create({
      title: 'Código seguridad facturación',
      inputs: [
        {
          name: 'codigo',
          placeholder: 'Código',
          type: 'password'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Ingresar',
          handler: data => {

            if (data.codigo === 'tom') {
              this.login =  1;
              //this.navCtrl.push(MenuGranelPage);
            } else {
              this.presentToast();
              //return false;
            }
          }
        }
      ],
      enableBackdropDismiss : false
    });
    alert.present();
  }

  async presentToast() {
    const toast = await this.toast.create({
      message: 'Código incorrecto,intenta de nuevo',
      position: 'top',
      duration: 2000
    });
    toast.present();
  }

  obtenerLaRutaQueSeCargo(){

    let allRutasLoadingController = this.lCtrl.create({
    content: "Obteniendo Ruta Desde la Base de datos"
    });

    allRutasLoadingController.present();

    this.db.obtenerLaRutaCargada(parseInt(this.rutaCargada[0].ruta)).then((data) => {
    //allFacturasLoadingController.dismiss();
    allRutasLoadingController.dismiss();
    this.rutasAPI = data;
    console.log("Esta es la ruta cargada:" ,this.rutasAPI);
    //console.log("Estas son los clientes de SQLite",this.productosSQLite);
    //console.log(this.facturasSQLite);
    }, (error) => {
    console.log(error);
    })
  }


  //metodo que trae las rutas desde el api para posteriormente poder almacenarlas en SQLite
  cargarRutas(){

    let allRutasLoadingController = this.lCtrl.create({
      content: "Obteniendo Rutas Desde el Servidor"
    });

    allRutasLoadingController.present();

    let data:Observable<any> = this.http.get(`${this.apiURL}rutas?filter[where][cedi]=${this.cedi}`);

    data.subscribe(result => {
      allRutasLoadingController.dismiss();
      this.rutasAPI = result;
      //console.log(this.rutasAPI);

      return result;
    })
  }

  //metodo que guarda las rutas dentro de SQLite utilizando el metodo del DatabaseProvider
  guardarRutas(){

    //recorro las rutas que obtuve del api
   for(var index in this.rutasAPI){
     //armo cada ruta para despues almacenarlas
     let ruta = {
      id: this.rutasAPI[index].id,
      nombre: this.rutasAPI[index].nombre,
      unidad_negocio: this.rutasAPI[index].unidad_negocio,
      cedi: this.rutasAPI[index].cedi,
      descripcion: this.rutasAPI[index].descripcion,
      cerosTOM: this.rutasAPI[index].cerosTOM,
      consecutivo: this.rutasAPI[index].consecutivo
     }

     //console.log(ruta);
     //llamo al metodo del provider para pasar el objeto ruta y que lo almacene en SQLite
     this.db.guardarRutasSQLite(ruta);
   }

  }


  ionViewDidLoad() {
    this.obtenerDatos();
    this.obtenerRutaDesdeCliente();
  }

  //metodo que obtiene las rutas almacenadas en SQLite utilizando el metodo de DatabaseProvider
  obtenerRutasDeSQLite(){

    let allRutasLoadingController = this.lCtrl.create({
      content: "Obteniendo Rutas Desde la Base de datos"
    });

    allRutasLoadingController.present();

    this.db.getRutasSQLite().then((data) => {
      allRutasLoadingController.dismiss();
      this.rutasAPI = data;
      console.log(data);
    },(error) => {
      console.log(error);
    })
  }

  //metodo que obtiene los clientes almacenados en SQLite utilizando el metodo de DatabaseProvider
  obtenerClientesDeSQLite(){
    this.db.getClientesSQLite().then((data) => {
      this.clientesSQLite = data;

      if(this.clientesSQLite.length === 0 ){
        this.getClientesGranel();
      }else{
        console.log("ya los clientes han sido ingresados");
      }
      console.log(data);
    }, (error) => {
      console.log(error);
    })
  }



  obtenerRutaDesdeCliente(){
    this.db.obtenerRutaEspecificaCargada().then((data) => {
      //allFacturasLoadingController.dismiss();
      this.rutaCargada = data;
      console.log("Esta es la ruta cargada:" ,this.rutaCargada[0].ruta);
      //console.log("Estas son los clientes de SQLite",this.productosSQLite);
      //console.log(this.facturasSQLite);
    }, (error) => {
      console.log(error);
    })
  }



  obtenerLaRutaQueSeCargoParaActualizarConsecutivo(){


    this.db.obtenerLaRutaCargada(parseInt(this.rutaCargada[0].ruta)).then((data) => {
      //allFacturasLoadingController.dismiss();

      this.rutasAPI = data;

      this.info.actualizarRutaConConsecutivo({
        id:this.rutasAPI[0].id,
        consecutivo:this.rutasAPI[0].consecutivo,
        cedi: this.rutasAPI[0].cedi,
        unidad_negocio: this.rutasAPI[0].unidad_negocio,
        descripcion: this.rutasAPI[0].descripcion,
        cerosTOM: this.rutasAPI[0].cerosTOM,
        nombre: this.rutasAPI[0].nombre
      }).subscribe(data => {
        console.log(data)
      });

      console.log("Esta es el id",this.rutasAPI[0].id)
      console.log("Esta es la ruta cargada:" ,this.rutasAPI);



      //console.log("Estas son los clientes de SQLite",this.productosSQLite);
      //console.log(this.facturasSQLite);
    }, (error) => {
      console.log(error);
    })
  }

   //metodo que actualiza la ruta en la base de datos para que al siguiente dia se utilice el ultimo consecutivo
   actualizarRuta(){
    this.info.actualizarRutaConConsecutivo({
      id: this.codRuta,
      nombre: this.nombreRuta,
      unidad_negocio: this.unidadNegocioRuta,
      cedi: this.cediRuta,
      descripcion: this.descripcionRuta,
      cerosTOM: this.cerosTOM,
      consecutivo: this.consecutivoRuta
    })
    .subscribe(data => {
      console.log(data)
    });
  }



  obtenerDatos(){

  this.db.getAllProductsSQLite().then((data) => {
    //allFacturasLoadingController.dismiss();
    this.productosSQLite = data;
    //console.log("Estas son los clientes de SQLite",this.productosSQLite);
    //console.log(this.facturasSQLite);
  }, (error) => {
    console.log(error);
  })

  this.db.getClientesSQLite().then((data) => {
    this.clientesSQLite =data;

  }, (error) => {
    console.log(error);
  })

  this.db.getRutasSQLite().then((data) => {
    this.rutasSQLite = data;
  },(error) => {
    console.log(error);
  })
  }

  //metodo que borra los clientes almacenados en SQLite
  eliminarClientes(){
      this.db.eliminarClientesSQLite().then((data)=>{
        console.log(data);
      },(error) => {
        console.log(error)
      })
  }

  //metodo que borra los productos almacenados en SQlite
  eliminarProductos(){
    this.db.eliminarProductosSQLite().then((data)=>{
      console.log(data);
    },(error) => {
      console.log(error)
    })
  }

  //metodo que borra las rutas almacenadas en SQLite
   eliminarRutas(){
    this.db.eliminarRutasSQLite().then((data)=>{
      console.log(data);
    },(error) => {
      console.log(error)
    })
  }


  //alerta que aparece cuando tienen que verificar si lleno la informacion
  mensajeDeSeguridad() {
    let alert = this.aCtrl.create({
      title: 'Si no aparecen los botones para ingresar al sistema, presione aceptar',
      subTitle: 'Solo si no aparecen los botones',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        },
         {
          text: 'Aceptar',
          handler: () => {
            console.log('Confirm Okay');
            this.eliminarClientes();
            this.eliminarProductos();
            this.eliminarRutas();
            setTimeout( () => {
              this.platform.exitApp();
            }, 1000);
          }
        }
      ]
    });
    alert.present();
  }

}
