import { ImprimirPage } from './../imprimir/imprimir';
import { InformacionProvider } from './../../providers/informacion/informacion';
import { SqliteDbProvider } from './../../providers/sqlite-db/sqlite-db';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, Platform } from 'ionic-angular';

/**
 * Generated class for the CierrePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-cierre',
  templateUrl: 'cierre.html',
})
export class CierrePage {



  total50mil; total20mil; total10mil; total5mil; total2mil; totalmil; totalQuinientos; totalCien; totalCincuenta; totalVeinticinco; totalDiez; totalCinco; totalDolares;
  granTotal; billetesSQLite: any = [];

  //Variables a utilizar
  facturasSQLite: any = [];
  facturasSQLiteCred: any = [];
  facturasSQLiteCon: any = [];
  TramitesSQLite: any = [];
  GastosSQLite: any = [];
  idRuta;
  TOM;
  GLN;
  //variables de ruta
  rutaEnUso; nombreRuta; unidadNegocioRuta; cediRuta; descripcionRuta; consecutivoRuta; cerosTOM;
  codRuta;

  //Variables a utilizar
  detallesSQLite: any = [];
  totalfac;
  totalFinal: number;
  total;
  totalCredito;
  totalContado;
  totalFinalContado;
  totalFinalCredito;
  clase = '';
  totalGastos = 0;
  //Variables para cierre

  claves: any = [];
  consecutivo;
  clave;

  //Variables para contar los liquidos
  q25r;
  q25;
  q20;
  q10;
  q10r;
  q35;
  q45;
  q60;
  q60r;
  q100;
  qtls;
  qkgs;
  totalq25;
  totalq25r;
  totalq10;
  totalq10r;
  totalq20;
  totalq30;
  totalq40;
  totalq35r;
  totalq45r;
  totalq60;
  totalq60r;
  totalq100;
  totallts;
  totalkgs;
  gasto: number;
  monto;
  final = 0;
  motivos: any = [];
  billetes: any = [];
  gastos: any = [];
  factura: any = [];
  detalle: any = [];
  clientesNuevos: any = [];
  claveConsecutivo: any = [];
  contador = 0;
  diezA25;
  veinteA25;
  veiticincoPreA25;
  veinticincoRosA25;
  treintaA25;
  cuarentaA25;
  cuarentaCincoA25;
  cincuentaA25;
  sesentaA25;
  cienA25;
  totalCilindros25;


  constructor(public navCtrl: NavController, public navParams: NavParams,
    public db: SqliteDbProvider, public aCtrl: AlertController, public platform: Platform, public info: InformacionProvider) {
    this.idRuta = this.navParams.get('id');
    console.log(this.idRuta, "La Ruta que se va a Actualizar");
    this.obtenerRutaEnUso();
  }


  ionViewDidLoad() {
    this.obtenerFacturasDeSQLite();
    this.obtenerFacturasDeCredito();
    this.obtenerFacturasDeContado();
    this.obtenerTramitesBancarios();
    this.obtenerGastos();
    this.obtenerBilletesDeSQLite();

  }

  //metodo que obtiene la ruta en uso para utilizar sus datos en la actualizacion del consecutivo
  //y armar el TOM para cada factura
  obtenerRutaEnUso() {
    this.db.getRutaUsada(this.idRuta).then((data) => {
      this.rutaEnUso = data;
      console.log(this.rutaEnUso);
      this.codRuta = this.rutaEnUso.id;
      this.nombreRuta = this.rutaEnUso.nombre;
      this.unidadNegocioRuta = this.rutaEnUso.unidad_negocio;
      this.cediRuta = this.rutaEnUso.cedi;
      this.descripcionRuta = this.rutaEnUso.descripcion;
      this.consecutivoRuta = this.rutaEnUso.consecutivo;
      this.cerosTOM = this.rutaEnUso.cerosTOM;

    }, (error) => {
      console.log(error);
    })
  }

  //metodo que actualiza la ruta en la base de datos para que al siguiente dia se utilice el ultimo consecutivo
  actualizarRuta() {
    this.info.actualizarRutaConConsecutivo({
      id: this.codRuta,
      nombre: this.nombreRuta,
      unidad_negocio: this.unidadNegocioRuta,
      cedi: this.cediRuta,
      descripcion: this.descripcionRuta,
      cerosTOM: this.cerosTOM,
      consecutivo: this.consecutivoRuta
    })
      .subscribe(data => {
        console.log(data)
      });
  }


  obtenerFacturasDeCredito() {
    this.db.getFacturaCredito().then((data) => {
      this.facturasSQLiteCred = data;
      this.total = this.facturasSQLiteCred.total;
      console.log(this.facturasSQLite);
      this.totalFinalCredito = this.facturasSQLiteCred.reduce((a, b) => a + (b.total * 1), 0);
      console.log(this.facturasSQLiteCred);
      console.log(this.totalFinalCredito);
    }, (error) => {
      console.log(error);
    })
  }

  //metodo que obtiene las facturas almacenados en SQLite utilizando el metodo de DatabaseProvider
  obtenerFacturasDeSQLite() {
    this.db.getFacturasSQLite().then((data) => {
      this.facturasSQLite = data;
      this.total = this.facturasSQLite.total;
      this.q25 = this.facturasSQLite.q25;
      this.q25r = this.facturasSQLite.q25r
      //console.log(this.facturasSQLite);
      this.totalFinal = this.facturasSQLite.reduce((a, b) => a + (b.total * 1), 0);
      this.totalq10 = this.facturasSQLite.reduce((a, b) => a + (b.q10 * 1), 0);
      this.totalq10r = this.facturasSQLite.reduce((a, b) => a + (b.q10r * 1), 0);
      this.totalq20 = this.facturasSQLite.reduce((a, b) => a + (b.q20 * 1), 0);

      this.totalq25 = this.facturasSQLite.reduce((a, b) => a + (b.q25 * 1), 0);
      this.totalq25r = this.facturasSQLite.reduce((a, b) => a + (b.q25r * 1), 0);
      this.totalq30 = this.facturasSQLite.reduce((a, b) => a + (b.q35 * 1), 0);
      this.totalq40 = this.facturasSQLite.reduce((a, b) => a + (b.q45 * 1), 0);
      this.totalq35r = this.facturasSQLite.reduce((a, b) => a + (b.q35r * 1), 0);
      this.totalq45r = this.facturasSQLite.reduce((a, b) => a + (b.q45r * 1), 0);
      this.totalq60 = this.facturasSQLite.reduce((a, b) => a + (b.q60 * 1), 0);
      this.totalq60r = this.facturasSQLite.reduce((a, b) => a + (b.q60r * 1), 0);
      this.totalq100 = this.facturasSQLite.reduce((a, b) => a + (b.q100 * 1), 0);
      this.totallts = this.facturasSQLite.reduce((a, b) => a + (b.qtls * 1), 0);
      this.totalkgs = this.facturasSQLite.reduce((a, b) => a + (b.qkgs * 1), 0);
      console.log(this.facturasSQLite);
      console.log('Total final 168', this.totalFinal);
    }, (error) => {
      console.log(error);
    })
  }

  //metodo que obtiene las facturas almacenados en SQLite utilizando el metodo de DatabaseProvider


  //metodo que obtiene las facturas almacenados en SQLite utilizando el metodo de DatabaseProvider
  obtenerFacturasDeContado() {
    this.db.getFacturaContado().then((data) => {
      this.facturasSQLiteCon = data;
      this.total = this.facturasSQLiteCon.total;
      console.log(this.facturasSQLite);
      this.totalFinalContado = this.facturasSQLiteCon.reduce((a, b) => a + (b.total * 1), 0);
      console.log(this.facturasSQLiteCon);
      console.log(this.totalFinalContado);
    }, (error) => {
      console.log(error);
    })
  }

  obtenerBilletesDeSQLite() {
    this.db.getBilletesSQLite().then((data) => {
      //allFacturasLoadingController.dismiss();
      this.billetesSQLite = data;
      this.total50mil = this.billetesSQLite.reduce((a, b) => a + (b.cincuentaMil * 50000), 0);
      this.total20mil = this.billetesSQLite.reduce((a, b) => a + (b.veinteMil * 20000), 0);
      this.total10mil = this.billetesSQLite.reduce((a, b) => a + (b.diezMil * 10000), 0);
      this.total5mil = this.billetesSQLite.reduce((a, b) => a + (b.cincoMil * 5000), 0);
      this.total2mil = this.billetesSQLite.reduce((a, b) => a + (b.dosMil * 2000), 0);
      this.totalmil = this.billetesSQLite.reduce((a, b) => a + (b.mil * 1000), 0);
      this.totalQuinientos = this.billetesSQLite.reduce((a, b) => a + (b.quinientos * 500), 0);
      this.totalCien = this.billetesSQLite.reduce((a, b) => a + (b.cien * 100), 0);
      this.totalCincuenta = this.billetesSQLite.reduce((a, b) => a + (b.cincuenta * 50), 0);
      this.totalVeinticinco = this.billetesSQLite.reduce((a, b) => a + (b.veinticinco * 25), 0);
      this.totalDiez = this.billetesSQLite.reduce((a, b) => a + (b.diez * 10), 0);
      this.totalCinco = this.billetesSQLite.reduce((a, b) => a + (b.cinco * 5), 0);

      this.granTotal = this.total50mil + this.total10mil + this.total20mil + this.total5mil + this.total2mil + this.totalmil + this.totalQuinientos + this.totalCien + this.totalCincuenta + this.totalVeinticinco + this.totalDiez + this.totalCinco;

      console.log(this.billetesSQLite);
    }, (error) => {
      console.log(error);
    })
  }

  // IrAVerFactura(TOM: object) {
  //   this.navCtrl.push(VerFacturaPage, {TOM:this.TOM} );
  // }


  //metodo que obtiene las facturas almacenados en SQLite utilizando el metodo de DatabaseProvider
  obtenerTramitesBancarios() {
    this.db.getTransacciones().then((data) => {
      this.TramitesSQLite = data;
      console.log("transacciones son aqui", this.TramitesSQLite);
      //this.totalFinalContado = this.facturasSQLiteCon.reduce((a, b) => a + (b.total * 1), 0);
      console.log(this.TramitesSQLite);
    }, (error) => {
      console.log(error);
    })
  }


  //metodo que obtiene las facturas almacenados en SQLite utilizando el metodo de DatabaseProvider
  obtenerGastos() {
    this.db.getGastos().then((data) => {
      this.GastosSQLite = data;
      //this.monto = this.GastosSQLite.monto

      this.totalGastos = this.GastosSQLite.reduce((a, b) => a + (b.monto * 1), 0);


      this.final = this.totalFinal - this.totalGastos;
      console.log("Gastos 219", this.totalGastos);
      console.log("Total final 220", this.totalFinal);
      console.log(" FINAL 221", this.final);


      //this.totalFinalContado = this.facturasSQLiteCon.reduce((a, b) => a + (b.total * 1), 0);
      console.log(this.GastosSQLite);
    }, (error) => {
      console.log(error);
    })
  }

  // restaGasto(){
  //   this.final =  this.totalFinal - this.totalGastos;
  //   console.log("Gastos 230",this.totalGastos);
  //   console.log("Total final 231",this.totalFinal);
  //   console.log(" FINAL 231", this.final);
  // }



  //alerta que aparece cuando tienen que verificar si lleno la informacion
  mensajeDeSeguridad() {
    let alert = this.aCtrl.create({
      title: 'Se va a actualizar el consecutivo para el siguiente día y borrar la informacion del día..',
      subTitle: 'Si esta seguro presione aceptar para actualizar y borrar los datos del día.',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            console.log('Confirm Okay');
            this.eliminacioneDeDatos();
            //this.actualizarRuta();
            setTimeout(() => {
              this.platform.exitApp();
            }, 2000);
          }
        }
      ],
      enableBackdropDismiss: false
    });
    alert.present();
  }

  //alerta que aparece cuando tienen que verificar si lleno la informacion
  mensajeDeSeguridadNoEnviado() {
    let alert = this.aCtrl.create({
      title: 'No ha enviado los datos faltantes, asegurese de tener señal de internet o datos para enviarlos. Presione el boton rojo para que se envien.',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            console.log('Confirm Okay');
            this.actualizarRuta();
            // this.eliminacioneDeDatos()
            //this.actualizarRuta();
            // setTimeout( () => {
            //   this.platform.exitApp();
            // }, 2000);
          }
        }
      ],
      enableBackdropDismiss: false
    });
    alert.present();
  }

  limpiayactualizacion() {

    if (this.contador === 0) {
      this.mensajeDeSeguridadNoEnviado();
    } else if (this.contador >= 1) {
      this.mensajeDeSeguridad();
    }
  }

  //alerta que aparece cuando tienen que verificar si lleno la informacion
  mensajeDeSeguridadEnvio() {
    let alert = this.aCtrl.create({
      title: 'Se va a enviar la informacion faltante para el cierre en caso de que falte alguna factura, un gasto o una transacción..',
      subTitle: 'Presione aceptar para completar la tarea.',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            console.log('Confirm Okay');
            //this.envios();
            this.actualizarRuta();
          }
        }
      ],
      enableBackdropDismiss: false
    });
    alert.present();
  }

  envios() {
    this.actualizarRuta();
    this.EnviarMotivosDeSQLite();
    this.enviarTransacciones();
    this.enviarBilletes();
    this.enviarFacturasRestantes();
    this.enviarDetallesFaltantes();
    this.enviarGastos();
    // this.enviarClaves();
    //this.enviarClientesNuevosFaltantes();

    this.contador = this.contador + 1;
  }

  //metodo que obtiene los motivos de sqlite y los envia a la base de datos
  EnviarMotivosDeSQLite() {
    this.db.getMotivosSQLite().then((data) => {
      this.motivos = data;
      console.log('linea 271', this.motivos);
      for (var index in this.motivos) {
        //armo cada cliente para despues almacenarlos
        let motivo = {
          codigoCliente: this.motivos[index].codigoCliente,
          fecha: this.motivos[index].fecha,
          ruta: this.motivos[index].ruta,
          motivo: this.motivos[index].motivo,
          fechaRuta: this.motivos[index].fechaRuta
        }
        console.log('Linea 281', motivo);
        this.info.enviarMotivosNoCompra(motivo);
      }
      console.log(data);
    }).catch(error => { console.error(error) })
  }

  //metodo para enviar las transacciones realizadas y los depositos a la base de datos
  enviarTransacciones() {
    this.db.getTransaccionesParaEnviar().then((data) => {
      this.TramitesSQLite = data;
      console.log(this.TramitesSQLite);

      for (var index in this.TramitesSQLite) {

        let tran = {
          tipo: this.TramitesSQLite[index].tipo,
          documento: this.TramitesSQLite[index].documento,
          banco: this.TramitesSQLite[index].banco,
          factura: this.TramitesSQLite[index].factura,
          monto: this.TramitesSQLite[index].monto,
          fecha: this.TramitesSQLite[index].fecha,
          ruta: this.TramitesSQLite[index].ruta,
        }

        console.log(tran);
        this.info.enviarTransacciones(tran);
      }


    }).catch(error => { console.error(error) })
  }

  //metodo para enviar los gastos ingresados a la base de datos
  enviarGastos() {
  }

  //metodo para enviar los billetes a la base de datos
  enviarBilletes() {
    this.db.getBilletesSQLite().then((data) => {
      this.billetes = data;
      console.log(this.billetes);

      for (var index in this.billetes) {

        let billete = {
          diez: this.billetes[index].diez,
          vein: this.billetes[index].veinticinco,
          cin: this.billetes[index].cincuenta,
          cien: this.billetes[index].cien,
          qui: this.billetes[index].quinientos,
          cinco: this.billetes[index].cinco,
          '50k': this.billetes[index].cincuentaMil,
          '20k': this.billetes[index].veinteMil,
          '10k': this.billetes[index].diezMil,
          '5k': this.billetes[index].cincoMil,
          '2k': this.billetes[index].dosMil,
          '1k': this.billetes[index].mil,
          dolares: this.billetes[index].dolares,
          vale: this.billetes[index].vale,
          ruta: this.billetes[index].ruta,
          fecha: this.billetes[index].fecha
        }

        console.log(billete);
        this.info.enviarBilletes(billete);
      }


    }).catch(error => { console.error(error) })
  }

  //metodo que envia las facturas que no se habian enviado a la base de datos
  enviarFacturasRestantes() {

    this.db.getFacturasNoEnviadasSQLite().then((data) => {
      this.factura = data;
      console.log(this.factura);

      for (var index in this.factura) {

        var fac = {
          codigo: this.factura[index].codigo,
          facnumero: this.factura[index].facnumero,
          razon_social: this.factura[index].razon_social,
          nombre: this.factura[index].nombre,
          descuento: this.factura[index].descuento,
          descuentot: this.factura[index].descuentot,
          credito: this.factura[index].credito,
          total: this.factura[index].total,
          ruta: this.factura[index].ruta,
          fecha: this.factura[index].fecha,
          q25: this.factura[index].q25,
          q25r: this.factura[index].q25r,
          q100: this.factura[index].q100,
          q20: this.factura[index].q20,
          q10: this.factura[index].q10,
          q10r: this.factura[index].q10r,
          q35: this.factura[index].q35,
          q35r: this.factura[index].q35r,
          q45: this.factura[index].q45,
          q45r: this.factura[index].q45r,
          q60: this.factura[index].q60,
          q60r: this.factura[index].q60r,
          qlts: this.factura[index].qtls,
          qkgs: this.factura[index].qkgs,
          LAT: this.factura[index].LAT,
          LONGI: this.factura[index].LONGI,
          g: this.factura[index].g,
          efectivo: this.factura[index].efectivo,
          transmonto: this.factura[index].transmonto,
          transnumero: this.factura[index].transnumero,
          chknumero: this.factura[index].chknumero,
          chkbanco: this.factura[index].chkbanco,
          chkmonto: this.factura[index].chkmonto,
          tipo: this.factura[index].tipo,
          cz25: this.factura[index].cz25,
          cz25r: this.factura[index].cz25r,
          cz100r: this.factura[index].cz100r,
          fechadt: this.factura[index].fechadt
        }


        this.info.enviarFaturas(fac);

        this.actualizarEnviado(fac.facnumero);

      }
    }).catch(error => { console.error(error) })
  }

  //metodo que actualiza el campo enviado para cambiar el color en la interfaz
  actualizarEnviado(TOM: any) {
    this.db.actualizarEnviadoFactura({
      facnumero: TOM,
      enviada: 1
    }).then((data) => {
      console.log(data);
    }).catch(error => { console.error(error) });

    this.obtenerFacturasDeSQLite();
  }

  //metodo que envia los detalles que no se habian enviado a la base de datos
  enviarDetallesFaltantes() {

    this.db.getDetallesNoEnviadosSQLite().then((data) => {
      this.detalle = data;
      console.log(this.detalle);

      for (var index in this.detalle) {

        var detail = {
          idcliente: this.detalle[index].idcliente,
          ruta: this.detalle[index].ruta,
          facnumero: this.detalle[index].facnumero,
          totalexenta: this.detalle[index].totalexenta,
          total: this.detalle[index].total,
          fechacreacion: this.detalle[index].fechacreacion,
          detalle: this.detalle[index].detalle,
          rawdetalle: this.detalle[index].rawdetalle,
          detail: this.detalle[index].detail,
          corre: this.detalle[index].corre,
          estado: this.detalle[index].estado,
        }

        this.info.enviarDetalles(detail);

      }
    }).catch(error => { console.error(error) })
  }

  enviarClientesNuevosFaltantes() {

    this.db.getClientesNuevosNoEnviados().then((data) => {
      this.clientesNuevos = data;
      console.log(this.clientesNuevos);

      for (var index in this.clientesNuevos) {


        var cliente = {
          codigo: this.clientesNuevos[index].codigo,
          codhis: '0',
          proveedor: '0',
          GLN: null,
          EAN: null,
          codigoscad: '0',
          nombre: this.clientesNuevos[index].nombre,
          razon: this.clientesNuevos[index].razon,
          descuento: this.clientesNuevos[index].descuento,
          precio: this.clientesNuevos[index].precio,
          tipo: this.clientesNuevos[index].tipo,
          ruta: this.clientesNuevos[index].ruta,
          subcanal: this.clientesNuevos[index].subcanal,
          formapago: this.clientesNuevos[index].formapago,
          credito: this.clientesNuevos[index].credito,
          LAT: this.clientesNuevos[index].LAT,
          LONGI: this.clientesNuevos[index].LONGI,
          g: '0',
          puedeFacturar: this.clientesNuevos[index].puedeFacturar,
          lprecioa: this.clientesNuevos[index].lprecioa,
          lprecioc: this.clientesNuevos[index].lprecioc,
          ldescuento: this.clientesNuevos[index].ldescuento,
          especial: this.clientesNuevos[index].especial,
          L: null,
          K: null,
          M: null,
          J: null,
          V: null,
          S: null,
          Do: null,
          fechacreacion: this.clientesNuevos[index].fechacreacion,
          tipodoc: '0',
          documento: '0',
          correo: null,
          nombredoc: null,
        }

        this.info.enviarClienteNuevo(cliente);
      }
    }).catch(error => { console.error(error) })
  }

  eliminacioneDeDatos() {
    this.eliminarClientes();
    this.eliminarProductos();
    this.eliminarClaves();
    //this.eliminarRutas();
    this.eliminarBilletes();
    this.eliminarClientesNuevos();
    this.eliminarTransacciones();
    this.eliminarFacturas();
    this.eliminarDetalles();
    this.eliminarMotivosNoCompra();
    this.eliminarDescargas();
  }

  eliminarDescargas() {
    this.db.eliminarDescargasSQLite().then((data) => {
      console.log(data);
    }, (error) => {
      console.log(error)
    })
  }

  //metodo que borra los clientes almacenados en SQLite
  eliminarClientes() {
    this.db.eliminarClientesSQLite().then((data) => {
      console.log(data);
    }, (error) => {
      console.log(error)
    })
  }

  eliminarClaves() {
    this.db.eliminarClaves().then((data) => {
      console.log(data);
    }, (error) => {
      console.log(error);
    })
  }

  //metodo que borra los clientes nuevos almacenados en SQLite
  eliminarClientesNuevos() {
    this.db.eliminarClientesNuevosSQLite().then((data) => {
      console.log(data);
    }, (error) => {
      console.log(error)
    })
  }

  //metodo que borra los clientes nuevos almacenados en SQLite
  eliminarMotivosNoCompra() {
    this.db.eliminaMotivosNoCompraSQLite().then((data) => {
      console.log(data);
    }, (error) => {
      console.log(error)
    })
  }

  //metodo que borra los billetes almacenados en SQLite
  eliminarBilletes() {
    this.db.eliminarBilletesSQLite().then((data) => {
      console.log(data);
    }, (error) => {
      console.log(error)
    })
  }

  //metodo que borra los billetes almacenados en SQLite
  eliminarTransacciones() {
    this.db.eliminarTransaccionesSQLite().then((data) => {
      console.log(data);
    }, (error) => {
      console.log(error)
    })
  }

  //metodo que borra las facturas almacenados en SQLite
  eliminarFacturas() {
    this.db.eliminarFacturasSQLite().then((data) => {
      console.log(data);
    }, (error) => {
      console.log(error)
    })
  }

  //metodo que borra los detalles almacenados en SQLite
  eliminarDetalles() {
    this.db.eliminarDetallesSQLite().then((data) => {
      console.log(data);
    }, (error) => {
      console.log(error)
    })
  }

  //metodo que borra los productos almacenados en SQlite
  eliminarProductos() {
    this.db.eliminarProductosSQLite().then((data) => {
      console.log(data);
    }, (error) => {
      console.log(error)
    })
  }

  //metodo que borra las rutas almacenadas en SQLite
  eliminarRutas() {
    this.db.eliminarRutasSQLite().then((data) => {
      console.log(data);
    }, (error) => {
      console.log(error)
    })
  }

  // enviarClaves(){
  //   this.db.getClavesDeSQLite().then((data) => {
  //     this.claveConsecutivo = data;

  //     for(var index in this.claveConsecutivo){

  //       var clave = {
  //         clave: this.claveConsecutivo[index].clave,
  //         consecutivo: this.claveConsecutivo[index].consecutivo,
  //         facnumero: this.claveConsecutivo[index].facnumero,
  //         fecha: this.claveConsecutivo[index].fecha
  //       }

  //       this.info.enviarClaves(clave);
  //     }



  //   })
  // }

  //metodo que lleva a la vista de imprimir
  irAImprimir(facnumero: any, name: any) {

    this.db.getClaveConsecutivoPorTOM(parseInt(facnumero)).then((data) => {

      this.claves = data;
      this.consecutivo = this.claves.consecutivo;
      this.clave = this.claves.clave;

      this.navCtrl.push(ImprimirPage, { TOM: facnumero, nombre: name, claveFactura: this.clave, consecutivoFactura: this.consecutivo });

    }, (error) => {
      console.log(error);
    })


  }

}
